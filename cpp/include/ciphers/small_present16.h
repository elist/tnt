/**
 * C implementation of SMALL-PRESENT16.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#ifndef  _SMALL_PRESENT16_H_
#define  _SMALL_PRESENT16_H_

// ---------------------------------------------------------------------

#include <cstdint>
#include <cstdlib>

#include "ciphers/small_present.h"

// ---------------------------------------------------------------------

namespace ciphers {

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

#define SMALL_PRESENT16_NUM_KEY_BYTES        SMALL_PRESENT_NUM_KEY_BYTES
#define SMALL_PRESENT16_NUM_ROUND_KEY_BYTES   2
#define SMALL_PRESENT16_NUM_STATE_BYTES       2
#define SMALL_PRESENT16_NUM_ROUNDS           SMALL_PRESENT_NUM_ROUNDS
#define SMALL_PRESENT16_NUM_ROUND_KEYS       SMALL_PRESENT_NUM_ROUND_KEYS
    
    // ---------------------------------------------------------
    // Types
    // ---------------------------------------------------------
    
    typedef uint8_t small_present16_key_t[SMALL_PRESENT16_NUM_KEY_BYTES];
    typedef uint8_t small_present16_state_t[SMALL_PRESENT16_NUM_STATE_BYTES];
    
    typedef struct {
        uint16_t subkeys[SMALL_PRESENT16_NUM_ROUND_KEYS];
    } small_present16_context_t;
    
    // ---------------------------------------------------------
    // API
    // ---------------------------------------------------------
    
    void small_present16_key_schedule(small_present16_context_t *ctx,
                                      const small_present16_key_t key);
    
    // ---------------------------------------------------------
    
    void small_present16_encrypt(const small_present16_context_t *ctx,
                                 const small_present16_state_t plaintext,
                                 small_present16_state_t ciphertext);
    
    // ---------------------------------------------------------
    
    void small_present16_encrypt(const small_present16_context_t *ctx,
                                 const small_present16_state_t plaintext,
                                 small_present16_state_t ciphertext,
                                 size_t num_rounds);
    
    // ---------------------------------------------------------
    
    void small_present16_decrypt(const small_present16_context_t *ctx,
                                 const small_present16_state_t ciphertext,
                                 small_present16_state_t plaintext);
    
    // ---------------------------------------------------------
    
    void small_present16_decrypt(const small_present16_context_t *ctx,
                                 const small_present16_state_t ciphertext,
                                 small_present16_state_t plaintext,
                                 size_t num_rounds);
    
}

// ---------------------------------------------------------------------

#endif // _SMALL_PRESENT16_H_
