/**
 * C implementation of Small-AES.
 * 
 * __author__ = anonymized
 * __date__   = 2019-05
 * __copyright__ = Creative Commons CC0
 */

#ifndef _EXPERIMENTAL_SMALL_TWAES36_H_
#define _EXPERIMENTAL_SMALL_TWAES36_H_

// ---------------------------------------------------------------------

#include <cstdint>

#include "ciphers/small_aes36.h"
#include "utils/utils.h"

namespace ciphers {

#define EXPERIMENTAL_SMALL_TWAES36_NUM_ROUNDS       5
#define EXPERIMENTAL_SMALL_TWAES36_NUM_ROUND_KEYS   6
    
    // ---------------------------------------------------------------------
    // Constants
    // ---------------------------------------------------------------------
    
    // ---------------------------------------------------------------------
    // Types
    // ---------------------------------------------------------------------
    
    // ---------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------
    
    void experimental_small_twaes36_key_schedule(small_aes36_context_t *ctx,
                                                 const small_aes36_key_t key);
    
    // ---------------------------------------------------------------------
    
    uint64_t
    experimental_small_twaes36_expand_tweak(uint64_t tweak_input);
    
    // ---------------------------------------------------------------------
    
    uint64_t
    experimental_small_twaes36_decrypt(const small_aes36_context_t *ctx,
                                       uint64_t tweak,
                                       uint64_t ciphertext);
    
    // ---------------------------------------------------------------------
    
    uint64_t
    experimental_small_twaes36_encrypt(const small_aes36_context_t *ctx,
                                       uint64_t tweak,
                                       uint64_t plaintext);
    
    // ---------------------------------------------------------------------
    
    uint64_t
    experimental_small_twaes36_encrypt_rounds(const small_aes36_context_t *ctx,
                                              uint64_t tweak,
                                              uint64_t plaintext,
                                              size_t num_rounds);
    
}

#endif  // _EXPERIMENTAL_SMALL_TWAES36_H_
