/**
 * C implementation of TNT with Small-PRESENT-16.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#ifndef  _TNT_H_
#define  _TNT_H_

// ---------------------------------------------------------------------

#include <cstdint>
#include <cstdlib>

namespace ciphers {

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

#define TNT_NUM_ROUNDS                       3

}

// ---------------------------------------------------------------------

#endif // _TNT_H_
