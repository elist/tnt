/**
 * C implementation of SMALL-PRESENT24.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#ifndef  _TWEAKABLE_SMALL_PRESENT24_H_
#define  _TWEAKABLE_SMALL_PRESENT24_H_

// ---------------------------------------------------------------------

#include <cstdint>
#include <cstdlib>

#include "ciphers/small_present.h"
#include "ciphers/small_present24.h"

// ---------------------------------------------------------------------

namespace ciphers {
    
    void tweakable_small_present24_encrypt(const small_present24_context_t *ctx,
                                           const small_present24_state_t tweak,
                                           const small_present24_state_t plaintext,
                                           small_present24_state_t ciphertext);
    
    // ---------------------------------------------------------
    
    void tweakable_small_present24_encrypt(const small_present24_context_t *ctx,
                                           const small_present24_state_t tweak,
                                           const small_present24_state_t plaintext,
                                           small_present24_state_t ciphertext,
                                           size_t num_rounds);
    
    // ---------------------------------------------------------
    
    void tweakable_small_present24_decrypt(const small_present24_context_t *ctx,
                                           const small_present24_state_t tweak,
                                           const small_present24_state_t ciphertext,
                                           small_present24_state_t plaintext);
    
    // ---------------------------------------------------------
    
    void tweakable_small_present24_decrypt(const small_present24_context_t *ctx,
                                           const small_present24_state_t tweak,
                                           const small_present24_state_t ciphertext,
                                           small_present24_state_t plaintext,
                                           size_t num_rounds);
    
}

// ---------------------------------------------------------------------

#endif // _TWEAKABLE_SMALL_PRESENT24_H_
