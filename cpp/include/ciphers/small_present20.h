/**
 * C implementation of SMALL-PRESENT20.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#ifndef  _SMALL_PRESENT20_H_
#define  _SMALL_PRESENT20_H_

// ---------------------------------------------------------------------

#include <cstdint>
#include <cstdlib>

#include "ciphers/small_present.h"

// ---------------------------------------------------------------------

namespace ciphers {

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

#define SMALL_PRESENT20_NUM_KEY_BYTES        SMALL_PRESENT_NUM_KEY_BYTES
#define SMALL_PRESENT20_NUM_ROUND_KEY_BYTES  3
#define SMALL_PRESENT20_NUM_STATE_BYTES      3
#define SMALL_PRESENT20_NUM_ROUNDS           SMALL_PRESENT_NUM_ROUNDS
#define SMALL_PRESENT20_NUM_ROUND_KEYS       SMALL_PRESENT_NUM_ROUND_KEYS

    static const size_t SMALL_PRESENT20_PERMUTATION[20] = {
        19, 15, 11,  7,  3,
        18, 14, 10,  6,  2,
        17, 13,  9,  5,  1,
        16, 12,  8,  4,  0
    };
    
    static const size_t SMALL_PRESENT20_INVERSE_PERMUTATION[20] = {
        19, 14,  9,  4,
        18, 13,  8,  3,
        17, 12,  7,  2,
        16, 11,  6,  1,
        15, 10,  5,  0
    };
    
    // ---------------------------------------------------------
    // Types
    // ---------------------------------------------------------
    
    typedef uint8_t small_present20_key_t[SMALL_PRESENT20_NUM_KEY_BYTES];
    typedef uint8_t small_present20_state_t[SMALL_PRESENT20_NUM_STATE_BYTES];
    
    typedef struct {
        uint32_t subkeys[SMALL_PRESENT20_NUM_ROUND_KEYS];
    } small_present20_context_t;
    
    // ---------------------------------------------------------
    // API
    // ---------------------------------------------------------
    
    void small_present20_key_schedule(small_present20_context_t *ctx,
                                      const small_present20_key_t key);
    
    // ---------------------------------------------------------
    
    void small_present20_encrypt(const small_present20_context_t *ctx,
                                 const small_present20_state_t plaintext,
                                 small_present20_state_t ciphertext);
    
    // ---------------------------------------------------------
    
    void small_present20_decrypt(const small_present20_context_t *ctx,
                                 const small_present20_state_t ciphertext,
                                 small_present20_state_t plaintext);
    
}

// ---------------------------------------------------------------------

#endif // _SMALL_PRESENT20_H_
