/**
 * C implementation of Small-AES.
 * 
 * __author__ = anonymized
 * __date__   = 2019-05
 * __copyright__ = Creative Commons CC0
 */

#ifndef _SMALL_AES36_H_
#define _SMALL_AES36_H_

// ---------------------------------------------------------------------

#include <cstdint>

#include "utils/utils.h"

namespace ciphers {
    
    // ---------------------------------------------------------------------
    // Constants
    // ---------------------------------------------------------------------

#define SMALL_AES36_NUM_STATE_BYTES     5
#define SMALL_AES36_NUM_KEY_BYTES       5
#define SMALL_AES36_NUM_ROUND_KEY_BYTES 5

#define SMALL_AES36_NUM_ROUNDS       10
#define SMALL_AES36_NUM_ROUND_KEYS   11
#define SMALL_AES36_NUM_ROWS          3
#define SMALL_AES36_NUM_COLUMNS       3
#define SMALL_AES36_NUM_CELLS         9
    
    const size_t SMALL_AES36_SBOX_ARRAY[16] = {
        // 0     1     2     3     4     5     6     7
        0x06, 0x0b, 0x05, 0x04, 0x02, 0x0e, 0x07, 0x0a,
        // 8     9    10    11    12    13    14    15
        // 8     9     a     b     c     d     e     f
        0x09, 0x0d, 0x0f, 0x0c, 0x03, 0x01, 0x00, 0x08
    };
    
    const size_t SMALL_AES36_INVERSE_SBOX_ARRAY[16] = {
        0x0e, 0x0d, 0x04, 0x0c, 0x03, 0x02, 0x00, 0x06,
        0x0f, 0x08, 0x07, 0x01, 0x0b, 0x09, 0x05, 0x0a
    };
    
    const size_t SMALL_AES_ROUND_CONSTANTS[15] = {
        0x0, 0x1, 0x2, 0x4, 0x8, 0x3, 0x6, 0xc,
        0xb, 0x5, 0xa, 0x7, 0xe, 0xf, 0xd
    };
    
    // ---------------------------------------------------------------------
    // SSE Constants
    // ---------------------------------------------------------------------

#define LO_NIBBLES_MASK                 vset64(0x0F0F0F0F0F0F0F0FL, 0x0F0F0F0F0F0F0F0FL)
#define SMALL_AES36_SBOX                vsetr8(0x06, 0x0b, 0x05, 0x04, 0x02, 0x0e, 0x07, 0x0a, 0x09, 0x0d, 0x0f, 0x0c, 0x03, 0x01, 0x00, 0x08)
#define SMALL_AES36_RCON(round)         vsetr8(SMALL_AES_ROUND_CONSTANTS[round], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
#define SMALL_AES36_SHIFT_ROWS          vsetr8(0, 4, 8, 3, 7, 2, 6, 1, 5, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff)
#define SMALL_AES36_TIMES_TWO           vsetr8(0x0, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x3, 0x1, 0x7, 0x5, 0xb, 0x9, 0xf, 0xd)
#define SMALL_AES36_TIMES_THREE         vsetr8(0x0, 0x3, 0x6, 0x5, 0xc, 0xf, 0xa, 0x9, 0xb, 0x8, 0xd, 0xe, 0x7, 0x4, 0x1, 0x2)
    
    // ---------------------------------------------------------------------
    // Types
    // ---------------------------------------------------------------------
    
    typedef uint8_t small_aes36_key_t[SMALL_AES36_NUM_KEY_BYTES];
    typedef uint8_t small_aes36_state_t[SMALL_AES36_NUM_STATE_BYTES];
    
    typedef struct {
        uint64_t subkeys[SMALL_AES36_NUM_ROUND_KEYS];
    } small_aes36_context_t;
    
    typedef struct {
        __m128i subkeys[SMALL_AES36_NUM_ROUND_KEYS];
    } small_aes36_sse_context_t;
    
    // ---------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_to_integer(const small_aes36_state_t input);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_to_byte_array(small_aes36_state_t output,
                                   uint64_t input);
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_mix_columns(uint64_t state);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_key_schedule(small_aes36_context_t *ctx,
                                  const small_aes36_key_t key);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt(const small_aes36_context_t *ctx,
                             const small_aes36_state_t plaintext,
                             small_aes36_state_t ciphertext);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt(const small_aes36_context_t *ctx,
                             const small_aes36_state_t plaintext,
                             small_aes36_state_t ciphertext,
                             size_t num_rounds);
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_encrypt_round(uint64_t round_key,
                                       uint64_t input);
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_encrypt_final_round(uint64_t round_key,
                                             uint64_t input);
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_decrypt_round(uint64_t round_key,
                                       uint64_t input);
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_decrypt_final_round(uint64_t round_key,
                                             uint64_t input);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt_final_round_without_mc(
        const small_aes36_context_t *ctx,
        const small_aes36_state_t plaintext,
        small_aes36_state_t ciphertext,
        size_t num_rounds);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_decrypt(const small_aes36_context_t *ctx,
                             const small_aes36_state_t ciphertext,
                             small_aes36_state_t plaintext);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_decrypt(const small_aes36_context_t *ctx,
                             const small_aes36_state_t ciphertext,
                             small_aes36_state_t plaintext,
                             size_t num_rounds);
    
    // ---------------------------------------------------------------------
    // SSE Functions
    // ---------------------------------------------------------------------
    
    void small_aes36_key_schedule(small_aes36_sse_context_t *ctx,
                                  const small_aes36_key_t key);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt(const small_aes36_sse_context_t *ctx,
                             const small_aes36_state_t plaintext,
                             small_aes36_state_t ciphertext);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt(const small_aes36_sse_context_t *ctx,
                             const small_aes36_state_t plaintext,
                             small_aes36_state_t ciphertext,
                             size_t num_rounds);
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_encrypt(const small_aes36_sse_context_t *ctx,
                                 uint64_t plaintext);
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_encrypt(const small_aes36_sse_context_t *ctx,
                                 uint64_t plaintext,
                                 size_t num_rounds);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_decrypt(const small_aes36_sse_context_t *ctx,
                             const small_aes36_state_t plaintext,
                             small_aes36_state_t ciphertext);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_decrypt(const small_aes36_sse_context_t *ctx,
                             const small_aes36_state_t plaintext,
                             small_aes36_state_t ciphertext,
                             size_t num_rounds);
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_decrypt(const small_aes36_sse_context_t *ctx,
                                 uint64_t ciphertext);
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_decrypt(const small_aes36_sse_context_t *ctx,
                                 uint64_t ciphertext,
                                 size_t num_rounds);
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt_final_round_without_mc(
        const small_aes36_sse_context_t *ctx,
        const small_aes36_state_t plaintext,
        small_aes36_state_t ciphertext,
        size_t num_rounds);
    
    // ---------------------------------------------------------------------
    
    __m128i small_aes36_encrypt_round(__m128i state, __m128i round_key);
    
    // ---------------------------------------------------------------------
    
    __m128i small_aes36_encrypt_final_round(__m128i state, __m128i round_key);
    
    // ---------------------------------------------------------------------
    
    __m128i small_aes36_shift_rows(__m128i state);
    
    // ---------------------------------------------------------------------
    
    __m128i small_aes36_sub_bytes(__m128i state);
    
    // ---------------------------------------------------------------------
    
    __m128i small_aes36_mix_columns(__m128i state);
    
}

#endif  // _SMALL_AES36_H_
