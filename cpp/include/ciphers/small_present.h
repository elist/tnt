/**
 * Commons of SMALL-PRESENT.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#ifndef _SMALL_PRESENT_H
#define _SMALL_PRESENT_H

// ---------------------------------------------------------------------

#include <cstdint>
#include <cstdlib>

// ---------------------------------------------------------------------

namespace ciphers {

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

#define SMALL_PRESENT_NUM_KEY_BYTES        10
#define SMALL_PRESENT_NUM_ROUNDS           31
#define SMALL_PRESENT_NUM_ROUND_KEYS       32
    
    const uint8_t SMALL_PRESENT_SBOX[16] = {
        0x0c, 0x05, 0x06, 0x0b, 0x09, 0x00, 0x0a, 0x0d,
        0x03, 0x0e, 0x0f, 0x08, 0x04, 0x07, 0x01, 0x02
    };
    
    const uint8_t SMALL_PRESENT_INVERSE_SBOX[16] = {
        0x05, 0x0e, 0x0f, 0x08, 0x0c, 0x01, 0x02, 0x0d,
        0x0b, 0x04, 0x06, 0x03, 0x00, 0x07, 0x09, 0x0a
    };
    const size_t SMALL_PRESENT_PERMUTATION[16] = {
        0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15
    };

}

// ---------------------------------------------------------------------

#endif // _SMALL_PRESENT_H
