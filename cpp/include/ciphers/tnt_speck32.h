/**
 * C implementation of TNT with Speck-32.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#ifndef  _TNT_SPECK_32_H_
#define  _TNT_SPECK_32_H_

// ---------------------------------------------------------------------

#include <stdint.h>
#include <stdlib.h>

#include "ciphers/speck32.h"

namespace ciphers {

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

#define TNT_NUM_ROUNDS                3
#define TNT_SPECK_32_NUM_KEY_BYTES    TNT_NUM_ROUNDS * SPECK_32_NUM_KEY_BYTES
#define TNT_SPECK_32_NUM_STATE_BYTES  SPECK_32_NUM_STATE_BYTES
#define TNT_SPECK_32_NUM_TWEAK_BYTES  SPECK_32_NUM_STATE_BYTES
    
    // ---------------------------------------------------------
    // Types
    // ---------------------------------------------------------
    
    typedef struct {
        speck32_context_t permutation_ctx[TNT_NUM_ROUNDS];
    } tnt_speck32_context_t;
    
    typedef uint8_t tnt_speck32_key_t[TNT_SPECK_32_NUM_KEY_BYTES];
    typedef uint8_t tnt_speck32_state_t[TNT_SPECK_32_NUM_STATE_BYTES];
    typedef uint8_t tnt_speck32_tweak_t[TNT_SPECK_32_NUM_TWEAK_BYTES];
    
    // ---------------------------------------------------------
    // API
    // ---------------------------------------------------------
    
    void tnt_speck32_key_schedule(tnt_speck32_context_t *ctx,
                                  const tnt_speck32_key_t key);
    
    // ---------------------------------------------------------
    
    void tnt_speck32_encrypt(const tnt_speck32_context_t *ctx,
                             const tnt_speck32_tweak_t tweak,
                             const tnt_speck32_state_t plaintext,
                             tnt_speck32_state_t ciphertext);
    
    // ---------------------------------------------------------
    
    void tnt_speck32_decrypt(const tnt_speck32_context_t *ctx,
                             const tnt_speck32_tweak_t tweak,
                             const tnt_speck32_state_t ciphertext,
                             tnt_speck32_state_t plaintext);
    
}

// ---------------------------------------------------------------------

#endif // _TNT_SPECK_32_H_
