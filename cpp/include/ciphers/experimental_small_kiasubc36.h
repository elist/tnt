/**
 * C implementation of Small-AES.
 * 
 * __author__ = anonymized
 * __date__   = 2019-05
 * __copyright__ = Creative Commons CC0
 */

#ifndef _EXPERIMENTAL_SMALL_KIASUBC36_H_
#define _EXPERIMENTAL_SMALL_KIASUBC36_H_

// ---------------------------------------------------------------------

#include <cstdint>

#include "ciphers/small_aes36.h"
#include "utils/utils.h"

namespace ciphers {

#define EXPERIMENTAL_SMALL_KIASUBC36_NUM_ROUNDS       5
#define EXPERIMENTAL_SMALL_KIASUBC36_NUM_ROUND_KEYS   6
    
    // ---------------------------------------------------------------------
    // Constants
    // ---------------------------------------------------------------------
    
    // ---------------------------------------------------------------------
    // Types
    // ---------------------------------------------------------------------
    
    // ---------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------
    
    void experimental_small_kiasubc36_key_schedule(small_aes36_context_t *ctx,
                                                   const small_aes36_key_t key);
    
    // ---------------------------------------------------------------------
    
    uint64_t
    experimental_small_kiasubc36_encrypt(const small_aes36_context_t *ctx,
                                         const uint64_t tweak,
                                         const uint64_t plaintext);
    
}

#endif  // _EXPERIMENTAL_SMALL_KIASUBC36_H_
