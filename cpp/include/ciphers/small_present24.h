/**
 * C implementation of SMALL-PRESENT24.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#ifndef  _SMALL_PRESENT24_H_
#define  _SMALL_PRESENT24_H_

// ---------------------------------------------------------------------

#include <cstdint>
#include <cstdlib>

#include "ciphers/small_present.h"

// ---------------------------------------------------------------------

namespace ciphers {

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

#define SMALL_PRESENT24_NUM_KEY_BYTES        SMALL_PRESENT_NUM_KEY_BYTES
#define SMALL_PRESENT24_NUM_ROUND_KEY_BYTES  3
#define SMALL_PRESENT24_NUM_STATE_BYTES      3
#define SMALL_PRESENT24_NUM_ROUNDS           SMALL_PRESENT_NUM_ROUNDS
#define SMALL_PRESENT24_NUM_ROUND_KEYS       SMALL_PRESENT_NUM_ROUND_KEYS
    
    static const size_t SMALL_PRESENT24_PERMUTATION[24] = {
        23, 19, 15, 11,  7,  3,
        22, 18, 14, 10,  6,  2,
        21, 17, 13,  9,  5,  1,
        20, 16, 12,  8,  4,  0
    };
    
    static const size_t SMALL_PRESENT24_INVERSE_PERMUTATION[24] = {
        23, 17, 11,  5,
        22, 16, 10,  4,
        21, 15,  9,  3,
        20, 14,  8,  2,
        19, 13,  7,  1,
        18, 12,  6,  0
    };
    
    // ---------------------------------------------------------
    // Types
    // ---------------------------------------------------------
    
    typedef uint8_t small_present24_key_t[SMALL_PRESENT24_NUM_KEY_BYTES];
    typedef uint8_t small_present24_state_t[SMALL_PRESENT24_NUM_STATE_BYTES];
    
    typedef struct {
        uint32_t subkeys[SMALL_PRESENT24_NUM_ROUND_KEYS];
    } small_present24_context_t;
    
    // ---------------------------------------------------------
    // API
    // ---------------------------------------------------------
    
    // ---------------------------------------------------------
    
    uint32_t small_present24_sbox_layer(uint32_t state);
    
    // ---------------------------------------------------------
    
    uint32_t small_present24_inverse_sbox_layer(uint32_t state);
    
    // ---------------------------------------------------------
    
    uint32_t small_present24_permutation_layer(uint32_t state);
    
    // ---------------------------------------------------------
    
    uint32_t small_present24_inverse_permutation_layer(uint32_t state);
    
    // ---------------------------------------------------------
    
    void small_present24_key_schedule(small_present24_context_t *ctx,
                                      const small_present24_key_t key);
    
    // ---------------------------------------------------------
    
    void small_present24_encrypt(const small_present24_context_t *ctx,
                                 const small_present24_state_t plaintext,
                                 small_present24_state_t ciphertext);
    
    // ---------------------------------------------------------
    
    void small_present24_encrypt(const small_present24_context_t *ctx,
                                 const small_present24_state_t plaintext,
                                 small_present24_state_t ciphertext,
                                 size_t num_rounds);
    
    // ---------------------------------------------------------
    
    void small_present24_decrypt(const small_present24_context_t *ctx,
                                 const small_present24_state_t ciphertext,
                                 small_present24_state_t plaintext);
    
    // ---------------------------------------------------------
    
    void small_present24_decrypt(const small_present24_context_t *ctx,
                                 const small_present24_state_t ciphertext,
                                 small_present24_state_t plaintext,
                                 size_t num_rounds);
    
}

// ---------------------------------------------------------------------

#endif // _SMALL_PRESENT24_H_
