/**
 * C implementation of Speck-32.
 *
 * __author__ = anonymized
 * __date__   = 2019-05
 * __copyright__ = Creative Commons CC0
 */

#ifndef  _SPECK32_H_
#define  _SPECK32_H_

// ---------------------------------------------------------------------

#include <stdint.h>
#include <stdlib.h>

namespace ciphers {

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

#define SPECK_32_NUM_KEY_BYTES         8
#define SPECK_32_NUM_ROUND_KEY_BYTES   2
#define SPECK_32_NUM_STATE_BYTES       4
#define SPECK_32_NUM_ROUNDS           22
#define SPECK_32_NUM_ROUND_KEYS       22
#define SPECK_32_ROT_ALPHA             7
#define SPECK_32_ROT_BETA              2
    
    // ---------------------------------------------------------
    // Types
    // ---------------------------------------------------------
    
    typedef struct {
        uint16_t subkeys[SPECK_32_NUM_ROUNDS];
    } speck32_context_t;
    
    typedef uint8_t speck32_key_t[SPECK_32_NUM_KEY_BYTES];
    typedef uint8_t speck32_state_t[SPECK_32_NUM_STATE_BYTES];
    
    // ---------------------------------------------------------
    // API
    // ---------------------------------------------------------
    
    void speck32_round(uint16_t *left,
                       uint16_t *right,
                       const uint16_t *round_key);
    
    // ---------------------------------------------------------
    
    void speck32_inverse_round(uint16_t *left,
                               uint16_t *right,
                               const uint16_t *round_key);
    
    // ---------------------------------------------------------
    
    void speck32_key_schedule(speck32_context_t *ctx,
                              const uint8_t *key);
    
    // ---------------------------------------------------------
    
    void speck32_encrypt_rounds(const speck32_context_t *ctx,
                                const uint8_t *plaintext,
                                uint8_t *ciphertext,
                                size_t num_rounds);
    
    // ---------------------------------------------------------
    
    void speck32_decrypt_rounds(const speck32_context_t *ctx,
                                const uint8_t *ciphertext,
                                uint8_t *plaintext,
                                size_t num_rounds);
    
    // ---------------------------------------------------------
    
    void speck32_encrypt(const speck32_context_t *ctx,
                         const uint8_t plaintext[SPECK_32_NUM_STATE_BYTES],
                         uint8_t ciphertext[SPECK_32_NUM_STATE_BYTES]);
    
    // ---------------------------------------------------------
    
    void speck32_decrypt(const speck32_context_t *ctx,
                         const uint8_t ciphertext[SPECK_32_NUM_STATE_BYTES],
                         uint8_t plaintext[SPECK_32_NUM_STATE_BYTES]);
    
}

// ---------------------------------------------------------------------

#endif // _SPECK32_H_
