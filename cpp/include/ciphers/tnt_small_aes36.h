/**
 * C implementation of TNT with Small-PRESENT-24.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#ifndef  _TNT_SMALL_AES36_H_
#define  _TNT_SMALL_AES36_H_

// ---------------------------------------------------------------------

#include <cstdint>
#include <cstdlib>

#include "ciphers/small_aes36.h"
#include "ciphers/tnt.h"

namespace ciphers {

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

#define TNT_SMALL_AES36_NUM_KEY_BYTES    TNT_NUM_ROUNDS * SMALL_AES36_NUM_KEY_BYTES
#define TNT_SMALL_AES36_NUM_STATE_BYTES  SMALL_AES36_NUM_STATE_BYTES
#define TNT_SMALL_AES36_NUM_TWEAK_BYTES  SMALL_AES36_NUM_STATE_BYTES
#define TNT_SMALL_AES36_NUM_PART_ROUNDS  5
    
    // ---------------------------------------------------------
    // Types
    // ---------------------------------------------------------
    
    typedef struct {
        small_aes36_context_t permutation_ctx[TNT_NUM_ROUNDS];
    } tnt_small_aes36_context_t;
    
    typedef struct {
        small_aes36_sse_context_t permutation_ctx[TNT_NUM_ROUNDS];
    } tnt_small_aes36_sse_context_t;
    
    typedef uint8_t tnt_small_aes36_key_t[
        TNT_SMALL_AES36_NUM_KEY_BYTES];
    typedef uint8_t tnt_small_aes36_state_t[TNT_SMALL_AES36_NUM_STATE_BYTES];
    typedef uint8_t tnt_small_aes36_tweak_t[TNT_SMALL_AES36_NUM_TWEAK_BYTES];
    
    // ---------------------------------------------------------
    // API
    // ---------------------------------------------------------
    
    void tnt_small_aes36_key_schedule(tnt_small_aes36_context_t *ctx,
                                      const tnt_small_aes36_key_t key);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt_first_part(
        const tnt_small_aes36_context_t *ctx,
        const tnt_small_aes36_tweak_t tweak,
        const tnt_small_aes36_state_t plaintext,
        tnt_small_aes36_state_t ciphertext);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt_first_part(
        const tnt_small_aes36_context_t *ctx,
        const tnt_small_aes36_tweak_t tweak,
        const tnt_small_aes36_state_t plaintext,
        tnt_small_aes36_state_t ciphertext,
        size_t num_first_rounds);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt(const tnt_small_aes36_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t plaintext,
                                 tnt_small_aes36_state_t ciphertext);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt(const tnt_small_aes36_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t plaintext,
                                 tnt_small_aes36_state_t ciphertext,
                                 size_t num_first_rounds);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_decrypt(const tnt_small_aes36_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t ciphertext,
                                 tnt_small_aes36_state_t plaintext);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_decrypt(const tnt_small_aes36_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t ciphertext,
                                 tnt_small_aes36_state_t plaintext,
                                 size_t num_first_rounds);
    
    // ---------------------------------------------------------
    // SSE Functions
    // ---------------------------------------------------------
    
    void tnt_small_aes36_key_schedule(tnt_small_aes36_sse_context_t *ctx,
                                      const tnt_small_aes36_key_t key);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt_first_part(
        const tnt_small_aes36_sse_context_t *ctx,
        const tnt_small_aes36_tweak_t tweak,
        const tnt_small_aes36_state_t plaintext,
        tnt_small_aes36_state_t ciphertext);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt_first_part(
        const tnt_small_aes36_sse_context_t *ctx,
        const tnt_small_aes36_tweak_t tweak,
        const tnt_small_aes36_state_t plaintext,
        tnt_small_aes36_state_t ciphertext,
        size_t num_first_rounds);
    
    // ---------------------------------------------------------
    
    uint64_t tnt_small_aes36_encrypt_first_part(
        const tnt_small_aes36_sse_context_t *ctx,
        const uint64_t tweak,
        const uint64_t plaintext);
    
    // ---------------------------------------------------------
    
    uint64_t tnt_small_aes36_encrypt_first_two_parts(
        const tnt_small_aes36_sse_context_t *ctx,
        uint64_t tweak,
        uint64_t plaintext);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt(const tnt_small_aes36_sse_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t plaintext,
                                 tnt_small_aes36_state_t ciphertext);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt(const tnt_small_aes36_sse_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t plaintext,
                                 tnt_small_aes36_state_t ciphertext,
                                 size_t num_first_rounds);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_decrypt(const tnt_small_aes36_sse_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t ciphertext,
                                 tnt_small_aes36_state_t plaintext);
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_decrypt(const tnt_small_aes36_sse_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t ciphertext,
                                 tnt_small_aes36_state_t plaintext,
                                 size_t num_first_rounds);
    
}

// ---------------------------------------------------------------------

#endif // _TNT_SMALL_AES36_H_
