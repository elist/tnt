/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdint>
#include <gtest/gtest.h>

#include "ciphers/small_present24.h"
#include "utils/utils.h"


using ciphers::small_present24_context_t;
using ciphers::small_present24_key_t;
using ciphers::small_present24_state_t;
using utils::assert_equal;

// ---------------------------------------------------------

static
void run_encryption_test(const small_present24_key_t key,
                         const small_present24_state_t plaintext,
                         const small_present24_state_t expected_ciphertext) {
    small_present24_context_t ctx;
    small_present24_key_schedule(&ctx, key);
    
    small_present24_state_t ciphertext;
    small_present24_encrypt(&ctx, plaintext, ciphertext);
    ASSERT_TRUE(assert_equal(expected_ciphertext, ciphertext,
                             (size_t) SMALL_PRESENT24_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

static
void run_decryption_test(const small_present24_key_t key,
                         const small_present24_state_t ciphertext,
                         const small_present24_state_t expected_plaintext) {
    small_present24_context_t ctx;
    small_present24_key_schedule(&ctx, key);
    
    small_present24_state_t plaintext;
    small_present24_decrypt(&ctx, ciphertext, plaintext);
    ASSERT_TRUE(assert_equal(expected_plaintext, plaintext,
                             (size_t) SMALL_PRESENT24_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

TEST(SMALL_PRESENT24, test_key_schedule) {
    const small_present24_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    small_present24_context_t ctx;
    small_present24_key_schedule(&ctx, key);
    
    uint8_t round_key[SMALL_PRESENT24_NUM_ROUND_KEY_BYTES];

    const uint8_t expected_round_keys[
        SMALL_PRESENT24_NUM_ROUND_KEY_BYTES * SMALL_PRESENT24_NUM_ROUND_KEYS
    ] = {
            0x00, 0x00, 0x00,  0x00, 0x00, 0x00,  0x00, 0x00, 0x01,  0x00, 0x00, 0x01,
            0x40, 0x00, 0x62,  0x80, 0x00, 0x2a,  0xc0, 0x00, 0x33,  0x40, 0x00, 0x5b,
            0x00, 0x06, 0x4c,  0x80, 0x02, 0x84,  0x40, 0x03, 0x55,  0xc0, 0x05, 0xed,
            0x80, 0x64, 0x9e,  0xc0, 0x28, 0x76,  0x00, 0x35, 0x7f,  0x00, 0x5e, 0xc7,
            0xc6, 0x49, 0xa8,  0x42, 0x87, 0x30,  0x83, 0x57, 0x81,  0x05, 0xec, 0x19,
            0x24, 0x9a, 0xea,  0xa8, 0x73, 0x6e,  0xf5, 0x78, 0x3e,  0x1e, 0xc1, 0xd5,
            0x09, 0xae, 0x8f,  0x07, 0x36, 0xad,  0xd7, 0x83, 0xad,  0xac, 0x1d, 0x77,
            0x1a, 0xe8, 0x9b,  0x33, 0x6a, 0xcd,  0x78, 0x3a, 0xc9,  0x41, 0xd7, 0x00
        };

    for (size_t i = 0; i < SMALL_PRESENT24_NUM_ROUND_KEYS; ++i) {
        const uint8_t *start_expected_round_key =
            expected_round_keys + i * SMALL_PRESENT24_NUM_ROUND_KEY_BYTES;
    
        round_key[0] = (ctx.subkeys[i] >> 16) & 0xFF;
        round_key[1] = (ctx.subkeys[i] >>  8) & 0xFF;
        round_key[2] = (ctx.subkeys[i]) & 0xFF;

        ASSERT_TRUE(utils::assert_equal(start_expected_round_key, round_key,
                                        SMALL_PRESENT24_NUM_ROUND_KEY_BYTES));
    }
}

// ---------------------------------------------------------

TEST(SMALL_PRESENT24, test_encrypt_full) {
    const small_present24_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    const small_present24_state_t plaintext = {
        0x00, 0x00, 0x00
    };
    const small_present24_state_t expected_ciphertext = {
        0x06, 0xe3, 0xac
    };
    run_encryption_test(key, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(SMALL_PRESENT24, test_decrypt_full) {
    const small_present24_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    const small_present24_state_t ciphertext = {
        0x06, 0xe3, 0xac
    };
    const small_present24_state_t expected_plaintext = {
        0x00, 0x00, 0x00
    };
    run_decryption_test(key, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// ---------------------------------------------------------
