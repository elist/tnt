/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdint>
#include <gtest/gtest.h>

#include "ciphers/small_present16.h"
#include "utils/utils.h"


using ciphers::small_present16_context_t;
using ciphers::small_present16_key_t;
using ciphers::small_present16_state_t;
using utils::assert_equal;

// ---------------------------------------------------------

static void run_encryption_test(const small_present16_key_t key,
                                const small_present16_state_t plaintext,
                                const small_present16_state_t expected_ciphertext) {
    small_present16_context_t ctx;
    small_present16_key_schedule(&ctx, key);
    
    small_present16_state_t ciphertext;
    small_present16_encrypt(&ctx, plaintext, ciphertext);
    ASSERT_TRUE(assert_equal(expected_ciphertext, ciphertext,
                             (size_t) SMALL_PRESENT16_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

static void run_decryption_test(const small_present16_key_t key,
                                const small_present16_state_t ciphertext,
                                const small_present16_state_t expected_plaintext) {
    small_present16_context_t ctx;
    small_present16_key_schedule(&ctx, key);
    
    small_present16_state_t plaintext;
    small_present16_decrypt(&ctx, ciphertext, plaintext);
    ASSERT_TRUE(assert_equal(expected_plaintext, plaintext,
                             (size_t) SMALL_PRESENT16_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

TEST(SMALL_PRESENT16, test_key_schedule) {
    const small_present16_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    small_present16_context_t ctx;
    small_present16_key_schedule(&ctx, key);

    uint8_t round_key[SMALL_PRESENT16_NUM_ROUND_KEY_BYTES];

    const uint8_t expected_round_keys[
        SMALL_PRESENT16_NUM_ROUND_KEY_BYTES * SMALL_PRESENT16_NUM_ROUND_KEYS
    ] = {
            0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01,
            0x00, 0x62, 0x00, 0x2a, 0x00, 0x33, 0x00, 0x5b,
            0x06, 0x4c, 0x02, 0x84, 0x03, 0x55, 0x05, 0xed,
            0x64, 0x9e, 0x28, 0x76, 0x35, 0x7f, 0x5e, 0xc7,
            0x49, 0xa8, 0x87, 0x30, 0x57, 0x81, 0xec, 0x19,
            0x9a, 0xea, 0x73, 0x6e, 0x78, 0x3e, 0xc1, 0xd5,
            0xae, 0x8f, 0x36, 0xad, 0x83, 0xad, 0x1d, 0x77,
            0xe8, 0x9b, 0x6a, 0xcd, 0x3a, 0xc9, 0xd7, 0x00
        };

    for (size_t i = 0; i < SMALL_PRESENT16_NUM_ROUND_KEYS; ++i) {
        const uint8_t *start_expected_round_key =
            expected_round_keys + i * SMALL_PRESENT16_NUM_ROUND_KEY_BYTES;
        utils::to_uint8(round_key,
                        reinterpret_cast<const uint16_t *>(&(ctx.subkeys[i])),
                        SMALL_PRESENT16_NUM_ROUND_KEY_BYTES);

        ASSERT_TRUE(utils::assert_equal(start_expected_round_key, round_key,
                                        SMALL_PRESENT16_NUM_ROUND_KEY_BYTES));
    }
}

// ---------------------------------------------------------

TEST(SMALL_PRESENT16, test_encrypt_full) {
    const small_present16_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    const small_present16_state_t plaintext = {
        0x00, 0x00
    };
    const small_present16_state_t expected_ciphertext = {
        0x50, 0x96
    };
    run_encryption_test(key, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(SMALL_PRESENT16, test_decrypt_full) {
    const small_present16_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    const small_present16_state_t ciphertext = {
        0x50, 0x96
    };
    const small_present16_state_t expected_plaintext = {
        0x00, 0x00
    };
    run_decryption_test(key, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// ---------------------------------------------------------
