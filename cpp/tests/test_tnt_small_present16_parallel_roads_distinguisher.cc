/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <array>
#include <functional>
#include <memory>
#include <vector>
#include <cstdint>
#include <cstdlib>

#include "ciphers/small_present16.h"
#include "ciphers/tnt_small_present16.h"
#include "utils/argparse.h"
#include "utils/utils.h"
#include "utils/xorshift1024.h"


using ciphers::tnt_small_present16_context_t;
using ciphers::tnt_small_present16_state_t;
using ciphers::tnt_small_present16_tweak_t;
using ciphers::tnt_small_present16_key_t;
using utils::assert_equal;
using utils::compute_mean;
using utils::compute_variance;
using utils::xor_arrays;
using utils::zeroize_array;
using utils::ArgumentParser;

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

static const size_t NUM_BITS = 16;
static const auto NUM_CIPHERTEXT_VALUES = static_cast<size_t>(1L) << NUM_BITS;

// ---------------------------------------------------------
// Types
// ---------------------------------------------------------

typedef struct {
    tnt_small_present16_key_t key;
    tnt_small_present16_context_t cipher_ctx;
    size_t num_keys = 0;
    size_t num_texts_per_set = 0;
    size_t num_texts_per_set_log = 0;
    std::vector<size_t> num_matches;
    bool be_verbose = false;
    bool use_tprp = false;
} ExperimentContext;

typedef struct {
    std::vector<size_t> num_collisions_per_set;
    size_t num_collisions = 0;
} ExperimentResult;

typedef uint16_t Ciphertext;
typedef std::vector<Ciphertext> CiphertextsVector;
typedef std::array<size_t, NUM_CIPHERTEXT_VALUES> Table;
typedef std::shared_ptr<Table> TablePointer;
typedef std::array<CiphertextsVector, NUM_CIPHERTEXT_VALUES> TweakIndices;
typedef std::shared_ptr<TweakIndices> TweakIndicesPointer;


typedef std::function<void(const tnt_small_present16_context_t *,
                           tnt_small_present16_tweak_t,
                           tnt_small_present16_state_t,
                           tnt_small_present16_state_t)> EncryptFunction;

// ---------------------------------------------------------
// Functions
// ---------------------------------------------------------

static uint16_t to_integer(const tnt_small_present16_state_t state) {
    uint16_t result = 0;
    result |= static_cast<uint16_t>(state[0]) << 8;
    result |= static_cast<uint16_t>(state[1]);
    return result;
}

// ---------------------------------------------------------

static void to_array(tnt_small_present16_state_t result, const uint16_t state) {
    result[0] = static_cast<uint16_t>((state >> 8) & 0xFF);
    result[1] = static_cast<uint16_t>(state & 0xFF);
}

// ---------------------------------------------------------

static void generate_set0_tweak(tnt_small_present16_state_t tweak,
                                const size_t index) {
    to_array(tweak, static_cast<uint16_t>(index));
}

// ---------------------------------------------------------

/**
 * num_bits = log(num_sets_per_key), e.g. 11, 12, 13.
 * @param tweak
 * @param index
 * @param num_bits
 */
static void generate_set1_tweak(tnt_small_present16_state_t tweak,
                                const size_t index,
                                const size_t num_bits) {
    const size_t shift = NUM_BITS - num_bits; // e.g. 5, 4, 3
    const uint16_t shifted_index = index << shift;
    to_array(tweak, shifted_index);
}

// ---------------------------------------------------------

static size_t generate_set0_tweak_as_int(const size_t index) {
    return index;
}

// ---------------------------------------------------------

/**
 * num_bits = log(num_sets_per_key), e.g. 11, 12, 13.
 * @param tweak
 * @param index
 * @param num_bits
 */
static size_t generate_set1_tweak_as_int(const size_t index,
                                         const size_t num_bits) {
    return index << (NUM_BITS - num_bits);
}

// ---------------------------------------------------------

static void encrypt_random(const tnt_small_present16_context_t *ctx,
                           const tnt_small_present16_tweak_t tweak,
                           const tnt_small_present16_state_t plaintext,
                           tnt_small_present16_state_t ciphertext) {
    // Suppress unused parameter warnings. We need those parameters only to
    // comply with the real encryption function.
    (void) ctx;
    (void) tweak;
    (void) plaintext;
    utils::get_random_bytes(ciphertext, TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
}

// ---------------------------------------------------------

static void encrypt_real(const tnt_small_present16_context_t *ctx,
                         const tnt_small_present16_tweak_t tweak,
                         const tnt_small_present16_state_t plaintext,
                         tnt_small_present16_state_t ciphertext) {
    ciphers::tnt_small_present16_encrypt(ctx, tweak, plaintext, ciphertext);
}

// ---------------------------------------------------------

static void zeroize_table(TablePointer &table) {
    for (size_t i = 0; i < NUM_CIPHERTEXT_VALUES; ++i) {
        (*table)[i] = 0;
    }
}

// ---------------------------------------------------------

static void clear_lists(TweakIndicesPointer &lists) {
    lists->fill(CiphertextsVector());
}

// ---------------------------------------------------------

static size_t find_num_collisions(const TweakIndicesPointer &lists,
                                  TablePointer &table,
                                  const uint16_t t_k,
                                  const uint16_t ciphertext,
                                  const size_t num_texts_per_set_log) {
    const CiphertextsVector &ciphertexts = (*lists)[ciphertext];
    const size_t num_colliding_ciphertexts = ciphertexts.size();
    
    if (num_colliding_ciphertexts == 0) {
        return 0;
    }
    
    size_t num_collisions = 0;
    
    for (size_t i = 0; i < num_colliding_ciphertexts; ++i) {
        const size_t t_ell_index = ciphertexts[i];
        const size_t t_ell = generate_set1_tweak_as_int(t_ell_index, 
            num_texts_per_set_log);
        
        const size_t delta_t = t_k ^ t_ell;
        num_collisions += (*table)[delta_t];
    }
    
    return num_collisions;
}

// ---------------------------------------------------------

static void increment_num_collisions(const TweakIndicesPointer &lists,
                                     TablePointer &table,
                                     const uint16_t t_i,
                                     const uint16_t ciphertext) {
    const CiphertextsVector &ciphertexts = (*lists)[ciphertext];
    const size_t num_colliding_ciphertexts = ciphertexts.size();
    
    if (num_colliding_ciphertexts == 0) {
        return;
    }
    
    for (size_t i = 0; i < num_colliding_ciphertexts; ++i) {
        const size_t t_index = ciphertexts[i];
        const size_t t_j = generate_set0_tweak_as_int(t_index);
        const size_t delta_t = t_i ^ t_j;
        (*table)[delta_t]++;
    }
}

// ---------------------------------------------------------

static size_t generate_lists(ExperimentContext *context,
                             const EncryptFunction &encryption_function,
                             TweakIndicesPointer &lists) {
    // ---------------------------------------------------------
    // Prepare the table
    // ---------------------------------------------------------
    
    TablePointer num_pairs_per_difference(new Table());
    zeroize_table(num_pairs_per_difference);
    size_t num_total_collisions = 0;
    
    // ---------------------------------------------------------
    // Key setup, random key
    // ---------------------------------------------------------
    
    tnt_small_present16_context_t cipher_ctx = context->cipher_ctx;
    tnt_small_present16_key_t key;
    
    utils::get_random_bytes(key, TNT_SMALL_PRESENT16_NUM_KEY_BYTES);
    tnt_small_present16_key_schedule(&cipher_ctx, key);
    
    // ---------------------------------------------------------
    // Log
    // ---------------------------------------------------------
    
    if (context->be_verbose) {
        utils::print_hex("# Key", key, TNT_SMALL_PRESENT16_NUM_KEY_BYTES);
    }
    
    // ---------------------------------------------------------
    // Fill lists with encryptions
    // ---------------------------------------------------------
    
    tnt_small_present16_state_t tweak;
    tnt_small_present16_state_t ciphertext;
    Ciphertext ciphertext_as_int;
    
    if (context->be_verbose) {
        printf("#    i    j    t    p    c\n");
    }
    
    // ---------------------------------------------------------
    // Random message M^0
    // ---------------------------------------------------------
    
    tnt_small_present16_state_t plaintext;
    utils::get_random_bytes(plaintext, TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
    
    // ---------------------------------------------------------
    // Fill list with encryptions of first (T^i, M) pairs
    // ---------------------------------------------------------
    
    if (context->be_verbose) {
        utils::print_hex("# M^0", plaintext,
                         TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
    }
    
    for (size_t i = 0; i < context->num_texts_per_set; ++i) {
        generate_set0_tweak(tweak, i);
        size_t t = generate_set0_tweak_as_int(i);
        encryption_function(&cipher_ctx, tweak, plaintext, ciphertext);
        ciphertext_as_int = to_integer(ciphertext);

        increment_num_collisions(lists,
                                 num_pairs_per_difference,
                                 t,
                                 ciphertext_as_int);
        (*lists)[ciphertext_as_int].push_back(i);
    }
    
    // ---------------------------------------------------------
    // Encrypt second list of (T^j, M') pairs.
    // Find collisions (C^i, C^j), where C^i corresponds to some (T^i, M)
    // on-the-fly, determine for each C^j a = the number of C^i's it collides
    // with, compute delta = T^i xor T^j, add table[delta] to the number of
    // total collisions, and increase table[delta] += a.
    // ---------------------------------------------------------
    
    utils::get_random_bytes(plaintext, TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
    
    if (context->be_verbose) {
        utils::print_hex("# M^1", plaintext,
                         TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
    }

    clear_lists(lists);
    
    for (size_t i = 0; i < context->num_texts_per_set; ++i) {
        generate_set1_tweak(tweak, i, context->num_texts_per_set_log);
        const size_t t_prime = generate_set1_tweak_as_int(
            i, context->num_texts_per_set_log);
        
        encryption_function(&cipher_ctx, tweak, plaintext, ciphertext);
        ciphertext_as_int = to_integer(ciphertext);
        
        const size_t num_new_collisions = find_num_collisions(
            lists,
            num_pairs_per_difference,
            t_prime,
            ciphertext_as_int,
            context->num_texts_per_set_log);
        num_total_collisions += num_new_collisions;

        (*lists)[ciphertext_as_int].push_back(i);
    }
    
    return num_total_collisions;
}

// ---------------------------------------------------------

static size_t perform_experiment(ExperimentContext *context) {
    TweakIndicesPointer lists(new TweakIndices());
    const EncryptFunction encryption_function = context->use_tprp ?
                                                encrypt_random :
                                                encrypt_real;
    const size_t num_collisions = generate_lists(context,
                                                 encryption_function,
                                                 lists);
    return num_collisions;
}

// ---------------------------------------------------------

static void perform_experiments(ExperimentContext *context) {
    ExperimentResult all_results;
    all_results.num_collisions = 0;
    
    printf("# %8zu Experiments\n", context->num_keys);
    printf("# %8zu Sets/key\n", context->num_texts_per_set);
    printf("# Key Collisions Mean Variance \n");
    
    for (size_t i = 0; i < context->num_keys; ++i) {
        const size_t num_collisions = perform_experiment(context);
        const double mean =
            (double) num_collisions / (double) context->num_texts_per_set;
        
        all_results.num_collisions += num_collisions;
        all_results.num_collisions_per_set.push_back(num_collisions);
        
        printf("%4zu %8zu %8.4f\n", i + 1, num_collisions, mean);
    }
    
    const double mean = compute_mean(all_results.num_collisions_per_set);
    const double variance = compute_variance(
        all_results.num_collisions_per_set);
    
    printf("# Total Keys Collisions Mean Variance \n");
    printf("# %4zu %8zu %8.4f %8.8f\n",
           context->num_keys,
           all_results.num_collisions,
           mean,
           variance);
}

// ---------------------------------------------------------
// Argument parsing
// ---------------------------------------------------------

static void
parse_args(ExperimentContext *context, int argc, const char **argv) {
    ArgumentParser parser;
    parser.appName("Test for the TNT distinguisher.");
    parser.addArgument("-k", "--num_keys", 1, false);
    parser.addArgument("-s", "--num_texts_per_set", 1, false); // 2^12
    parser.addArgument("-r", "--use_tprp", 1, false);
    parser.addArgument("-v", "--verbose", 1, false);
    
    try {
        parser.parse((size_t) argc, argv);
        
        context->num_texts_per_set_log = parser.retrieveAsLong("s");
        context->num_texts_per_set = static_cast<const size_t>(1L)
            << context->num_texts_per_set_log;
        context->num_keys = parser.retrieveAsLong("k");
        context->use_tprp = static_cast<bool>(parser.retrieveAsInt("r"));
        context->be_verbose = static_cast<bool>(parser.retrieveAsInt("v"));
    } catch (...) {
        fprintf(stderr, "%s\n", parser.usage().c_str());
        exit(EXIT_FAILURE);
    }
    
    printf("# Keys     %8zu\n", context->num_keys);
    printf("# Sets/Key %8zu\n", context->num_texts_per_set);
    printf("# Use TPRP %8d\n", context->use_tprp);
    printf("# Verbose  %8d\n", context->be_verbose);
}

// ---------------------------------------------------------

int main(int argc, const char **argv) {
    ExperimentContext context;
    parse_args(&context, argc, argv);
    perform_experiments(&context);
    return EXIT_SUCCESS;
}
