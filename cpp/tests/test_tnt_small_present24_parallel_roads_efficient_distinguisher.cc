/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <array>
#include <functional>
#include <memory>
#include <vector>
#include <cstdint>
#include <cstdlib>

#include "ciphers/small_present24.h"
#include "ciphers/tnt_small_present24.h"
#include "utils/argparse.h"
#include "utils/utils.h"
#include "utils/xorshift1024.h"


using ciphers::tnt_small_present24_context_t;
using ciphers::tnt_small_present24_state_t;
using ciphers::tnt_small_present24_tweak_t;
using ciphers::tnt_small_present24_key_t;
using utils::assert_equal;
using utils::compute_mean;
using utils::compute_variance;
using utils::xor_arrays;
using utils::zeroize_array;
using utils::ArgumentParser;

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

static const size_t NUM_BITS = 24;
static const size_t NUM_LSB_BITS = 3 * NUM_BITS / 4; // 18
static const auto NUM_STORED_VALUES = static_cast<size_t>(1L) << NUM_LSB_BITS;

static const size_t LSB_MASK = (1L << NUM_LSB_BITS) - 1;
static const size_t MSB_MASK = ((1L << NUM_BITS) - 1) ^LSB_MASK;

// ---------------------------------------------------------
// Types
// ---------------------------------------------------------

typedef struct {
    tnt_small_present24_key_t key;
    tnt_small_present24_context_t cipher_ctx;
    size_t num_keys = 0;
    size_t num_texts_per_set = 0;
    size_t num_texts_per_set_log = 0;
    std::vector<size_t> num_matches;
    bool be_verbose = false;
    bool use_tprp = false;
} ExperimentContext;

typedef struct {
    std::vector<size_t> num_collisions_per_set;
    size_t num_collisions = 0;
} ExperimentResult;

typedef uint32_t Ciphertext;
typedef std::vector<Ciphertext> CiphertextsVector;
typedef std::array<std::vector<size_t>, NUM_STORED_VALUES> Table;
typedef std::shared_ptr<Table> TablePointer;

typedef std::pair<Ciphertext, Ciphertext> CiphertextTweakTuple;
typedef std::vector<CiphertextTweakTuple> CiphertextTweakTupleVector;
typedef std::array<CiphertextTweakTupleVector, NUM_STORED_VALUES>
    TweakIndices;
typedef std::shared_ptr<TweakIndices> TweakIndicesPointer;

typedef std::function<void(const tnt_small_present24_context_t *,
                           tnt_small_present24_tweak_t,
                           tnt_small_present24_state_t,
                           tnt_small_present24_state_t)> EncryptFunction;

// ---------------------------------------------------------
// Functions
// ---------------------------------------------------------

static uint32_t to_integer(const tnt_small_present24_state_t state) {
    uint32_t result = 0;
    result |= static_cast<uint32_t>(state[0]) << 16;
    result |= static_cast<uint32_t>(state[1]) << 8;
    result |= static_cast<uint32_t>(state[2]);
    return result;
}

// ---------------------------------------------------------

static void split(uint32_t &high,
                  uint32_t &lo,
                  const uint32_t &value) {
    high = (value & MSB_MASK) >> NUM_LSB_BITS;
    lo = value & LSB_MASK;
}

// ---------------------------------------------------------

static void to_array(tnt_small_present24_state_t result, const uint32_t state) {
    result[0] = static_cast<uint32_t>((state >> 16) & 0xFF);
    result[1] = static_cast<uint32_t>((state >> 8) & 0xFF);
    result[2] = static_cast<uint32_t>(state & 0xFF);
}

// ---------------------------------------------------------

static void generate_set0_tweak(tnt_small_present24_state_t tweak,
                                const size_t index) {
    to_array(tweak, static_cast<uint32_t>(index));
}

// ---------------------------------------------------------

/**
 * num_bits = log(num_sets_per_key), e.g. 11, 12, 13.
 * @param tweak
 * @param index
 * @param num_bits
 */
static void generate_set1_tweak(tnt_small_present24_state_t tweak,
                                const size_t index,
                                const size_t num_bits) {
    const size_t shift = NUM_BITS - num_bits; // e.g. 5, 4, 3
    const uint32_t shifted_index = index << shift;
    to_array(tweak, shifted_index);
}

// ---------------------------------------------------------

static size_t generate_set0_tweak_as_int(const size_t index) {
    return index;
}

// ---------------------------------------------------------

/**
 * num_bits = log(num_sets_per_key), e.g. 11, 12, 13.
 * @param tweak
 * @param index
 * @param num_bits
 */
static size_t generate_set1_tweak_as_int(const size_t index,
                                         const size_t num_bits) {
    return index << (NUM_BITS - num_bits);
}

// ---------------------------------------------------------

static void encrypt_random(const tnt_small_present24_context_t *ctx,
                           const tnt_small_present24_tweak_t tweak,
                           const tnt_small_present24_state_t plaintext,
                           tnt_small_present24_state_t ciphertext) {
    // Suppress unused parameter warnings. We need those parameters only to
    // comply with the real encryption function.
    (void) ctx;
    (void) tweak;
    (void) plaintext;
    utils::get_random_bytes(ciphertext, TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
}

// ---------------------------------------------------------

static void encrypt_real(const tnt_small_present24_context_t *ctx,
                         const tnt_small_present24_tweak_t tweak,
                         const tnt_small_present24_state_t plaintext,
                         tnt_small_present24_state_t ciphertext) {
    ciphers::tnt_small_present24_encrypt(ctx, tweak, plaintext, ciphertext);
}

// ---------------------------------------------------------

static void clear_lists(TweakIndicesPointer &lists) {
    lists->fill(CiphertextTweakTupleVector());
}

// ---------------------------------------------------------

static void clear_table(TablePointer &table) {
    table->fill(std::vector<size_t>());
}

// ---------------------------------------------------------

static
size_t find_num_collisions_in_d(TablePointer &table,
                                const uint32_t msb_delta_t,
                                const uint32_t lsb_delta_t,
                                size_t &num_find_num_collisions_in_d_calls) {
    const std::vector<size_t> &entries = (*table)[lsb_delta_t];
    size_t num_collisions = 0;
    
    for (const size_t &previous_msb_delta_t : entries) {
        if (previous_msb_delta_t == msb_delta_t) {
            num_collisions++;
        }
        
        num_find_num_collisions_in_d_calls++;
    }
    
    return num_collisions;
}

// ---------------------------------------------------------

static size_t find_num_collisions(const TweakIndicesPointer &lists,
                                  const Ciphertext msb_ciphertext_l,
                                  const Ciphertext lsb_ciphertext_l,
                                  const Ciphertext t_l,
                                  TablePointer &table,
                                  const size_t num_texts_per_set_log,
                                  size_t &num_find_num_collisions_calls,
                                  size_t &num_find_num_collisions_in_d_calls) {
    size_t num_collisions = 0;
    const CiphertextTweakTupleVector &previous_ciphertexts =
        (*lists)[lsb_ciphertext_l];
    
    for (const CiphertextTweakTuple &item : previous_ciphertexts) {
        if (item.second != msb_ciphertext_l) { // C^1_k != C^1_l
            continue;
        }
        
        num_find_num_collisions_calls++;
        
        // ---------------------------------------------------------
        // Add Delta T^1_{k,l} = (T^1_k, T^1_l)
        // msb_delta_t, lsb_delta_t = msb(Delta T^1_{k,l}), lsb(Delta T^1_{k,l})
        // Append msb_delta_t to D[lsb_delta_t]
        // ---------------------------------------------------------
        
        const uint32_t t_k_index = item.first; // T^1_l
        const uint32_t t_k = generate_set1_tweak_as_int(t_k_index,
                                                        num_texts_per_set_log);
        const uint32_t delta_t = t_l ^t_k;
        
        uint32_t msb_delta_t;
        uint32_t lsb_delta_t;
        split(msb_delta_t, lsb_delta_t, delta_t);
        
        num_collisions += find_num_collisions_in_d(table,
                                                   msb_delta_t,
                                                   lsb_delta_t,
                                                   num_find_num_collisions_in_d_calls);
    }
    
    return num_collisions;
}

// ---------------------------------------------------------

static void investigate(const TweakIndicesPointer &lists,
                        const Ciphertext msb_ciphertext_j,
                        const Ciphertext lsb_ciphertext_j,
                        const Ciphertext t_j,
                        TablePointer &table,
                        size_t& num_calls) {
    const CiphertextTweakTupleVector &previous_ciphertexts =
        (*lists)[lsb_ciphertext_j];
    
    for (const CiphertextTweakTuple &item : previous_ciphertexts) {
        if (item.second != msb_ciphertext_j) { // C^0_i != C^0_j
            continue;
        }
        
        num_calls++;
        
        // ---------------------------------------------------------
        // Add Delta T^0_{i,j} = (T^0_i, T^0_j)
        // msb_delta_t, lsb_delta_t = msb(Delta T^0_{i,j}), lsb(Delta T^0_{i,j})
        // Append msb_delta_t to D[lsb_delta_t]
        // ---------------------------------------------------------
        
        const uint32_t t_index = item.first; // T^0_i
        const uint32_t t_i = generate_set0_tweak_as_int(t_index);
        const uint32_t delta_t = t_i ^t_j;
        
        uint32_t msb_delta_t;
        uint32_t lsb_delta_t;
        split(msb_delta_t, lsb_delta_t, delta_t);
        
        (*table)[lsb_delta_t].push_back(msb_delta_t);
    }
}

// ---------------------------------------------------------

static size_t generate_lists(ExperimentContext *context,
                             const EncryptFunction &encryption_function,
                             TweakIndicesPointer &lists) {
    // ---------------------------------------------------------
    // Prepare the table D
    // ---------------------------------------------------------
    
    TablePointer num_pairs_per_difference(new Table());
    clear_table(num_pairs_per_difference);
    size_t num_total_collisions = 0;
    
    // ---------------------------------------------------------
    // Key setup, random key
    // ---------------------------------------------------------
    
    tnt_small_present24_context_t cipher_ctx = context->cipher_ctx;
    tnt_small_present24_key_t key;
    
    utils::get_random_bytes(key, TNT_SMALL_PRESENT24_NUM_KEY_BYTES);
    tnt_small_present24_key_schedule(&cipher_ctx, key);
    
    if (context->be_verbose) {
        utils::print_hex("# Key", key, TNT_SMALL_PRESENT24_NUM_KEY_BYTES);
    }
    
    // ---------------------------------------------------------
    // Prepare
    // ---------------------------------------------------------
    
    tnt_small_present24_state_t tweak;
    tnt_small_present24_state_t ciphertext;
    tnt_small_present24_state_t plaintext;
    Ciphertext ciphertext_as_int;
    
    if (context->be_verbose) {
        printf("#    i    j    t    p    c\n");
    }
    
    // ---------------------------------------------------------
    // Trackers
    // ---------------------------------------------------------
    
    size_t num_investigate_calls = 0;
    size_t num_find_num_collisions_calls = 0;
    size_t num_find_num_collisions_in_d_calls = 0;
    
    // ---------------------------------------------------------
    // Random message M^0
    // ---------------------------------------------------------
    
    utils::get_random_bytes(plaintext, TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    
    if (context->be_verbose) {
        utils::print_hex("# M^0", plaintext,
                         TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    }
    
    // ---------------------------------------------------------
    // Fill list with encryptions of first (T^i, M) pairs
    // ---------------------------------------------------------
    
    uint32_t msb_ciphertext_j;
    uint32_t lsb_ciphertext_j;
    
    for (size_t i = 0; i < context->num_texts_per_set; ++i) {
        generate_set0_tweak(tweak, i);
        size_t t = generate_set0_tweak_as_int(i);
        encryption_function(&cipher_ctx, tweak, plaintext, ciphertext);
        
        // Split ciphertext C^0_i
        ciphertext_as_int = to_integer(ciphertext);
        split(msb_ciphertext_j, lsb_ciphertext_j, ciphertext_as_int);
        
        investigate(lists,
                    msb_ciphertext_j,
                    lsb_ciphertext_j,
                    t,
                    num_pairs_per_difference,
                    num_investigate_calls);
        
        // Store (T^0_i, msb(C^0_i))
        const CiphertextTweakTuple tuple = std::make_pair(i, msb_ciphertext_j);
        (*lists)[lsb_ciphertext_j].push_back(tuple);
    }
    
    // ---------------------------------------------------------
    // Encrypt second list of (T^j, M') pairs.
    // Find collisions (C^i, C^j), where C^i corresponds to some (T^i, M)
    // on-the-fly, determine for each C^j a = the number of C^i's it collides
    // with, compute delta = T^i xor T^j, add table[delta] to the number of
    // total collisions, and increase table[delta] += a.
    // ---------------------------------------------------------
    
    utils::get_random_bytes(plaintext, TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    
    if (context->be_verbose) {
        utils::print_hex("# M^1", plaintext,
                         TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    }
    
    clear_lists(lists);
    
    for (size_t i = 0; i < context->num_texts_per_set; ++i) {
        generate_set1_tweak(tweak, i, context->num_texts_per_set_log);
        const size_t t_prime = generate_set1_tweak_as_int(
            i, context->num_texts_per_set_log);
        
        encryption_function(&cipher_ctx, tweak, plaintext, ciphertext);
        
        // Split ciphertext C^1_j
        ciphertext_as_int = to_integer(ciphertext);
        split(msb_ciphertext_j, lsb_ciphertext_j, ciphertext_as_int);
        
        const size_t num_new_collisions =
            find_num_collisions(lists,
                                msb_ciphertext_j,
                                lsb_ciphertext_j,
                                t_prime,
                                num_pairs_per_difference,
                                context->num_texts_per_set_log,
                                num_find_num_collisions_calls,
                                num_find_num_collisions_in_d_calls);
        
        num_total_collisions += num_new_collisions;
        
        // Store (T^1_j, msb(C^1_j))
        const CiphertextTweakTuple tuple = std::make_pair(i, msb_ciphertext_j);
        (*lists)[lsb_ciphertext_j].push_back(tuple);
    }
    
    printf("# investigate_calls: %zu\n", num_investigate_calls);
    printf("# find_num_collisions_calls: %zu\n", num_find_num_collisions_calls);
    printf("# find_num_collisions_in_d_calls: %zu\n",
           num_find_num_collisions_in_d_calls);
    
    return num_total_collisions;
}

// ---------------------------------------------------------

static size_t perform_experiment(ExperimentContext *context) {
    TweakIndicesPointer lists(new TweakIndices());
    const EncryptFunction encryption_function = context->use_tprp ?
                                                encrypt_random :
                                                encrypt_real;
    const size_t num_collisions = generate_lists(context,
                                                 encryption_function,
                                                 lists);
    return num_collisions;
}

// ---------------------------------------------------------

static void perform_experiments(ExperimentContext *context) {
    ExperimentResult all_results;
    all_results.num_collisions = 0;
    
    printf("# %8zu Experiments\n", context->num_keys);
    printf("# %8zu Sets/key\n", context->num_texts_per_set);
    printf("# Key Collisions Mean Variance \n");
    
    for (size_t i = 0; i < context->num_keys; ++i) {
        const size_t num_collisions = perform_experiment(context);
        const double mean =
            (double) num_collisions / (double) context->num_texts_per_set;
        
        all_results.num_collisions += num_collisions;
        all_results.num_collisions_per_set.push_back(num_collisions);
        
        printf("%4zu %8zu %8.4f\n", i + 1, num_collisions, mean);
    }
    
    const double mean = compute_mean(all_results.num_collisions_per_set);
    const double variance = compute_variance(
        all_results.num_collisions_per_set);
    
    printf("# Total Keys Collisions Mean Variance \n");
    printf("# %4zu %8zu %8.4f %8.8f\n",
           context->num_keys,
           all_results.num_collisions,
           mean,
           variance);
}

// ---------------------------------------------------------
// Argument parsing
// ---------------------------------------------------------

static void
parse_args(ExperimentContext *context, int argc, const char **argv) {
    ArgumentParser parser;
    parser.appName("Test for the TNT distinguisher.");
    parser.addArgument("-k", "--num_keys", 1, false);
    parser.addArgument("-s", "--num_texts_per_set", 1, false); // 2^12
    parser.addArgument("-r", "--use_tprp", 1, false);
    parser.addArgument("-v", "--verbose", 1, false);
    
    try {
        parser.parse((size_t) argc, argv);
        
        context->num_texts_per_set_log = parser.retrieveAsLong("s");
        context->num_texts_per_set = static_cast<const size_t>(1L)
            << context->num_texts_per_set_log;
        context->num_keys = parser.retrieveAsLong("k");
        context->use_tprp = static_cast<bool>(parser.retrieveAsInt("r"));
        context->be_verbose = static_cast<bool>(parser.retrieveAsInt("v"));
    } catch (...) {
        fprintf(stderr, "%s\n", parser.usage().c_str());
        exit(EXIT_FAILURE);
    }
    
    printf("# Keys     %8zu\n", context->num_keys);
    printf("# Sets/Key %8zu\n", context->num_texts_per_set);
    printf("# Use TPRP %8d\n", context->use_tprp);
    printf("# Verbose  %8d\n", context->be_verbose);
}

// ---------------------------------------------------------

int main(int argc, const char **argv) {
    ExperimentContext context;
    parse_args(&context, argc, argv);
    perform_experiments(&context);
    return EXIT_SUCCESS;
}
