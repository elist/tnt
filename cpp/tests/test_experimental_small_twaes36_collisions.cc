/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <functional>
#include <vector>
#include <cstdint>
#include <cstdlib>
#include <cstring>

#include "ciphers/small_aes36.h"
#include "ciphers/experimental_small_twaes36.h"
#include "ciphers/tnt_small_aes36.h"
#include "utils/argparse.h"
#include "utils/utils.h"
#include "utils/xorshift1024.h"

using ciphers::tnt_small_aes36_sse_context_t;
using ciphers::small_aes36_sse_context_t;
using ciphers::tnt_small_aes36_key_t;

using ciphers::small_aes36_context_t;
using ciphers::small_aes36_state_t;
using ciphers::small_aes36_key_t;
using utils::assert_equal;
using utils::compute_mean;
using utils::compute_variance;
using utils::xor_arrays;
using utils::zeroize_array;
using utils::ArgumentParser;

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

static const size_t NUM_STORAGE_INDEX_BITS = 20;
static const size_t NUM_STORAGE_LISTS = 1L << NUM_STORAGE_INDEX_BITS;
static const size_t NUM_TWEAKS_PER_SET = 1L << 4;

// ---------------------------------------------------------
// Types
// ---------------------------------------------------------

typedef uint64_t Ciphertext;
typedef std::pair<Ciphertext, uint8_t> CiphertextTweakPair;
typedef std::vector<size_t> ValuesList;
typedef std::vector<CiphertextTweakPair> PairsList;
typedef std::vector<PairsList> PairsStorageIndexList;

typedef std::function<Ciphertext(const small_aes36_context_t *,
                                 const Ciphertext,
                                 const Ciphertext)> EncryptFunction;

typedef std::function<Ciphertext(const size_t,
                                 const size_t)> GeneratePlaintextFunction;

typedef struct {
    small_aes36_key_t key;
    small_aes36_context_t cipher_ctx;
    size_t num_keys = 0;
    size_t num_rounds = 0;
    size_t num_texts_per_set = 0;
    size_t num_sets_per_key = 0;
    PairsStorageIndexList pairs_list;
    bool be_verbose = false;
    bool use_tprp = false;
} ExperimentContext;

typedef std::function<void(const ExperimentContext *)> GenerateTextFunction;

typedef struct {
    std::vector<size_t> num_collisions_per_set;
    size_t num_collisions = 0;
} ExperimentResult;

// ---------------------------------------------------------
// Functions
// ---------------------------------------------------------

static void generate_random_key(ExperimentContext *context) {
    small_aes36_key_t key;
    
    utils::get_random_bytes(key, SMALL_AES36_NUM_KEY_BYTES);
    ciphers::small_aes36_key_schedule(&(context->cipher_ctx), key);
    
    utils::print_hex("# Key", key, SMALL_AES36_NUM_KEY_BYTES);
}

// ---------------------------------------------------------

/**
 * Given a 16-bit index (i_0, i_1, i_2, i_3) and a base plaintext
 * (x_0, ..., x_8), generates
 *
 * i_0 i_2 x_6
 * i_1 i_3 x_7
 * x_2 x_5 x_8
 *
 * @param base_plaintext
 * @param index
 * @return
 */
static uint64_t
generate_plaintext(const uint64_t base_plaintext, const size_t index) {
    return ((index & 0xFF00) << 20)
           | ((index & 0x00FF) << 16)
           | (base_plaintext & 0x00F00FFFFL);
}

// ---------------------------------------------------------

static uint64_t generate_tweak(const uint64_t base_tweak, const size_t index) {
    const uint64_t nibble = index & 0xF;
    return (nibble << 8) | (nibble << 4); // | (base_tweak & 0xF);
}

// ---------------------------------------------------------

static uint64_t generate_base_tweak() {
    return (utils::get_random_word() & 0xFFFFFFFFFL);
}

// ---------------------------------------------------------

static uint64_t generate_base_plaintext() {
    return (utils::get_random_word() & 0xFFFFFFFFFL);
}

// ---------------------------------------------------------

/**
 * Given a 36-bit ciphertext
 * x_0 x_3 x_6
 * x_1 x_4 x_7
 * x_2 x_5 x_8
 * returns (x_2 || x_5 || x_6 || x_7 || x_8)
 *
 * @param ciphertext
 * @return
 */
static uint64_t get_storage_index_from_ciphertext(const uint64_t ciphertext) {
    return ((ciphertext >> 8) & 0xF0000)
           | (ciphertext & 0x0FFFF);
}

// ---------------------------------------------------------

static uint64_t encrypt_real(const ExperimentContext *context,
                             const uint64_t tweak,
                             const uint64_t plaintext) {
    return ciphers::experimental_small_twaes36_encrypt_rounds(
        &(context->cipher_ctx),
        tweak,
        plaintext,
        context->num_rounds);
}

// ---------------------------------------------------------

static uint64_t encrypt_random(const ExperimentContext *context,
                               const uint64_t tweak,
                               const uint64_t plaintext) {
    (void) context;
    (void) tweak;
    (void) plaintext;
    return utils::get_random_word() & 0xFFFFFFFFFL;
}

// ---------------------------------------------------------

/**
 * Returns true if the tweak difference is non-zero and, given a ciphertext
 * difference of
 * x_0 x_3 0
 * x_1 x_4 0
 * 0   0   0
 * if x_0 = x_1 = x_3 = x_4.
 * Returns false otherwise.
 *
 * @param ciphertext_i
 * @param tweak_i
 * @param ciphertext_j
 * @param tweak_j
 * @return
 */
static bool is_collision_counted(const uint64_t ciphertext_i,
                                 const uint8_t tweak_i,
                                 const uint64_t ciphertext_j,
                                 const uint8_t tweak_j) {
    // ---------------------------------------------------------
    // We do not count collisions with equal tweaks
    // ---------------------------------------------------------
    
    if (tweak_i == tweak_j) {
        return false;
    }
    
    const uint64_t delta_c = ciphertext_i ^ciphertext_j;
    const uint64_t delta_0 = (delta_c >> 32) & 0xF;
    const uint64_t delta_1 = (delta_c >> 28) & 0xF;
    const uint64_t delta_3 = (delta_c >> 20) & 0xF;
    const uint64_t delta_4 = (delta_c >> 16) & 0xF;
    return (delta_0 == delta_1)
           && (delta_1 == delta_3)
           && (delta_3 == delta_4);
}

// ---------------------------------------------------------

static bool is_tweak_induced_collision(const uint64_t ciphertext_i,
                                       const uint8_t tweak_i,
                                       const uint64_t ciphertext_j,
                                       const uint8_t tweak_j) {
    return (tweak_i != tweak_j)
           && (ciphertext_i == ciphertext_j);
}

// ---------------------------------------------------------

static size_t find_num_collisions(const PairsStorageIndexList &pairs_list) {
    size_t num_collisions = 0;
    
    for (const PairsList &sub_list : pairs_list) {
        const size_t num_entries = sub_list.size();
        
        if (num_entries < 2) {
            continue;
        }
        
        for (size_t i = 0; i < num_entries; ++i) {
            const CiphertextTweakPair &pair_i = sub_list[i];
            
            for (size_t j = i + 1; j < num_entries; ++j) {
                const CiphertextTweakPair &pair_j = sub_list[j];
                
                if (is_collision_counted(pair_i.first,
                                         pair_i.second,
                                         pair_j.first,
                                         pair_j.second)) {
                    const uint64_t delta_c = pair_i.first ^pair_j.first;
                    
                    printf("%09lx %09lx %02x %09lx %02x \n",
                           delta_c,
                           pair_i.first, pair_i.second,
                           pair_j.first, pair_j.second);
                    
                    num_collisions++;
                }
            }
        }
    }
    
    return num_collisions;
}

// ---------------------------------------------------------

static void generate_texts_real(ExperimentContext *context) {
    const uint64_t base_tweak = generate_base_tweak();
    const uint64_t base_plaintext = generate_base_plaintext();
    
    for (size_t t = 0; t < NUM_TWEAKS_PER_SET; ++t) {
        const uint64_t tweak = generate_tweak(base_tweak, t);
        
        for (size_t j = 0; j < context->num_texts_per_set; ++j) {
            uint64_t plaintext = generate_plaintext(base_plaintext, j);
            
            const uint64_t ciphertext =
                encrypt_real(context, tweak, plaintext);
            
            const size_t storage_index =
                get_storage_index_from_ciphertext(ciphertext);
            
            CiphertextTweakPair ciphertext_tweak_pair;
            ciphertext_tweak_pair.first = ciphertext;
            ciphertext_tweak_pair.second = t & 0xFF;
            context->pairs_list[storage_index].push_back(ciphertext_tweak_pair);
        }
    }
}

// ---------------------------------------------------------

static void generate_texts_random(ExperimentContext *context) {
    const uint64_t base_tweak = generate_base_tweak();
    const uint64_t base_plaintext = generate_base_plaintext();
    
    for (size_t t = 0; t < NUM_TWEAKS_PER_SET; ++t) {
        uint64_t tweak = generate_tweak(base_tweak, t);
        
        for (size_t j = 0; j < context->num_texts_per_set; ++j) {
            uint64_t plaintext = generate_plaintext(base_plaintext, j);
            
            const uint64_t ciphertext =
                encrypt_random(context, tweak, plaintext);
            
            const size_t storage_index =
                get_storage_index_from_ciphertext(ciphertext);
            
            CiphertextTweakPair ciphertext_tweak_pair;
            ciphertext_tweak_pair.first = ciphertext;
            ciphertext_tweak_pair.second = static_cast<uint8_t>(t & 0xFF);
            context->pairs_list[storage_index].push_back(ciphertext_tweak_pair);
        }
    }
}

// ---------------------------------------------------------

static void initialize_lists(PairsStorageIndexList &pairs_list) {
    pairs_list.clear();
    pairs_list.assign(NUM_STORAGE_LISTS, PairsList());
}

// ---------------------------------------------------------

static size_t perform_experiment(ExperimentContext *context) {
    // ---------------------------------------------------------
    // Key setup, random key
    // ---------------------------------------------------------
    
    generate_random_key(context);
    
    size_t num_collisions = 0;
    
    for (size_t i = 0; i < context->num_sets_per_key; ++i) {
        initialize_lists(context->pairs_list);
        
        if (context->use_tprp) {
            generate_texts_random(context);
        } else {
            generate_texts_real(context);
        }
        
        num_collisions += find_num_collisions(context->pairs_list);
    }
    
    return num_collisions;
}

// ---------------------------------------------------------

static void perform_experiments(ExperimentContext *context) {
    ExperimentResult all_results;
    all_results.num_collisions = 0;
    
    printf("# Key Collisions Mean Variance \n");
    
    for (size_t i = 0; i < context->num_keys; ++i) {
        const size_t num_collisions = perform_experiment(context);
        const double mean =
            (double) num_collisions /
            (double) context->num_texts_per_set;
        
        all_results.num_collisions += num_collisions;
        all_results.num_collisions_per_set.push_back(num_collisions);
        
        printf("%4zu %8zu %8.4f\n", i + 1, num_collisions, mean);
    }
    
    const double mean = compute_mean(
        all_results.num_collisions_per_set);
    const double variance = compute_variance(
        all_results.num_collisions_per_set);
    
    printf("# Total Keys Collisions Mean Variance \n");
    printf("# %4zu %8zu %8.4f %8.8f\n",
           context->num_keys,
           all_results.num_collisions,
           mean,
           variance);
}

// ---------------------------------------------------------
// Argument parsing
// ---------------------------------------------------------

static void
parse_args(ExperimentContext *context, int argc, const char **argv) {
    ArgumentParser parser;
    parser.appName("Test for collisions with an experimental version "
                   "of TwAES36.");
    parser.addArgument("-k", "--num_keys", 1, false);
    parser.addArgument("-t", "--num_texts_per_set", 1, false);
    parser.addArgument("-s", "--num_sets_per_key", 1, false);
    parser.addArgument("-u", "--num_rounds", 1, false);
    parser.addArgument("-r", "--use_tprp", 1, false);
    parser.addArgument("-v", "--verbose", 1, false);
    
    try {
        parser.parse((size_t) argc, argv);
        
        const size_t num_texts_per_set_log = parser.retrieveAsLong("t");
        const size_t num_structures_log = parser.retrieveAsLong("s");
        context->num_texts_per_set = static_cast<const size_t>(1L
            << num_texts_per_set_log);
        context->num_sets_per_key = static_cast<const size_t>(1L
            << num_structures_log);
        context->num_keys = parser.retrieveAsLong("k");
        context->num_rounds = parser.retrieveAsLong("u");
        context->use_tprp = static_cast<bool>(parser.retrieveAsInt(
            "r"));
        context->be_verbose = static_cast<bool>(parser.retrieveAsInt(
            "v"));
    } catch (...) {
        fprintf(stderr, "%s\n", parser.usage().c_str());
        exit(EXIT_FAILURE);
    }
    
    printf("# Keys     %8zu\n", context->num_keys);
    printf("# Sets/Key %8zu\n", context->num_sets_per_key);
    printf("# Texts/Set%8zu\n", context->num_texts_per_set);
    printf("# Rounds   %8zu\n", context->num_rounds);
    printf("# Use TPRP %8d\n", context->use_tprp);
    printf("# Verbose  %8d\n", context->be_verbose);
}

// ---------------------------------------------------------

int main(int argc, const char **argv) {
    ExperimentContext context;
    parse_args(&context, argc, argv);
    perform_experiments(&context);
    return EXIT_SUCCESS;
}
