/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <array>
#include <functional>
#include <memory>
#include <vector>
#include <cstdint>
#include <cstdlib>
#include <cstring>

#include "ciphers/small_aes36.h"
#include "ciphers/experimental_small_kiasubc36.h"
#include "utils/argparse.h"
#include "utils/utils.h"
#include "utils/xorshift1024.h"

using ciphers::small_aes36_context_t;
using ciphers::small_aes36_state_t;
using ciphers::small_aes36_key_t;
using utils::assert_equal;
using utils::compute_mean;
using utils::compute_variance;
using utils::xor_arrays;
using utils::zeroize_array;
using utils::ArgumentParser;

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

static const size_t NUM_BITS = 36;
static const size_t NUM_VALUES = 1L << 24;
static const size_t NUM_BYTES_IN_CHUNK = 1L << 20;

// ---------------------------------------------------------
// Types
// ---------------------------------------------------------

typedef uint64_t Ciphertext;
typedef std::vector<size_t> ValuesList;

typedef std::function<Ciphertext(const small_aes36_context_t *,
                                 const Ciphertext,
                                 const Ciphertext)> EncryptFunction;

typedef std::function<Ciphertext(const size_t,
                                 const size_t)> GeneratePlaintextFunction;

typedef struct {
    small_aes36_key_t key;
    small_aes36_context_t cipher_ctx;
    size_t num_keys = 0;
    size_t num_texts_per_set = 0;
    size_t num_texts_per_set_log = 0;
    ValuesList left_values_list;
    ValuesList right_values_list;
    bool be_verbose = false;
    bool use_tprp = false;
} ExperimentContext;

typedef std::function<void(const ExperimentContext *)> GenerateTextFunction;

typedef struct {
    std::vector<size_t> num_collisions_per_set;
    size_t num_collisions = 0;
} ExperimentResult;

// ---------------------------------------------------------
// Functions
// ---------------------------------------------------------

static void generate_random_key(ExperimentContext *context) {
    small_aes36_context_t &cipher_ctx = context->cipher_ctx;
    small_aes36_key_t key;
    
    utils::get_random_bytes(key, SMALL_AES36_NUM_KEY_BYTES);
    experimental_small_kiasubc36_key_schedule(&cipher_ctx, key);
    
    utils::print_hex("# Key", key, SMALL_AES36_NUM_KEY_BYTES);
}

// ---------------------------------------------------------

/**
 * Given a 36-bit value
 * @param plaintext
 * @param i
 */
static Ciphertext generate_left_plaintext(const size_t num_used_bits,
                                          const size_t i) {
    (void) num_used_bits;
    return i;
}

// ---------------------------------------------------------

static Ciphertext generate_right_plaintext(const size_t num_used_bits,
                                           const size_t i) {
    const size_t shift = NUM_BITS - num_used_bits;
    return i << shift;
}

// ---------------------------------------------------------

static Ciphertext encrypt(const small_aes36_context_t *ctx,
                          const Ciphertext tweak,
                          const Ciphertext plaintext) {
    return ciphers::experimental_small_kiasubc36_encrypt(ctx,
                                                         tweak,
                                                         plaintext);
}

// ---------------------------------------------------------

static void initialize_list(ValuesList &values_list) {
    values_list = ValuesList(0, NUM_VALUES);
    values_list.reserve(NUM_VALUES);
}

// ---------------------------------------------------------

static void initialize_lists(ExperimentContext *context) {
    initialize_list(context->left_values_list);
    initialize_list(context->right_values_list);
}

// ---------------------------------------------------------

static uint64_t get_ith_ciphertext(const uint8_t *chunk,
                                   const size_t index,
                                   const size_t num_bytes_per_word) {
    const uint8_t *start = chunk + (index * num_bytes_per_word);
    return (((uint64_t) (start[0])) << 16)
           | (((uint64_t) (start[1])) << 8)
           | ((uint64_t) (start[2]));
}

// ---------------------------------------------------------

static void generate_texts_for_list_random(
    ExperimentContext *context,
    ValuesList &values_list) {
    const size_t num_bytes_per_word = 3;
    const size_t num_texts_per_chunk = NUM_BYTES_IN_CHUNK / num_bytes_per_word;
    const size_t num_bytes_needed =
        context->num_texts_per_set * num_bytes_per_word;
    const auto num_chunks = (size_t) (num_bytes_needed / NUM_BYTES_IN_CHUNK) + 1;
    auto *random_bytes_chunk = (uint8_t *) malloc(NUM_BYTES_IN_CHUNK);
    
//    printf("num_bytes_per_word: %zu\n", num_bytes_per_word);
//    printf("num_texts_per_chunk: %zu\n", num_texts_per_chunk);
//    printf("num_bytes_needed: %zu\n", num_bytes_needed);
//    printf("num_chunks: %zu\n", num_chunks);
    
    Ciphertext ciphertext = 0;
    size_t plaintext_index = 0;
    
    for (size_t chunk_index = 0; chunk_index < num_chunks; ++chunk_index) {
        utils::get_random_bytes(random_bytes_chunk, NUM_BYTES_IN_CHUNK);
        
//        printf("chunk_index: %zu\n", chunk_index);
        
        for (size_t i = 0; i < num_texts_per_chunk; ++i) {
            if (plaintext_index >= context->num_texts_per_set) {
                break;
            }
            
            ciphertext = get_ith_ciphertext(random_bytes_chunk, i,
                                            num_bytes_per_word);
            values_list[ciphertext]++;
            ++plaintext_index;
        }
    }
    
    free(random_bytes_chunk);
}

// ---------------------------------------------------------

/**
 * x0 x3 x6
 * x1 x4 x7
 * x2 x5 x8
 *
 * @param ciphertext
 * @return 24-bit value (x1 || x2 || x3 || x4 || x6 || x8)
 */
static size_t ciphertext_to_value(const Ciphertext ciphertext) {
    return ((ciphertext & 0x0FFFF0000) >> 8)
           | ((ciphertext & 0x000000F00) >> 4)
           | (ciphertext & 0x00000000F);
}

// ---------------------------------------------------------

static void generate_texts_for_list(
    ExperimentContext *context,
    ValuesList &values_list,
    const Ciphertext tweak,
    const GeneratePlaintextFunction &generate_plaintext_function) {
    Ciphertext plaintext = 0;
    Ciphertext ciphertext = 0;
    size_t ciphertext_as_value;
    
    for (size_t i = 0; i < context->num_texts_per_set; ++i) {
        plaintext = generate_plaintext_function(context->num_texts_per_set_log,
                                                i);
        ciphertext = encrypt(&(context->cipher_ctx),
                             tweak,
                             plaintext);
        ciphertext_as_value = ciphertext_to_value(ciphertext);
        values_list[ciphertext_as_value]++;
    }
}

// ---------------------------------------------------------

static void generate_texts_real(ExperimentContext *context) {
    const Ciphertext left_tweak = 0x000000000L;
    const Ciphertext right_tweak = 0xf00000000L;
    
    generate_texts_for_list(context,
                            context->left_values_list,
                            left_tweak,
                            generate_left_plaintext);
    generate_texts_for_list(context,
                            context->right_values_list,
                            right_tweak,
                            generate_right_plaintext);
}

// ---------------------------------------------------------

static void generate_texts_random(ExperimentContext *context) {
    generate_texts_for_list_random(context,
                                   context->left_values_list);
    generate_texts_for_list_random(context,
                                   context->right_values_list);
}

// ---------------------------------------------------------

static size_t find_num_collisions(const ExperimentContext *context) {
    const ValuesList &left_values_list = context->left_values_list;
    const ValuesList &right_values_list = context->right_values_list;
    size_t num_collisions = 0;
    
    for (size_t i = 0; i < NUM_VALUES; ++i) {
        num_collisions += left_values_list[i] * right_values_list[i];
    }
    
    return num_collisions;
}

// ---------------------------------------------------------

static size_t perform_experiment(ExperimentContext *context) {
    // ---------------------------------------------------------
    // Key setup, random key
    // ---------------------------------------------------------
    
    generate_random_key(context);
    initialize_lists(context);
    
    if (context->use_tprp) {
        generate_texts_random(context);
    } else {
        generate_texts_real(context);
    }
    
    return find_num_collisions(context);
}

// ---------------------------------------------------------

static void perform_experiments(ExperimentContext *context) {
    ExperimentResult all_results;
    all_results.num_collisions = 0;
    
    printf("# %8zu Experiments\n", context->num_keys);
    printf("# %8zu Sets/key\n", context->num_texts_per_set);
    printf("# Key Collisions Mean Variance \n");
    
    for (size_t i = 0; i < context->num_keys; ++i) {
        const size_t num_collisions = perform_experiment(context);
        const double mean =
            (double) num_collisions /
            (double) context->num_texts_per_set;
        
        all_results.num_collisions += num_collisions;
        all_results.num_collisions_per_set.push_back(num_collisions);
        
        printf("%4zu %8zu %8.4f\n", i + 1, num_collisions, mean);
    }
    
    const double mean = compute_mean(
        all_results.num_collisions_per_set);
    const double variance = compute_variance(
        all_results.num_collisions_per_set);
    
    printf("# Total Keys Collisions Mean Variance \n");
    printf("# %4zu %8zu %8.4f %8.8f\n",
           context->num_keys,
           all_results.num_collisions,
           mean,
           variance);
}

// ---------------------------------------------------------
// Argument parsing
// ---------------------------------------------------------

static void
parse_args(ExperimentContext *context, int argc, const char **argv) {
    ArgumentParser parser;
    parser.appName("Test for collisions with an experimental version "
                   "of Kiasu-BC36.");
    parser.addArgument("-k", "--num_keys", 1, false);
    parser.addArgument("-s", "--num_texts_per_set", 1, false); // 2^12
    parser.addArgument("-r", "--use_tprp", 1, false);
    parser.addArgument("-v", "--verbose", 1, false);
    
    try {
        parser.parse((size_t) argc, argv);
        
        context->num_texts_per_set_log = parser.retrieveAsLong("s");
        context->num_texts_per_set = static_cast<const size_t>(1L)
            << context->num_texts_per_set_log;
        context->num_keys = parser.retrieveAsLong("k");
        context->use_tprp = static_cast<bool>(parser.retrieveAsInt(
            "r"));
        context->be_verbose = static_cast<bool>(parser.retrieveAsInt(
            "v"));
    } catch (...) {
        fprintf(stderr, "%s\n", parser.usage().c_str());
        exit(EXIT_FAILURE);
    }
    
    printf("# Keys     %8zu\n", context->num_keys);
    printf("# Sets/Key %8zu\n", context->num_texts_per_set);
    printf("# Use TPRP %8d\n", context->use_tprp);
    printf("# Verbose  %8d\n", context->be_verbose);
}

// ---------------------------------------------------------

int main(int argc, const char **argv) {
    ExperimentContext context;
    parse_args(&context, argc, argv);
    perform_experiments(&context);
    return EXIT_SUCCESS;
}
