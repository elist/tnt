/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <array>
#include <functional>
#include <memory>
#include <vector>
#include <cstdint>
#include <cstdlib>
#include <cstring>

#include "ciphers/small_present24.h"
#include "ciphers/tnt_small_present24.h"
#include "utils/argparse.h"
#include "utils/utils.h"
#include "utils/xorshift1024.h"

using ciphers::tnt_small_present24_context_t;
using ciphers::tnt_small_present24_state_t;
using ciphers::tnt_small_present24_tweak_t;
using ciphers::tnt_small_present24_key_t;
using utils::assert_equal;
using utils::compute_mean;
using utils::compute_variance;
using utils::xor_arrays;
using utils::zeroize_array;
using utils::ArgumentParser;

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

static const size_t NUM_BITS = 24; // The state size in bits

// For this experiment, we set 8 bits to zero after pi_1 and use 16 bits
// effectively for the tweaks.
static const size_t NUM_USED_TWEAK_BITS = 16;

// We only store 2^12 messages in the table
static const size_t NUM_LSB_BITS = 12;
static const auto NUM_STORED_VALUES = static_cast<size_t>(1L) << NUM_LSB_BITS;
static const size_t NUM_INITIAL_ROUNDS = 3;
static const size_t NUM_MESSAGES = 15;

static const size_t LSB_MASK = (1L << NUM_LSB_BITS) - 1;
static const size_t MSB_MASK = ((1L << NUM_BITS) - 1) ^LSB_MASK;
static const uint32_t DIFFERENCE_AFTER_PI_1 =
    ciphers::small_present24_permutation_layer(0x00F00F);


// ---------------------------------------------------------
// Types
// ---------------------------------------------------------

typedef struct {
    tnt_small_present24_key_t key;
    tnt_small_present24_context_t cipher_ctx;
    size_t num_keys = 0;
    size_t num_texts_per_set = 0;
    size_t num_texts_per_set_log = 0;
    std::vector<size_t> num_matches;
    bool be_verbose = false;
    bool use_tprp = false;
} ExperimentContext;

typedef struct {
    std::vector<size_t> num_collisions_per_set;
    size_t num_collisions = 0;
} ExperimentResult;

typedef uint32_t Ciphertext;
typedef std::vector<Ciphertext> CiphertextsVector;
typedef std::array<std::vector<size_t>, NUM_STORED_VALUES> Table;
typedef std::shared_ptr<Table> TablePointer;
typedef std::array<Table, NUM_MESSAGES> TableList;
typedef std::shared_ptr<TableList> TableListPointer;
typedef std::array<size_t, NUM_MESSAGES * NUM_MESSAGES> CollisionsList;
typedef std::array<Ciphertext, NUM_MESSAGES> CiphertextsList;
typedef std::array<bool, NUM_MESSAGES * NUM_MESSAGES> PossibilityList;

typedef std::pair<Ciphertext, Ciphertext> CiphertextTweakTuple;
typedef std::vector<CiphertextTweakTuple> CiphertextTweakTupleVector;
typedef std::array<CiphertextTweakTupleVector, NUM_STORED_VALUES>
    TweakIndices;
typedef std::array<TweakIndices, NUM_MESSAGES> TweakIndicesList;
typedef std::shared_ptr<TweakIndices> TweakIndicesPointer;
typedef std::shared_ptr<TweakIndicesList> TweakIndicesListPointer;

typedef std::function<void(const tnt_small_present24_context_t *,
                           tnt_small_present24_tweak_t,
                           tnt_small_present24_state_t,
                           tnt_small_present24_state_t)> EncryptFunction;

// ---------------------------------------------------------
// Functions
// ---------------------------------------------------------

static uint32_t to_integer(const tnt_small_present24_state_t state) {
    uint32_t result = 0;
    result |= static_cast<uint32_t>(state[0]) << 16;
    result |= static_cast<uint32_t>(state[1]) << 8;
    result |= static_cast<uint32_t>(state[2]);
    return result;
}

// ---------------------------------------------------------

static void split(uint32_t &high,
                  uint32_t &lo,
                  const uint32_t &value) {
    high = (value & MSB_MASK) >> NUM_LSB_BITS;
    lo = value & LSB_MASK;
}

// ---------------------------------------------------------

static void to_array(tnt_small_present24_state_t result, const uint32_t state) {
    result[0] = static_cast<uint32_t>((state >> 16) & 0xFF);
    result[1] = static_cast<uint32_t>((state >> 8) & 0xFF);
    result[2] = static_cast<uint32_t>(state & 0xFF);
}

// ---------------------------------------------------------

static void generate_plaintext_i(tnt_small_present24_state_t plaintext,
                                 const tnt_small_present24_state_t base_plaintext,
                                 const size_t m) {
    memcpy(plaintext, base_plaintext, SMALL_PRESENT24_NUM_STATE_BYTES);
    plaintext[0] = ((m + 1) & 0x0F) << 4;
}

// ---------------------------------------------------------

static void generate_plaintext_j(tnt_small_present24_state_t plaintext,
                                 const tnt_small_present24_state_t base_plaintext,
                                 const size_t m) {
    memcpy(plaintext, base_plaintext, SMALL_PRESENT24_NUM_STATE_BYTES);
    plaintext[0] = (m + 1) & 0x0F;
}

// ---------------------------------------------------------

/**
 * We set the LSB to a single 1 bit since this bit is active only in an
 * impossible-differential output difference.
 * @param tweak
 * @param index
 */
static size_t generate_set0_tweak_as_int(const size_t index) {
    const uint32_t shifted_index = ((index << 4) & 0x000FF0)
                                   | ((index << 8) & 0xFF0000);
    return ciphers::small_present24_permutation_layer(shifted_index);
}

// ---------------------------------------------------------

/**
 * num_bits = log(num_sets_per_key), e.g. 11, 12, 13.
 * @param tweak
 * @param index
 * @param num_bits
 */
static size_t generate_set1_tweak_as_int(const size_t index,
                                         const size_t num_bits) {
    // For example, 16 - 12,13,14... = 4,3,2
    // means shift j << (16 - 4,2,3)
    const size_t shift = NUM_USED_TWEAK_BITS - num_bits;
    uint32_t shifted_index = index << shift;
    shifted_index = ((shifted_index << 4) & 0x000FF0)
                    | ((shifted_index << 8) & 0xFF0000);
    return ciphers::small_present24_permutation_layer(shifted_index);
}

// ---------------------------------------------------------

/**
 * We set the LSB to a single 1 bit since this bit is active only in an
 * impossible-differential output difference.
 * @param tweak
 * @param index
 */
static void generate_set0_tweak(tnt_small_present24_state_t tweak,
                                const size_t index) {
    const uint32_t permuted_shifted_index = generate_set0_tweak_as_int(index);
    to_array(tweak, permuted_shifted_index);
}

// ---------------------------------------------------------

/**
 * num_bits = log(num_sets_per_key), e.g. 11, 12, 13.
 * @param tweak
 * @param index
 * @param num_bits
 */
static void generate_set1_tweak(tnt_small_present24_state_t tweak,
                                const size_t index,
                                const size_t num_bits) {
    const uint32_t permuted_shifted_index =
        generate_set1_tweak_as_int(index, num_bits);
    to_array(tweak, permuted_shifted_index);
}

// ---------------------------------------------------------

static void encrypt_random(const tnt_small_present24_context_t *ctx,
                           const tnt_small_present24_tweak_t tweak,
                           const tnt_small_present24_state_t plaintext,
                           tnt_small_present24_state_t ciphertext) {
    // ---------------------------------------------------------
    // Suppress unused parameter warnings. We need those parameters
    // here only to be compliant with the real encryption function.
    // ---------------------------------------------------------
    
    (void) ctx;
    (void) tweak;
    (void) plaintext;
    utils::get_random_bytes(ciphertext, TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
}

// ---------------------------------------------------------

static void encrypt_real(const tnt_small_present24_context_t *ctx,
                         const tnt_small_present24_tweak_t tweak,
                         const tnt_small_present24_state_t plaintext,
                         tnt_small_present24_state_t ciphertext) {
    ciphers::tnt_small_present24_encrypt(ctx,
                                         tweak,
                                         plaintext,
                                         ciphertext,
                                         NUM_INITIAL_ROUNDS);
}

// ---------------------------------------------------------

static void encrypt_real_first_part(const tnt_small_present24_context_t *ctx,
                                    const tnt_small_present24_tweak_t tweak,
                                    const tnt_small_present24_state_t plaintext,
                                    tnt_small_present24_state_t ciphertext) {
    ciphers::tnt_small_present24_encrypt_first_part(ctx,
                                                    tweak,
                                                    plaintext,
                                                    ciphertext,
                                                    NUM_INITIAL_ROUNDS);
}

// ---------------------------------------------------------

static void clear_lists(TweakIndicesPointer &lists) {
    lists->fill(CiphertextTweakTupleVector());
}

// ---------------------------------------------------------

static void clear_tables(TableListPointer &table) {
    for (size_t m = 0; m < NUM_MESSAGES; ++m) {
        (*table)[m].fill(std::vector<size_t>());
    }
}

// ---------------------------------------------------------

static size_t get_list_index(const size_t m_i, const size_t m_j) {
    return (NUM_MESSAGES * m_i) + m_j;
}

// ---------------------------------------------------------

static bool is_difference_possible(const Ciphertext c_i,
                                   const Ciphertext c_j) {
    return ((c_i ^ c_j) & DIFFERENCE_AFTER_PI_1) == 0;
}

// ---------------------------------------------------------

static
size_t find_num_collisions_in_d(const std::vector<size_t> &entries,
                                const uint32_t msb_delta_t,
                                size_t &num_find_num_collisions_in_d_calls) {
    size_t num_collisions = 0;
    
    for (const size_t &previous_msb_delta_t : entries) {
        if (previous_msb_delta_t == msb_delta_t) {
            num_collisions++;
        }
        
        num_find_num_collisions_in_d_calls++;
    }
    
    return num_collisions;
}

// ---------------------------------------------------------

static void find_num_collisions(const TweakIndicesPointer &lists,
                                const Ciphertext msb_ciphertext_l,
                                const Ciphertext lsb_ciphertext_l,
                                const Ciphertext t_l,
                                const size_t m_l,
                                TableListPointer &table,
                                const size_t num_texts_per_set_log,
                                size_t &num_find_num_collisions_calls,
                                size_t &num_find_num_collisions_in_d_calls,
                                CollisionsList &collisions_list) {
    const CiphertextTweakTupleVector &previous_ciphertexts =
        (*lists)[lsb_ciphertext_l];
    
    for (const CiphertextTweakTuple &item : previous_ciphertexts) {
        if (item.second != msb_ciphertext_l) { // C^1_k != C^1_l
            continue;
        }
        
        // We found a pair (C_k, C_l) that collides
        
        num_find_num_collisions_calls++;
        
        // ---------------------------------------------------------
        // Add Delta T^1_{k,l} = (T^1_k, T^1_l)
        // msb_delta_t, lsb_delta_t = msb(Delta T^1_{k,l}), lsb(Delta T^1_{k,l})
        // Append msb_delta_t to D[lsb_delta_t]
        // ---------------------------------------------------------
        
        const uint32_t t_k_index = item.first; // T^1_l
        const uint32_t t_k = generate_set1_tweak_as_int(t_k_index,
                                                        num_texts_per_set_log);
        const uint32_t delta_t = t_l ^t_k;
        
        uint32_t msb_delta_t;
        uint32_t lsb_delta_t;
        split(msb_delta_t, lsb_delta_t, delta_t);
        
        for (size_t m_i = 0; m_i < NUM_MESSAGES; ++m_i) {
            const std::vector<size_t> &entries = (*table)[m_i][lsb_delta_t];
            const size_t list_index = get_list_index(m_i, m_l);
            collisions_list[list_index] += find_num_collisions_in_d(
                entries,
                msb_delta_t,
                num_find_num_collisions_in_d_calls);
        }
    }
}

// ---------------------------------------------------------

static void investigate(const TweakIndicesPointer &lists,
                        const Ciphertext msb_ciphertext_j,
                        const Ciphertext lsb_ciphertext_j,
                        const Ciphertext t_j,
                        TableListPointer &table,
                        size_t &num_calls,
                        const size_t m) {
    const CiphertextTweakTupleVector &previous_ciphertexts =
        (*lists)[lsb_ciphertext_j];
    
    for (const CiphertextTweakTuple &item : previous_ciphertexts) {
        if (item.second != msb_ciphertext_j) { // C^0_i != C^0_j
            continue;
        }
        
        num_calls++;
        
        // ---------------------------------------------------------
        // Add Delta T^0_{i,j} = (T^0_i, T^0_j)
        // msb_delta_t, lsb_delta_t = msb(Delta T^0_{i,j}), lsb(Delta T^0_{i,j})
        // Append msb_delta_t to D[lsb_delta_t]
        // ---------------------------------------------------------
        
        const uint32_t t_index = item.first; // T^0_i
        const uint32_t t_i = generate_set0_tweak_as_int(t_index);
        const uint32_t delta_t = t_i ^t_j;
        
        uint32_t msb_delta_t;
        uint32_t lsb_delta_t;
        split(msb_delta_t, lsb_delta_t, delta_t);
        
        (*table)[m][lsb_delta_t].push_back(msb_delta_t);
    }
}

// ---------------------------------------------------------

static CollisionsList generate_lists(
    ExperimentContext *context,
    const EncryptFunction &encryption_function,
    const EncryptFunction &first_part_encryption_function,
    TweakIndicesPointer &lists,
    PossibilityList &possible_differences_list) {
    // ---------------------------------------------------------
    // Prepare the tables D_i, for i = 1..NUM_MESSAGES-1
    // ---------------------------------------------------------
    
    TableListPointer num_pairs_per_difference(new TableList());
    clear_tables(num_pairs_per_difference);
    
    CollisionsList collisions_list;
    collisions_list.fill(0);
    
    CiphertextsList first_part_ciphertexts;
    first_part_ciphertexts.fill(0);
    
    possible_differences_list.fill(false);
    
    // ---------------------------------------------------------
    // Key setup, random key
    // ---------------------------------------------------------
    
    tnt_small_present24_context_t cipher_ctx = context->cipher_ctx;
    tnt_small_present24_key_t key;
    
    utils::get_random_bytes(key, TNT_SMALL_PRESENT24_NUM_KEY_BYTES);
    tnt_small_present24_key_schedule(&cipher_ctx, key);
    
    if (context->be_verbose) {
        utils::print_hex("# Key", key, TNT_SMALL_PRESENT24_NUM_KEY_BYTES);
    }
    
    // ---------------------------------------------------------
    // Prepare
    // ---------------------------------------------------------
    
    tnt_small_present24_state_t tweak;
    tnt_small_present24_state_t ciphertext;
    tnt_small_present24_state_t base_plaintext;
    tnt_small_present24_state_t plaintext;
    Ciphertext ciphertext_as_int;
    
    if (context->be_verbose) {
        printf("#    i    j    t    p    c\n");
    }
    
    // ---------------------------------------------------------
    // Trackers
    // ---------------------------------------------------------
    
    size_t num_investigate_calls = 0;
    size_t num_find_num_collisions_calls = 0;
    size_t num_find_num_collisions_in_d_calls = 0;
    
    // ---------------------------------------------------------
    // Random message M^0
    // ---------------------------------------------------------
    
    utils::get_random_bytes(base_plaintext,
                            TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    
    if (context->be_verbose) {
        utils::print_hex("# M^0", base_plaintext,
                         TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    }
    
    // ---------------------------------------------------------
    // Fill list with encryptions of first (T^i, M) pairs
    // ---------------------------------------------------------
    
    uint32_t msb_ciphertext_j;
    uint32_t lsb_ciphertext_j;
    
    for (size_t m = 0; m < NUM_MESSAGES; ++m) {
        // Fill plaintext M^i 1..3
        generate_plaintext_i(plaintext, base_plaintext, m);
        
        // ---------------------------------------------------------
        // Encrypt through pi_1, so we can evaluate the difference
        // S^i xor S^j later
        // ---------------------------------------------------------
        
        first_part_encryption_function(&cipher_ctx, tweak, plaintext,
                                       ciphertext);
        first_part_ciphertexts[m] = to_integer(ciphertext);
    
        if (context->be_verbose) {
            printf("# S^0_%1zu %06x\n", m + 1, first_part_ciphertexts[m]);
        }
        
        for (size_t i = 0; i < context->num_texts_per_set; ++i) {
            generate_set0_tweak(tweak, i);
            size_t t = generate_set0_tweak_as_int(i);
            encryption_function(&cipher_ctx, tweak, plaintext, ciphertext);
            
            // Split ciphertext C^0_i
            ciphertext_as_int = to_integer(ciphertext);
            split(msb_ciphertext_j, lsb_ciphertext_j, ciphertext_as_int);
            
            investigate(lists,
                        msb_ciphertext_j,
                        lsb_ciphertext_j,
                        t,
                        num_pairs_per_difference,
                        num_investigate_calls,
                        m);
            
            // Store (T^0_i, msb(C^0_i))
            const CiphertextTweakTuple tuple = std::make_pair(i,
                                                              msb_ciphertext_j);
            (*lists)[lsb_ciphertext_j].push_back(tuple);
        }
        
        // ---------------------------------------------------------
        // Essential: We need to clear all ciphertext entries from L
        // for the current message M^i to reuse it for the next message.
        // ---------------------------------------------------------
        
        clear_lists(lists);
    }
    
    // ---------------------------------------------------------
    // Encrypt second list of (T^j, M') pairs.
    // Find collisions (C^i, C^j), where C^i corresponds to some (T^i, M)
    // on-the-fly, determine for each C^j a = the number of C^i's it collides
    // with, compute delta = T^i xor T^j, add table[delta] to the number of
    // total collisions, and increase table[delta] += a.
    // ---------------------------------------------------------
    
    utils::get_random_bytes(plaintext, TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    
    if (context->be_verbose) {
        utils::print_hex("# M^1", plaintext,
                         TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    }
    
    for (size_t m_j = 0; m_j < NUM_MESSAGES; ++m_j) {
        // Fill plaintext M^j 1..3
        generate_plaintext_j(plaintext, base_plaintext, m_j);
        
        // ---------------------------------------------------------
        // Encrypt through pi_1, so we can evaluate the difference
        // S^i xor S^j later
        // ---------------------------------------------------------
        
        first_part_encryption_function(&cipher_ctx, tweak, plaintext,
                                       ciphertext);
        ciphertext_as_int = to_integer(ciphertext);
    
        if (context->be_verbose) {
            printf("# S^1_%1zu %06x\n", m_j + 1, ciphertext_as_int);
        }
        
        // ---------------------------------------------------------
        // Which differences S^0 xor S^1 are impossible?
        // ---------------------------------------------------------
        
        for (size_t m = 0; m < NUM_MESSAGES; ++m) {
            const size_t index = get_list_index(m, m_j);
            const Ciphertext difference =
                first_part_ciphertexts[m] ^ciphertext_as_int;
    
            if (context->be_verbose) {
                printf("# S^0_%1zu xor S^1_%1zu = %06x\n", m, m_j + 1,
                       difference);
            }
            
            possible_differences_list[index] = is_difference_possible(
                first_part_ciphertexts[m],
                ciphertext_as_int);
        }
        
        for (size_t i = 0; i < context->num_texts_per_set; ++i) {
            generate_set1_tweak(tweak, i, context->num_texts_per_set_log);
            const size_t t_prime = generate_set1_tweak_as_int(
                i, context->num_texts_per_set_log);
            
            encryption_function(&cipher_ctx, tweak, plaintext, ciphertext);
            
            // Split ciphertext C^1_j
            ciphertext_as_int = to_integer(ciphertext);
            split(msb_ciphertext_j, lsb_ciphertext_j, ciphertext_as_int);
            
            find_num_collisions(lists,
                                msb_ciphertext_j,
                                lsb_ciphertext_j,
                                t_prime,
                                m_j,
                                num_pairs_per_difference,
                                context->num_texts_per_set_log,
                                num_find_num_collisions_calls,
                                num_find_num_collisions_in_d_calls,
                                collisions_list);
            
            // Store (T^1_j, msb(C^1_j))
            const CiphertextTweakTuple tuple = std::make_pair(
                i, msb_ciphertext_j);
            (*lists)[lsb_ciphertext_j].push_back(tuple);
        }
        
        // ---------------------------------------------------------
        // Essential: We need to clear all ciphertext entries from L
        // for the current message M^i to reuse it for the next message.
        // ---------------------------------------------------------
        
        clear_lists(lists);
    }
    
    printf("# investigate_calls: %zu\n", num_investigate_calls);
    printf("# find_num_collisions_calls: %zu\n", num_find_num_collisions_calls);
    printf("# find_num_collisions_in_d_calls: %zu\n",
           num_find_num_collisions_in_d_calls);
    
    return collisions_list;
}

// ---------------------------------------------------------

static
CollisionsList perform_experiment(ExperimentContext *context) {
    TweakIndicesPointer lists(new TweakIndices());
    PossibilityList possible_differences_list;
    
    const EncryptFunction encryption_function =
        context->use_tprp ?
        encrypt_random :
        encrypt_real;
    const EncryptFunction first_part_encryption_function =
        context->use_tprp ?
        encrypt_random :
        encrypt_real_first_part;
    const CollisionsList collisions_list = generate_lists(
        context,
        encryption_function,
        first_part_encryption_function,
        lists,
        possible_differences_list);
    
    printf("# i  j  Delta M  colls\n");
    
    for (size_t m_i = 0; m_i < NUM_MESSAGES; ++m_i) {
        for (size_t m_j = 0; m_j < NUM_MESSAGES; ++m_j) {
            const size_t list_index = get_list_index(m_i, m_j);
            const size_t num_collisions_i_j = collisions_list[list_index];
            const size_t delta_m = (((m_i + 1) & 0xF) << 20)
                                   | (((m_j + 1) & 0xF) << 16);
            const bool is_possible = possible_differences_list[list_index];
            
            // Only for the distinguisher with 1 message pair:
            // const size_t delta_m = 0xf0;
            printf("%zu %zu  %04zx %zu %d\n",
                   m_i,
                   m_j,
                   delta_m,
                   num_collisions_i_j,
                   is_possible);
        }
    }
    
    return collisions_list;
}

// ---------------------------------------------------------

static size_t sum_num_collisions(const CollisionsList &collisions_list) {
    size_t num_collisions = 0;
    
    for (size_t m_i = 0; m_i < NUM_MESSAGES; ++m_i) {
        for (size_t m_j = 0; m_j < NUM_MESSAGES; ++m_j) {
            const size_t list_index = get_list_index(m_i, m_j);
            num_collisions += collisions_list[list_index];
        }
    }
    
    return num_collisions;
}

// ---------------------------------------------------------

static void perform_experiments(ExperimentContext *context) {
    ExperimentResult all_results;
    all_results.num_collisions = 0;
    
    printf("# %8zu Experiments\n", context->num_keys);
    printf("# %8zu Sets/key\n", context->num_texts_per_set);
    printf("# Key Collisions Mean Variance \n");
    
    for (size_t i = 0; i < context->num_keys; ++i) {
        const CollisionsList collisions_list = perform_experiment(context);
        const size_t num_collisions = sum_num_collisions(collisions_list);
        const double mean =
            (double) num_collisions / (double) context->num_texts_per_set;
        
        all_results.num_collisions += num_collisions;
        all_results.num_collisions_per_set.push_back(num_collisions);
        
        printf("%4zu %8zu %8.4f\n", i + 1, num_collisions, mean);
    }
    
    const double mean = compute_mean(all_results.num_collisions_per_set);
    const double variance = compute_variance(
        all_results.num_collisions_per_set);
    
    printf("# Total Keys Collisions Mean Variance \n");
    printf("# %4zu %8zu %8.4f %8.8f\n",
           context->num_keys,
           all_results.num_collisions,
           mean,
           variance);
}

// ---------------------------------------------------------
// Argument parsing
// ---------------------------------------------------------

static void
parse_args(ExperimentContext *context, int argc, const char **argv) {
    ArgumentParser parser;
    parser.appName("Test for the TNT distinguisher.");
    parser.addArgument("-k", "--num_keys", 1, false);
    parser.addArgument("-s", "--num_texts_per_set", 1, false); // 2^12
    parser.addArgument("-r", "--use_tprp", 1, false);
    parser.addArgument("-v", "--verbose", 1, false);
    
    try {
        parser.parse((size_t) argc, argv);
        
        context->num_texts_per_set_log = parser.retrieveAsLong("s");
        context->num_texts_per_set = static_cast<const size_t>(1L)
            << context->num_texts_per_set_log;
        context->num_keys = parser.retrieveAsLong("k");
        context->use_tprp = static_cast<bool>(parser.retrieveAsInt("r"));
        context->be_verbose = static_cast<bool>(parser.retrieveAsInt("v"));
    } catch (...) {
        fprintf(stderr, "%s\n", parser.usage().c_str());
        exit(EXIT_FAILURE);
    }
    
    printf("# Keys     %8zu\n", context->num_keys);
    printf("# Sets/Key %8zu\n", context->num_texts_per_set);
    printf("# Use TPRP %8d\n", context->use_tprp);
    printf("# Verbose  %8d\n", context->be_verbose);
}

// ---------------------------------------------------------

int main(int argc, const char **argv) {
    ExperimentContext context;
    parse_args(&context, argc, argv);
    perform_experiments(&context);
    return EXIT_SUCCESS;
}
