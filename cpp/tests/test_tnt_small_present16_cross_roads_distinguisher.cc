/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <array>
#include <functional>
#include <vector>
#include <cstdint>
#include <cstdlib>

#include "ciphers/small_present16.h"
#include "ciphers/tnt_small_present16.h"
#include "utils/argparse.h"
#include "utils/utils.h"
#include "utils/xorshift1024.h"


using ciphers::tnt_small_present16_context_t;
using ciphers::tnt_small_present16_state_t;
using ciphers::tnt_small_present16_tweak_t;
using ciphers::tnt_small_present16_key_t;
using utils::assert_equal;
using utils::compute_mean;
using utils::compute_variance;
using utils::to_uint16;
using utils::to_uint32;
using utils::xor_arrays;
using utils::zeroize_array;
using utils::ArgumentParser;

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

static const size_t NUM_LISTS = 2;
static const size_t NUM_BITS = 16;
static const size_t NUM_CIPHERTEXT_VALUES = 1L << NUM_BITS;

// ---------------------------------------------------------
// Types
// ---------------------------------------------------------

typedef struct {
    tnt_small_present16_key_t key;
    tnt_small_present16_context_t cipher_ctx;
    size_t num_keys;
    size_t num_texts_per_set;
    size_t num_texts_per_set_log;
    std::vector<size_t> num_matches;
    bool be_verbose = false;
    bool use_tprp = false;
} ExperimentContext;

typedef struct {
    std::vector<size_t> num_collisions_per_set;
    size_t num_collisions;
} ExperimentResult;

typedef uint16_t Ciphertext;
typedef std::vector<Ciphertext> CiphertextsVector;
typedef std::array<CiphertextsVector, NUM_LISTS> CiphertextsLists;
typedef std::array<size_t, NUM_CIPHERTEXT_VALUES> Table;
typedef std::array<CiphertextsVector, NUM_CIPHERTEXT_VALUES> TweakIndices;
typedef std::array<TweakIndices, NUM_LISTS> TweakIndicesList;
typedef std::array<Table, NUM_LISTS> TableList;


typedef std::function<void(const tnt_small_present16_context_t *,
                           tnt_small_present16_tweak_t,
                           tnt_small_present16_state_t,
                           tnt_small_present16_state_t)> EncryptFunction;

// ---------------------------------------------------------
// Functions
// ---------------------------------------------------------

static uint16_t to_integer(const tnt_small_present16_state_t state) {
    uint16_t result = 0;
    result |= static_cast<uint16_t>(state[0]) << 8;
    result |= static_cast<uint16_t>(state[1]);
    return result;
}

// ---------------------------------------------------------

static void to_array(tnt_small_present16_state_t result, const uint16_t state) {
    result[0] = static_cast<uint16_t>((state >> 8) & 0xFF);
    result[1] = static_cast<uint16_t>(state & 0xFF);
}

// ---------------------------------------------------------

static void generate_set0_tweak(tnt_small_present16_state_t tweak,
                                const size_t index) {
    to_array(tweak, static_cast<uint16_t>(index));
}

// ---------------------------------------------------------

/**
 * num_bits = log(num_sets_per_key), e.g. 11, 12, 13.
 * @param tweak
 * @param index
 * @param num_bits
 */
static void generate_set1_tweak(tnt_small_present16_state_t tweak,
                                const size_t index,
                                const size_t num_bits) {
    const size_t shift = NUM_BITS - num_bits; // e.g. 5, 4, 3
    const uint16_t shifted_index = index << shift;
    to_array(tweak, shifted_index);
}

// ---------------------------------------------------------

static void encrypt_random(const tnt_small_present16_context_t *ctx,
                           const tnt_small_present16_tweak_t tweak,
                           const tnt_small_present16_state_t plaintext,
                           tnt_small_present16_state_t ciphertext) {
    // Suppress unused parameter warnings. We need those parameters only to
    // comply with the real encryption function.
    (uint8_t*)ctx;
    (void)tweak;
    (void)plaintext;
    utils::get_random_bytes(ciphertext, TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
}

// ---------------------------------------------------------

static void encrypt_real(const tnt_small_present16_context_t *ctx,
                         const tnt_small_present16_tweak_t tweak,
                         const tnt_small_present16_state_t plaintext,
                         tnt_small_present16_state_t ciphertext) {
    ciphers::tnt_small_present16_encrypt(ctx, tweak, plaintext, ciphertext);
}

// ---------------------------------------------------------

static void generate_lists(ExperimentContext *context,
                           const EncryptFunction &encryption_function,
                           CiphertextsLists &lists) {
    tnt_small_present16_context_t cipher_ctx = context->cipher_ctx;
    tnt_small_present16_key_t key;
    
    // ---------------------------------------------------------
    // Key setup, random key
    // ---------------------------------------------------------
    
    utils::get_random_bytes(key, TNT_SMALL_PRESENT16_NUM_KEY_BYTES);
    tnt_small_present16_key_schedule(&cipher_ctx, key);
    
    // ---------------------------------------------------------
    // Random messages M^0, M^1
    // ---------------------------------------------------------
    
    tnt_small_present16_state_t plaintexts[NUM_LISTS];
    
    utils::get_random_bytes(plaintexts[0], TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
    utils::get_random_bytes(plaintexts[1], TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
    
    // ---------------------------------------------------------
    // Log
    // ---------------------------------------------------------
    
    if (context->be_verbose) {
        utils::print_hex("# Key", key, TNT_SMALL_PRESENT16_NUM_KEY_BYTES);
        utils::print_hex("# M^0", plaintexts[0],
                         TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
        utils::print_hex("# M^1", plaintexts[1],
                         TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
    }
    
    // ---------------------------------------------------------
    // Fill lists with encryptions
    // ---------------------------------------------------------
    
    Ciphertext tweaks_as_ints[NUM_LISTS];
    Ciphertext ciphertexts_as_ints[NUM_LISTS];
    tnt_small_present16_state_t tweaks[NUM_LISTS];
    tnt_small_present16_state_t ciphertexts[NUM_LISTS];
    
    if (context->be_verbose) {
        printf("#    i    j    t    p    c\n");
    }
    
    for (size_t i = 0; i < context->num_texts_per_set; ++i) {
        generate_set0_tweak(tweaks[0], i);
        generate_set1_tweak(tweaks[1], i, context->num_texts_per_set_log);
        
        for (size_t j = 0; j < NUM_LISTS; ++j) {
            encryption_function(&cipher_ctx,
                                tweaks[j],
                                plaintexts[j],
                                ciphertexts[j]);
    
            ciphertexts_as_ints[j] = to_integer(ciphertexts[j]);
            
            if (context->be_verbose) {
                tweaks_as_ints[j] = to_integer(tweaks[j]);
                
                printf("# %4zu %4zu %04x %02x%02x %04x\n", i, j,
                       tweaks_as_ints[j],
                       plaintexts[j][0],
                       plaintexts[j][1],
                       ciphertexts_as_ints[j]);
            }
            
            // Dirty hack, but works: Prevents that the random function generates
            // the same C = C' for different (T, M) and (T, M').
            if ((i == 0) && (j == 1) &&
                (ciphertexts_as_ints[0] == ciphertexts_as_ints[1])) {
                ciphertexts_as_ints[1]++;
            }
            
            // Store i (tweak index) into the position of the ciphertext
            // in list j.
            lists[j].push_back(ciphertexts_as_ints[j]);
        }
    }
}

// ---------------------------------------------------------

static size_t find_num_collisions_in_lists(const ExperimentContext *context,
                                           const CiphertextsLists &lists) {
    size_t num_total_collisions = 0;
    // 16 - 12 = 4 bits left in T^i and 4 bits right in T^j
    // #bits left and right only set in either T^i or T^j, not in their
    // Difference
    const size_t num_side_bits = NUM_BITS - context->num_texts_per_set_log;
    // The middle, 16 - (2 * 4) = 8 bits in the middle
    const size_t num_middle_bits = NUM_BITS - (2 * num_side_bits);
    const size_t num_middle_values = 1L << num_middle_bits;
    // 0x000F for num_side_bits = 4
    const size_t right_mask = (1L << num_side_bits) - 1;
    // 0xFFFF
    const size_t full_mask = NUM_CIPHERTEXT_VALUES - 1;
    // 0xFFF0 for num_side_bits = 4
    const size_t left_mask = full_mask ^ right_mask;
    
    if (context->be_verbose) {
        printf("# deltat  i    j  c_i  c_j\n");
    }
    
    for (size_t delta_t = 1; delta_t < NUM_CIPHERTEXT_VALUES; ++delta_t) {
        size_t num_colliding_pairs_for_delta_t = 0;
        
        for (size_t i = 0; i < num_middle_values; ++i) {
            // (i << 4) || (T^i & 0x000F)
            const size_t t = (i << num_side_bits) | (delta_t & right_mask);
            const size_t t_prime = (delta_t & left_mask) ^ (i << num_side_bits);
            
            const size_t t_index = t;
            const size_t t_prime_index = t_prime >> num_side_bits;
            
            const uint16_t ciphertext_i = lists[0][t_index];
            const uint16_t ciphertext_j = lists[1][t_prime_index];
            
            if (ciphertext_i == ciphertext_j) {
                if (context->be_verbose) {
                    printf("# %04zx %04zx %04zx %04x %04x\n",
                           delta_t, t_index, t_prime_index,
                           ciphertext_i, ciphertext_j);
                }
                
                num_colliding_pairs_for_delta_t++;
            }
        }
        
        const size_t num_colliding_quartets_for_delta_t =
            (num_colliding_pairs_for_delta_t *
             (num_colliding_pairs_for_delta_t - 1)) / 2;
        num_total_collisions += num_colliding_quartets_for_delta_t;
    }
    
    return num_total_collisions;
}

// ---------------------------------------------------------

static size_t perform_experiment(ExperimentContext *context) {
    CiphertextsLists lists;
    
    const EncryptFunction encryption_function = context->use_tprp ?
                                                encrypt_random : encrypt_real;
    
    generate_lists(context, encryption_function, lists);
    const size_t num_collisions = find_num_collisions_in_lists(context, lists);
    return num_collisions;
}

// ---------------------------------------------------------

static void perform_experiments(ExperimentContext *context) {
    ExperimentResult all_results;
    all_results.num_collisions = 0;
    
    printf("# %8zu Experiments\n", context->num_keys);
    printf("# %8zu Sets/key\n", context->num_texts_per_set);
    printf("# Key Collisions Mean Variance \n");
    
    for (size_t i = 0; i < context->num_keys; ++i) {
        const size_t num_collisions = perform_experiment(context);
        const double mean =
            (double) num_collisions / (double) context->num_texts_per_set;
        
        all_results.num_collisions += num_collisions;
        all_results.num_collisions_per_set.push_back(num_collisions);
        
        printf("%4zu %8zu %8.4f\n", i + 1, num_collisions, mean);
    }
    
    const double mean = compute_mean(all_results.num_collisions_per_set);
    const double variance = compute_variance(
        all_results.num_collisions_per_set);
    
    printf("# Total Keys Collisions Mean Variance \n");
    printf("# %4zu %8zu %8.4f %8.8f\n",
           context->num_keys,
           all_results.num_collisions,
           mean,
           variance);
}

// ---------------------------------------------------------
// Argument parsing
// ---------------------------------------------------------

static void
parse_args(ExperimentContext *context, int argc, const char **argv) {
    ArgumentParser parser;
    parser.appName("Test for the TNT distinguisher.");
    parser.addArgument("-k", "--num_keys", 1, false);
    parser.addArgument("-s", "--num_texts_per_set", 1, false); // 2^12
    parser.addArgument("-r", "--use_tprp", 1, false);
    parser.addArgument("-v", "--verbose", 1, false);
    
    try {
        parser.parse((size_t) argc, argv);
        
        context->num_texts_per_set_log = parser.retrieveAsLong("s");
        context->num_texts_per_set = static_cast<const size_t>(
            1L << context->num_texts_per_set_log
        );
        context->num_keys = parser.retrieveAsLong("k");
        context->use_tprp = static_cast<bool>(parser.retrieveAsInt("r"));
        context->be_verbose = static_cast<bool>(parser.retrieveAsInt("v"));
    } catch (...) {
        fprintf(stderr, "%s\n", parser.usage().c_str());
        exit(EXIT_FAILURE);
    }
    
    printf("# Keys     %8zu\n", context->num_keys);
    printf("# Sets/Key %8zu\n", context->num_texts_per_set);
    printf("# Use TPRP %8d\n", context->use_tprp);
    printf("# Verbose  %8d\n", context->be_verbose);
}

// ---------------------------------------------------------

int main(int argc, const char **argv) {
    ExperimentContext context;
    parse_args(&context, argc, argv);
    perform_experiments(&context);
    return EXIT_SUCCESS;
}
