/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdint>
#include <gtest/gtest.h>

#include "ciphers/experimental_small_twaes36.h"
#include "utils/utils.h"


using ciphers::small_aes36_context_t;
using ciphers::small_aes36_key_t;
using ciphers::small_aes36_state_t;
using utils::assert_equal;

// ---------------------------------------------------------

static
void run_encryption_test(const small_aes36_key_t key,
                         const uint64_t tweak,
                         const uint64_t plaintext,
                         const uint64_t expected_ciphertext) {
    small_aes36_context_t ctx;
    small_aes36_key_schedule(&ctx, key);
    
    const uint64_t ciphertext = experimental_small_twaes36_encrypt(
        &ctx, tweak, plaintext);
    ASSERT_EQ(expected_ciphertext, ciphertext);
}

// ---------------------------------------------------------

static
void run_round_encryption_test(const small_aes36_key_t key,
                               const uint64_t tweak,
                               const uint64_t plaintext,
                               const uint64_t expected_ciphertext,
                               const size_t num_rounds) {
    small_aes36_context_t ctx;
    small_aes36_key_schedule(&ctx, key);
    
    const uint64_t ciphertext = experimental_small_twaes36_encrypt_rounds(
        &ctx, tweak, plaintext, num_rounds);
    ASSERT_EQ(expected_ciphertext, ciphertext);
}

// ---------------------------------------------------------

static
void run_decryption_test(const small_aes36_key_t key,
                         const uint64_t tweak,
                         const uint64_t ciphertext,
                         const uint64_t expected_plaintext) {
    small_aes36_context_t ctx;
    small_aes36_key_schedule(&ctx, key);
    
    const uint64_t plaintext = experimental_small_twaes36_decrypt(
        &ctx, tweak, ciphertext);
    ASSERT_EQ(expected_plaintext, plaintext);
}

// ---------------------------------------------------------

TEST(EXPERIMENTAL_SMALL_TWAES36, test_key_schedule) {
    const small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80
    };
    small_aes36_context_t ctx;
    small_aes36_key_schedule(&ctx, key);
    
    const uint64_t expected_round_keys[SMALL_AES36_NUM_ROUND_KEYS] = {
        0x012345678,
        0xb858c0eb8,
        0x515dd536d,
        0x601bd48b9,
        0x2d890c1b5,
        0xd3343f58a,
        0x2cd6f2378,
        0x4592ab1d3,
        0xe12cb9d6a,
        0xce305ad30,
        0x2822d8fe8
    };
    
    for (size_t i = 0; i < SMALL_AES36_NUM_ROUND_KEYS; ++i) {
        const uint64_t expected_round_key = expected_round_keys[i];
        ASSERT_EQ(expected_round_key, ctx.subkeys[i]);
    }
}

// ---------------------------------------------------------

TEST(EXPERIMENTAL_SMALL_TWAES36, test_encrypt_zeroes) {
    const small_aes36_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const uint64_t plaintext = 0x000000000L;
    const uint64_t tweak = 0x000L;
    const uint64_t expected_ciphertext = 0x62edddc2bL;
    run_encryption_test(key, tweak, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(EXPERIMENTAL_SMALL_TWAES36, test_encrypt_1234) {
    const small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80
    };
    const uint64_t plaintext = 0x02468ace3L;
    const uint64_t tweak = 0x000L;
    const uint64_t expected_ciphertext = 0xb4326dedf;
    run_encryption_test(key, tweak, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(EXPERIMENTAL_SMALL_TWAES36, test_encrypt_non_zero_tweak) {
    const small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80
    };
    const uint64_t plaintext = 0x02468ace3L;
    const uint64_t tweak = 0x123L;
    const uint64_t expected_ciphertext = 0x90767482cL;
    run_encryption_test(key, tweak, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(EXPERIMENTAL_SMALL_TWAES36, test_encrypt_two_cell_tweak) {
    const small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80
    };
    const uint64_t plaintext = 0x02468ace3L;
    const uint64_t tweak = 0x110L;
    const uint64_t expected_ciphertext = 0x4ea43fbdaL;
    run_encryption_test(key, tweak, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(EXPERIMENTAL_SMALL_TWAES36, test_encrypt_rounds) {
    const small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80
    };
    const uint64_t tweak = 0x000L;
    const uint64_t plaintext = 0x02468ace3L;
    const uint64_t expected_ciphertexts[(SMALL_AES36_NUM_ROUNDS + 1)] = {
        0x02468ace3L,
        0x5465c5649L,
        0x144f0f9bfL,
        0x8fabc94aaL,
        0x5d57e7274L,
        0x7feb0c65bL,
        0x5604c7a0dL,
        0xbc4a2fe63L,
        0xf38081d81L,
        0x8c2eb6d1dL,
        0xb4326dedfL
    };
    
    for (size_t num_rounds = 0;
         num_rounds <= SMALL_AES36_NUM_ROUNDS; ++num_rounds) {
        const uint64_t expected_ciphertext = expected_ciphertexts[num_rounds];
        run_round_encryption_test(key,
                                  tweak,
                                  plaintext,
                                  expected_ciphertext,
                                  num_rounds);
    }
}

// ---------------------------------------------------------

TEST(EXPERIMENTAL_SMALL_TWAES36, test_encrypt_rounds_zero_key) {
    const small_aes36_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const uint64_t tweak = 0x000L;
    const uint64_t plaintext = 0x000000000L;
    const uint64_t expected_ciphertexts[(SMALL_AES36_NUM_ROUNDS + 1)] = {
        0x000000000L,
        0xbaabaabaaL,
        0xcf7b91cf7L,
        0x428b0a4faL,
        0xebba9ffc4L,
        0x1e70ff5a0L,
        0xa16c49d20L,
        0xa821efad8L,
        0xc02bac74dL,
        0xc2bd621cdL,
        0x62edddc2bL
    };
    
    for (size_t num_rounds = 0;
         num_rounds <= SMALL_AES36_NUM_ROUNDS; ++num_rounds) {
        const uint64_t expected_ciphertext = expected_ciphertexts[num_rounds];
        run_round_encryption_test(key,
                                  tweak,
                                  plaintext,
                                  expected_ciphertext,
                                  num_rounds);
    }
}

// ---------------------------------------------------------

TEST(DISABLED_SMALL_AES36, test_decrypt_zeroes) {
    const small_aes36_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const uint64_t tweak = 0x000L;
    const uint64_t ciphertext = 0x62edddc2bL;
    const uint64_t expected_plaintext = 0x000000000L;
    run_decryption_test(key, tweak, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

TEST(DISABLED_SMALL_AES36, test_decrypt_non_zero_key) {
    const small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80
    };
    const uint64_t tweak = 0x000L;
    const uint64_t ciphertext = 0xb4326dedf;
    const uint64_t expected_plaintext = 0x02468ace3L;
    run_decryption_test(key, tweak, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

TEST(DISABLED_SMALL_AES36, test_decrypt_non_zero_tweak) {
    const small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80
    };
    const uint64_t tweak = 0x123L;
    const uint64_t ciphertext = 0xb4326dedf;
    const uint64_t expected_plaintext = 0x02468ace3L;
    run_decryption_test(key, tweak, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// ---------------------------------------------------------
