/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdint>
#include <gtest/gtest.h>

#include "ciphers/small_present20.h"
#include "utils/utils.h"


using ciphers::small_present20_context_t;
using ciphers::small_present20_key_t;
using ciphers::small_present20_state_t;
using utils::assert_equal;

// ---------------------------------------------------------

static
void run_encryption_test(const small_present20_key_t key,
                         const small_present20_state_t plaintext,
                         const small_present20_state_t expected_ciphertext) {
    small_present20_context_t ctx;
    small_present20_key_schedule(&ctx, key);
    
    small_present20_state_t ciphertext;
    small_present20_encrypt(&ctx, plaintext, ciphertext);
    ASSERT_TRUE(assert_equal(expected_ciphertext, ciphertext,
                             (size_t) SMALL_PRESENT20_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

static
void run_decryption_test(const small_present20_key_t key,
                         const small_present20_state_t ciphertext,
                         const small_present20_state_t expected_plaintext) {
    small_present20_context_t ctx;
    small_present20_key_schedule(&ctx, key);
    
    small_present20_state_t plaintext;
    small_present20_decrypt(&ctx, ciphertext, plaintext);
    ASSERT_TRUE(assert_equal(expected_plaintext, plaintext,
                             (size_t) SMALL_PRESENT20_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

TEST(SMALL_PRESENT20, test_key_schedule) {
    const small_present20_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    small_present20_context_t ctx;
    small_present20_key_schedule(&ctx, key);

    uint8_t round_key[SMALL_PRESENT20_NUM_ROUND_KEY_BYTES];

    const uint8_t expected_round_keys[
        SMALL_PRESENT20_NUM_ROUND_KEY_BYTES * SMALL_PRESENT20_NUM_ROUND_KEYS
    ] = {
            0x00, 0x00, 0x00,  0x00, 0x00, 0x00,  0x00, 0x00, 0x10,  0x00, 0x00, 0x10,
            0x00, 0x06, 0x20,  0x00, 0x02, 0xa0,  0x00, 0x03, 0x30,  0x00, 0x05, 0xb0,
            0x00, 0x64, 0xc0,  0x00, 0x28, 0x40,  0x00, 0x35, 0x50,  0x00, 0x5e, 0xd0,
            0x06, 0x49, 0xe0,  0x02, 0x87, 0x60,  0x03, 0x57, 0xf0,  0x05, 0xec, 0x70,
            0x64, 0x9a, 0x80,  0x28, 0x73, 0x00,  0x35, 0x78, 0x10,  0x5e, 0xc1, 0x90,
            0x49, 0xae, 0xa0,  0x87, 0x36, 0xe0,  0x57, 0x83, 0xe0,  0xec, 0x1d, 0x50,
            0x9a, 0xe8, 0xf0,  0x73, 0x6a, 0xd0,  0x78, 0x3a, 0xd0,  0xc1, 0xd7, 0x70,
            0xae, 0x89, 0xb0,  0x36, 0xac, 0xd0,  0x83, 0xac, 0x90,  0x1d, 0x70, 0x00
        };

    for (size_t i = 0; i < SMALL_PRESENT20_NUM_ROUND_KEYS; ++i) {
        const uint8_t *start_expected_round_key =
            expected_round_keys + i * SMALL_PRESENT20_NUM_ROUND_KEY_BYTES;
        
        round_key[0] = (ctx.subkeys[i] >> 12) & 0xFF;
        round_key[1] = (ctx.subkeys[i] >>  4) & 0xFF;
        round_key[2] = (ctx.subkeys[i] <<  4) & 0xF0;

        ASSERT_TRUE(utils::assert_equal(start_expected_round_key, round_key,
                                        SMALL_PRESENT20_NUM_ROUND_KEY_BYTES));
    }
}

// ---------------------------------------------------------

TEST(SMALL_PRESENT20, test_encrypt_full) {
    const small_present20_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    const small_present20_state_t plaintext = {
        0x00, 0x00, 0x00
    };
    const small_present20_state_t expected_ciphertext = {
        0x11, 0xff, 0xe0
    };
    run_encryption_test(key, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(SMALL_PRESENT20, test_decrypt_full) {
    const small_present20_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    const small_present20_state_t ciphertext = {
        0x11, 0xff, 0xe0
    };
    const small_present20_state_t expected_plaintext = {
        0x00, 0x00, 0x00
    };
    run_decryption_test(key, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// ---------------------------------------------------------
