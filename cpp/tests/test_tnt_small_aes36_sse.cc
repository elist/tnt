/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdint>
#include <gtest/gtest.h>

#include "ciphers/tnt_small_aes36.h"
#include "utils/utils.h"


using ciphers::tnt_small_aes36_sse_context_t;
using ciphers::tnt_small_aes36_key_t;
using ciphers::tnt_small_aes36_state_t;
using utils::assert_equal;
using utils::print_128;

// ---------------------------------------------------------

static
void run_encryption_test(const tnt_small_aes36_key_t key,
                         const tnt_small_aes36_state_t tweak,
                         const tnt_small_aes36_state_t plaintext,
                         const tnt_small_aes36_state_t expected_ciphertext) {
    tnt_small_aes36_sse_context_t ctx;
    tnt_small_aes36_key_schedule(&ctx, key);
    
    tnt_small_aes36_state_t ciphertext;
    tnt_small_aes36_encrypt(&ctx, tweak, plaintext, ciphertext);
    ASSERT_TRUE(assert_equal(expected_ciphertext, ciphertext,
                             (size_t) TNT_SMALL_AES36_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

static
void run_first_part_encryption_test(
    const tnt_small_aes36_key_t key,
    const tnt_small_aes36_state_t tweak,
    const tnt_small_aes36_state_t plaintext,
    const tnt_small_aes36_state_t expected_ciphertext,
    const size_t num_rounds) {
    tnt_small_aes36_sse_context_t ctx;
    tnt_small_aes36_key_schedule(&ctx, key);
    
    tnt_small_aes36_state_t ciphertext;
    tnt_small_aes36_encrypt_first_part(&ctx, tweak, plaintext, ciphertext,
                                       num_rounds);
    ASSERT_TRUE(assert_equal(expected_ciphertext, ciphertext,
                             (size_t) TNT_SMALL_AES36_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

static
void run_decryption_test(const tnt_small_aes36_key_t key,
                         const tnt_small_aes36_state_t tweak,
                         const tnt_small_aes36_state_t ciphertext,
                         const tnt_small_aes36_state_t expected_plaintext) {
    tnt_small_aes36_sse_context_t ctx;
    tnt_small_aes36_key_schedule(&ctx, key);
    
    tnt_small_aes36_state_t plaintext;
    tnt_small_aes36_decrypt(&ctx, tweak, ciphertext, plaintext);
    ASSERT_TRUE(assert_equal(expected_plaintext, plaintext,
                             (size_t) TNT_SMALL_AES36_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

TEST(TNT_SMALL_AES36_SSE, test_key_schedule) {
    const tnt_small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80,
        0xab, 0xcd, 0xef, 0xfe, 0xd0,
        0xba, 0x98, 0x76, 0x54, 0x30
    };
    tnt_small_aes36_sse_context_t ctx;
    tnt_small_aes36_key_schedule(&ctx, key);
    
    const uint8_t
        expected_round_keys[TNT_NUM_ROUNDS][9 * SMALL_AES36_NUM_ROUND_KEYS + 16] = {
        {
            0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8,
            0xb, 0x8, 0x5, 0x8, 0xc, 0x0, 0xe, 0xb, 0x8,
            0x5, 0x1, 0x5, 0xd, 0xd, 0x5, 0x3, 0x6, 0xd,
            0x6, 0x0, 0x1, 0xb, 0xd, 0x4, 0x8, 0xb, 0x9,
            0x2, 0xd, 0x8, 0x9, 0x0, 0xc, 0x1, 0xb, 0x5,
            0xd, 0x3, 0x3, 0x4, 0x3, 0xf, 0x5, 0x8, 0xa,
            0x2, 0xc, 0xd, 0x6, 0xf, 0x2, 0x3, 0x7, 0x8,
            0x4, 0x5, 0x9, 0x2, 0xa, 0xb, 0x1, 0xd, 0x3,
            0xe, 0x1, 0x2, 0xc, 0xb, 0x9, 0xd, 0x6, 0xa,
            0xc, 0xe, 0x3, 0x0, 0x5, 0xa, 0xd, 0x3, 0x0,
            0x2, 0x8, 0x2, 0x2, 0xd, 0x8, 0xf, 0xe, 0x8,
            0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
            0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
        },
        {
            0xa, 0xb, 0xc, 0xd, 0xe, 0xf, 0xf, 0xe, 0xd,
            0xb, 0xa, 0x4, 0x6, 0x4, 0xb, 0x9, 0xa, 0x6,
            0x6, 0xd, 0x9, 0x0, 0x9, 0x2, 0x9, 0x3, 0x4,
            0x6, 0xf, 0x4, 0x6, 0x6, 0x6, 0xf, 0x5, 0x2,
            0x0, 0xa, 0xc, 0x6, 0xc, 0xa, 0x9, 0x9, 0x8,
            0xe, 0x3, 0x1, 0x8, 0xf, 0xb, 0x1, 0x6, 0x3,
            0xf, 0x7, 0xa, 0x7, 0x8, 0x1, 0x6, 0xe, 0x2,
            0x3, 0x2, 0xd, 0x4, 0xa, 0xc, 0x2, 0x4, 0xe,
            0xa, 0x2, 0x8, 0xe, 0x8, 0x4, 0xc, 0xc, 0xa,
            0xc, 0xd, 0xb, 0x2, 0x5, 0xf, 0xe, 0x9, 0x5,
            0xb, 0x3, 0xb, 0x9, 0x6, 0x4, 0x7, 0xf, 0x1,
            0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
            0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
        },
        {
            0xb, 0xa, 0x9, 0x8, 0x7, 0x6, 0x5, 0x4, 0x3,
            0x8, 0xe, 0x7, 0x0, 0x9, 0x1, 0x5, 0xd, 0x2,
            0xb, 0xb, 0x9, 0xb, 0x2, 0x8, 0xe, 0xf, 0xa,
            0x7, 0x4, 0x9, 0xc, 0x6, 0x1, 0x2, 0x9, 0xb,
            0x2, 0x8, 0xc, 0xe, 0xe, 0xd, 0xc, 0x7, 0x6,
            0xb, 0xf, 0xf, 0x5, 0x1, 0x2, 0x9, 0x6, 0x4,
            0xa, 0xd, 0x2, 0xf, 0xc, 0x0, 0x6, 0xa, 0x4,
            0x9, 0xf, 0x5, 0x6, 0x3, 0x5, 0x0, 0x9, 0x1,
            0xf, 0x4, 0x3, 0x9, 0x7, 0x6, 0x9, 0xe, 0x7,
            0xa, 0xe, 0xe, 0x3, 0x9, 0x8, 0xa, 0x7, 0xf,
            0xa, 0x6, 0x1, 0x9, 0xf, 0x9, 0x3, 0x8, 0x6,
            0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
            0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
        }
    };
    
    uint8_t round_key[16];
    
    for (size_t j = 0; j < TNT_NUM_ROUNDS; ++j) {
        for (size_t i = 0; i < SMALL_AES36_NUM_ROUND_KEYS; ++i) {
            const uint8_t *start_expected_round_key = expected_round_keys[j] + i * 9;
            storeu(round_key, ctx.permutation_ctx[j].subkeys[i]);
//            printf("%2zu %2zu ", j, i);
//            print_128("", ctx.permutation_ctx[j].subkeys[i]);
            ASSERT_TRUE(assert_equal(start_expected_round_key, round_key, 9));
        }
    }
}

// ---------------------------------------------------------

TEST(TNT_SMALL_AES36_SSE, test_encrypt_zeroes) {
    const tnt_small_aes36_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const tnt_small_aes36_state_t tweak = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const tnt_small_aes36_state_t plaintext = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const tnt_small_aes36_state_t expected_ciphertext = {
        0xf6, 0xd5, 0xcb, 0xc7, 0xe0
    };
    run_encryption_test(key, tweak, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(TNT_SMALL_AES36_SSE, test_encrypt_1234) {
    const tnt_small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80,
        0xab, 0xcd, 0xef, 0xfe, 0xd0,
        0xba, 0x98, 0x76, 0x54, 0x30
    };
    const tnt_small_aes36_state_t tweak = {
        0x02, 0x46, 0x8a, 0xce, 0x10
    };
    const tnt_small_aes36_state_t plaintext = {
        0x02, 0x46, 0x8a, 0xce, 0x30
    };
    const tnt_small_aes36_state_t expected_ciphertext = {
        0xfc, 0x95, 0xab, 0xb3, 0x00
    };
    run_encryption_test(key, tweak, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(TNT_SMALL_AES36_SSE, test_encrypt_first_part) {
    const tnt_small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80,
        0xab, 0xcd, 0xef, 0xfe, 0xd0,
        0xba, 0x98, 0x76, 0x54, 0x30
    };
    const tnt_small_aes36_state_t tweak = {
        0x02, 0x46, 0x8a, 0xce, 0x10
    };
    const tnt_small_aes36_state_t plaintext = {
        0x02, 0x46, 0x8a, 0xce, 0x30
    };
    const tnt_small_aes36_state_t expected_ciphertext = {
        0x7f, 0xeb, 0x0c, 0x65, 0xb0
    };
    run_first_part_encryption_test(key, tweak, plaintext, expected_ciphertext,
                                   TNT_SMALL_AES36_NUM_PART_ROUNDS);
}

// ---------------------------------------------------------

TEST(DISABLED_TNT_SMALL_AES36, test_decrypt_full) {
    const tnt_small_aes36_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const tnt_small_aes36_state_t tweak = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const tnt_small_aes36_state_t ciphertext = {
        0x5f, 0x5e, 0x1c, 0x7e, 0x70
    };
    const tnt_small_aes36_state_t expected_plaintext = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    run_decryption_test(key, tweak, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// ---------------------------------------------------------
