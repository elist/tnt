/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdint>
#include <gtest/gtest.h>

#include "ciphers/tnt_small_aes36.h"
#include "utils/utils.h"


using ciphers::tnt_small_aes36_context_t;
using ciphers::tnt_small_aes36_context_t;
using ciphers::tnt_small_aes36_key_t;
using ciphers::tnt_small_aes36_state_t;
using utils::assert_equal;

// ---------------------------------------------------------

static
void run_encryption_test(const tnt_small_aes36_key_t key,
                         const tnt_small_aes36_state_t tweak,
                         const tnt_small_aes36_state_t plaintext,
                         const tnt_small_aes36_state_t expected_ciphertext) {
    tnt_small_aes36_context_t ctx;
    tnt_small_aes36_key_schedule(&ctx, key);
    
    tnt_small_aes36_state_t ciphertext;
    tnt_small_aes36_encrypt(&ctx, tweak, plaintext, ciphertext);
    ASSERT_TRUE(assert_equal(expected_ciphertext, ciphertext,
                             (size_t) TNT_SMALL_AES36_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

static
void run_first_part_encryption_test(
    const tnt_small_aes36_key_t key,
    const tnt_small_aes36_state_t tweak,
    const tnt_small_aes36_state_t plaintext,
    const tnt_small_aes36_state_t expected_ciphertext,
    const size_t num_rounds) {
    tnt_small_aes36_context_t ctx;
    tnt_small_aes36_key_schedule(&ctx, key);
    
    tnt_small_aes36_state_t ciphertext;
    tnt_small_aes36_encrypt_first_part(&ctx, tweak, plaintext, ciphertext,
                                       num_rounds);
    ASSERT_TRUE(assert_equal(expected_ciphertext, ciphertext,
                             (size_t) TNT_SMALL_AES36_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

static
void run_decryption_test(const tnt_small_aes36_key_t key,
                         const tnt_small_aes36_state_t tweak,
                         const tnt_small_aes36_state_t ciphertext,
                         const tnt_small_aes36_state_t expected_plaintext) {
    tnt_small_aes36_context_t ctx;
    tnt_small_aes36_key_schedule(&ctx, key);
    
    tnt_small_aes36_state_t plaintext;
    tnt_small_aes36_decrypt(&ctx, tweak, ciphertext, plaintext);
    ASSERT_TRUE(assert_equal(expected_plaintext, plaintext,
                             (size_t) TNT_SMALL_AES36_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

TEST(TNT_SMALL_AES36, test_key_schedule) {
    const tnt_small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80,
        0xab, 0xcd, 0xef, 0xfe, 0xd0,
        0xba, 0x98, 0x76, 0x54, 0x30
    };
    tnt_small_aes36_context_t ctx;
    tnt_small_aes36_key_schedule(&ctx, key);
    
    const uint64_t
        expected_round_keys[TNT_NUM_ROUNDS][SMALL_AES36_NUM_ROUND_KEYS] = {
        {
            0x012345678,
            0xb858c0eb8,
            0x515dd536d,
            0x601bd48b9,
            0x2d890c1b5,
            0xd3343f58a,
            0x2cd6f2378,
            0x4592ab1d3,
            0xe12cb9d6a,
            0xce305ad30,
            0x2822d8fe8
        },
        {
            0xabcdeffed,
            0xba464b9a6,
            0x6d9092934,
            0x6f4666f52,
            0x0ac6ca998,
            0xe318fb163,
            0xf7a7816e2,
            0x32d4ac24e,
            0xa28e84cca,
            0xcdb25fe95,
            0xb3b9647f1
        },
        {
            0xba9876543,
            0x8e70915d2,
            0xbb9b28efa,
            0x749c6129b,
            0x28ceedc76,
            0xbff512964,
            0xad2fc06a4,
            0x9f5635091,
            0xf439769e7,
            0xaee398a7f,
            0xa619f9386
        }
    };
    
    for (size_t j = 0; j < TNT_NUM_ROUNDS; ++j) {
        for (size_t i = 0; i < SMALL_AES36_NUM_ROUND_KEYS; ++i) {
            const uint64_t expected_round_key = expected_round_keys[j][i];
//            printf("0x%09lx\n", ctx.permutation_ctx[j].subkeys[i]);
            ASSERT_EQ(expected_round_key, ctx.permutation_ctx[j].subkeys[i]);
        }
    }
}

// ---------------------------------------------------------

TEST(TNT_SMALL_AES36, test_encrypt_zeroes) {
    const tnt_small_aes36_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const tnt_small_aes36_state_t tweak = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const tnt_small_aes36_state_t plaintext = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const tnt_small_aes36_state_t expected_ciphertext = {
        0xf6, 0xd5, 0xcb, 0xc7, 0xe0
    };
    run_encryption_test(key, tweak, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(TNT_SMALL_AES36, test_encrypt_1234) {
    const tnt_small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80,
        0xab, 0xcd, 0xef, 0xfe, 0xd0,
        0xba, 0x98, 0x76, 0x54, 0x30
    };
    const tnt_small_aes36_state_t tweak = {
        0x02, 0x46, 0x8a, 0xce, 0x10
    };
    const tnt_small_aes36_state_t plaintext = {
        0x02, 0x46, 0x8a, 0xce, 0x30
    };
    const tnt_small_aes36_state_t expected_ciphertext = {
        0xfc, 0x95, 0xab, 0xb3, 0x00
    };
    run_encryption_test(key, tweak, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(TNT_SMALL_AES36, test_encrypt_first_part) {
    const tnt_small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80,
        0xab, 0xcd, 0xef, 0xfe, 0xd0,
        0xba, 0x98, 0x76, 0x54, 0x30
    };
    const tnt_small_aes36_state_t tweak = {
        0x02, 0x46, 0x8a, 0xce, 0x10
    };
    const tnt_small_aes36_state_t plaintext = {
        0x02, 0x46, 0x8a, 0xce, 0x30
    };
    const tnt_small_aes36_state_t expected_ciphertext = {
        0x7f, 0xeb, 0x0c, 0x65, 0xb0
    };
    run_first_part_encryption_test(key, tweak, plaintext, expected_ciphertext,
                                   TNT_SMALL_AES36_NUM_PART_ROUNDS);
}

// ---------------------------------------------------------

TEST(DISABLED_TNT_SMALL_AES36, test_decrypt_full) {
    const tnt_small_aes36_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const tnt_small_aes36_state_t tweak = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const tnt_small_aes36_state_t ciphertext = {
        0x5f, 0x5e, 0x1c, 0x7e, 0x70
    };
    const tnt_small_aes36_state_t expected_plaintext = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    run_decryption_test(key, tweak, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// ---------------------------------------------------------
