/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <array>
#include <functional>
#include <memory>
#include <vector>
#include <cstdint>
#include <cstdlib>
#include <cstring>

#include "ciphers/small_aes36.h"
#include "ciphers/tnt_small_aes36.h"
#include "utils/argparse.h"
#include "utils/utils.h"
#include "utils/xorshift1024.h"

using ciphers::tnt_small_aes36_sse_context_t;
using ciphers::tnt_small_aes36_state_t;
using ciphers::tnt_small_aes36_tweak_t;
using ciphers::tnt_small_aes36_key_t;
using utils::assert_equal;
using utils::compute_mean;
using utils::compute_variance;
using utils::xor_arrays;
using utils::zeroize_array;
using utils::ArgumentParser;

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

static const size_t NUM_BITS = 36; // The state size in bits

// For this experiment, we set 8 bits to zero after pi_1 and use 16 bits
// effectively for the tweaks.
static const size_t NUM_USED_TWEAK_BITS = 24;
static const size_t NUM_KEY_CANDIDATES = 1 << 12;

// We only store 2^12 messages in the table
static const size_t NUM_LSB_BITS = 16;
static const auto NUM_STORED_VALUES = static_cast<size_t>(1L) << NUM_LSB_BITS;
static const size_t NUM_MESSAGES = 1 << 12;

static const size_t LSB_MASK = (1L << NUM_LSB_BITS) - 1;
static const size_t MSB_MASK = ((1L << NUM_BITS) - 1) ^LSB_MASK;
static const uint64_t DIFFERENCE_AFTER_PI_1 = 0x00F0F0F00;

// ---------------------------------------------------------
// Types
// ---------------------------------------------------------

typedef uint64_t Ciphertext;
typedef std::vector<Ciphertext> CiphertextsVector;
typedef std::array<std::vector<size_t>, NUM_STORED_VALUES> Table;
typedef std::shared_ptr<Table> TablePointer;
typedef std::array<Ciphertext, NUM_MESSAGES> CiphertextsList;

typedef std::pair<Ciphertext, Ciphertext> CiphertextTweakTuple;
typedef std::vector<CiphertextTweakTuple> CiphertextTweakTupleVector;
typedef std::array<CiphertextTweakTupleVector, NUM_STORED_VALUES>
    TweakIndices;
typedef std::shared_ptr<TweakIndices> TweakIndicesPointer;

typedef std::function<void(const tnt_small_aes36_sse_context_t *,
                           tnt_small_aes36_tweak_t,
                           tnt_small_aes36_state_t,
                           tnt_small_aes36_state_t)> EncryptFunction;

typedef struct {
    tnt_small_aes36_key_t key;
    tnt_small_aes36_sse_context_t cipher_ctx;
    size_t num_keys = 0;
    size_t num_texts_per_set = 0;
    size_t num_texts_per_set_log = 0;
    size_t num_desired_possible_message_pairs = 0;
    std::vector<size_t> num_matches;
    bool be_verbose = false;
    bool use_tprp = false;
    bool use_wrong_messages = false;
} ExperimentContext;

typedef struct {
    std::vector<size_t> num_collisions_per_set;
    size_t num_collisions = 0;
} ExperimentResult;

// ---------------------------------------------------------
// Functions
// ---------------------------------------------------------

static uint64_t to_integer(const tnt_small_aes36_state_t state) {
    return ciphers::small_aes36_to_integer(state);
}

// ---------------------------------------------------------

static void split(uint64_t &high,
                  uint64_t &lo,
                  const uint64_t &value) {
    high = (value & MSB_MASK) >> NUM_LSB_BITS;
    lo = value & LSB_MASK;
}

// ---------------------------------------------------------

static void to_array(tnt_small_aes36_state_t result, const uint64_t state) {
    ciphers::small_aes36_to_byte_array(result, state);
}

// ---------------------------------------------------------

/**
 * Expand the 12-bit value m = (m_0, m_1, m_2) into the first diagonal:
 * m_0 .   .
 * .   m_1 .
 * .   .   m_2
 *
 * @param plaintext
 * @param base_plaintext
 * @param m
 */
static void generate_plaintext(tnt_small_aes36_state_t plaintext,
                               const tnt_small_aes36_state_t base_plaintext,
                               const size_t m) {
    memcpy(plaintext, base_plaintext, SMALL_AES36_NUM_STATE_BYTES);
    plaintext[0] = (plaintext[0] & 0x0F) | ((m >> 4) & 0xF0);
    plaintext[2] = (plaintext[2] & 0x0F) | (m & 0xF0);
    plaintext[4] = (m << 4) & 0xF0;
}

// ---------------------------------------------------------

/**
 * Encodes the t-bit tweak to a 24-bit value (0 || t) and stores the encoded
 * tweak i = (x_0, x_1, x_2, i_0, i_1, i_2, i_3, i_4, i_5) into
 * i_0 i_2 x_2
 * i_1 x_1 i_4
 * x_0 i_3 i_5
 *
 * @param tweak
 * @param index
 */
static uint64_t generate_set0_tweak_as_int(const size_t index) {
    const uint64_t tweak = ((index & 0xFF0000) << 12)
                           | ((index & 0x00F000) << 8)
                           | ((index & 0x000F00) << 4)
                           | (index & 0x0000FF)
                           | ((index & 0xF00000000) >> 8)
                           | ((index & 0x0F0000000) >> 12)
                           | ((index & 0x00F000000) >> 16);

//    printf("T  i %8zu %09lx\n", index, tweak);
    
    return ciphers::small_aes36_mix_columns(tweak);
}

// ---------------------------------------------------------

/**
 * Encodes the t-bit tweak to a 24-bit value (t || 0) and stores the encoded
 * tweak j = (j_0, j_1, j_2, j_3, j_4, j_5) into
 * j_0 j_2 x_2
 * j_1 x_1 j_4
 * x_0 j_3 j_5
 *
 * @param tweak
 * @param index
 */
static uint64_t generate_set1_tweak_as_int(const size_t index,
                                           const size_t num_bits) {
    // For example, 16 - 12,13,14... = 4,3,2
    // means shift j << (16 - 4,2,3)
    const size_t shift = num_bits <= NUM_USED_TWEAK_BITS ?
                         NUM_USED_TWEAK_BITS - num_bits :
                         0;
    const uint64_t shifted_index = index << shift;
    const uint64_t tweak = ((shifted_index & 0xFF0000) << 12)
                           | ((shifted_index & 0x00F000) << 8)
                           | ((shifted_index & 0x000F00) << 4)
                           | (shifted_index & 0x0000FF)
                           | ((shifted_index & 0xF00000000) >> 8)
                           | ((shifted_index & 0x0F0000000) >> 12)
                           | ((shifted_index & 0x00F000000) >> 16);

//    printf("T' j %8zu %09lx\n", index, tweak);
    
    return ciphers::small_aes36_mix_columns(tweak);
}

// ---------------------------------------------------------

/**
 * We set the LSB to a single 1 bit since this bit is active only in an
 * impossible-differential output difference.
 * @param tweak
 * @param index
 */
static void generate_set0_tweak(tnt_small_aes36_state_t tweak,
                                const size_t index) {
    const uint64_t tweak_as_int = generate_set0_tweak_as_int(index);
    to_array(tweak, tweak_as_int);
}

// ---------------------------------------------------------

/**
 * num_bits = log(num_sets_per_key), e.g. 11, 12, 13.
 * @param tweak
 * @param index
 * @param num_bits
 */
static void generate_set1_tweak(tnt_small_aes36_state_t tweak,
                                const size_t index,
                                const size_t num_bits) {
    const uint64_t tweak_as_int = generate_set1_tweak_as_int(index, num_bits);
    to_array(tweak, tweak_as_int);
}

// ---------------------------------------------------------

static void encrypt_random(const tnt_small_aes36_sse_context_t *ctx,
                           const tnt_small_aes36_tweak_t tweak,
                           const tnt_small_aes36_state_t plaintext,
                           tnt_small_aes36_state_t ciphertext) {
    // ---------------------------------------------------------
    // Suppress unused parameter warnings. We need those parameters
    // here only to be compliant with the real encryption function.
    // ---------------------------------------------------------
    
    (void) ctx;
    (void) tweak;
    (void) plaintext;
    utils::get_random_bytes(ciphertext, TNT_SMALL_AES36_NUM_STATE_BYTES);
}

// ---------------------------------------------------------

static void encrypt_real(const tnt_small_aes36_sse_context_t *ctx,
                         const tnt_small_aes36_tweak_t tweak,
                         const tnt_small_aes36_state_t plaintext,
                         tnt_small_aes36_state_t ciphertext) {
    ciphers::tnt_small_aes36_encrypt(ctx,
                                     tweak,
                                     plaintext,
                                     ciphertext);
}

// ---------------------------------------------------------

static void encrypt_real_first_part(const tnt_small_aes36_sse_context_t *ctx,
                                    const tnt_small_aes36_tweak_t tweak,
                                    const tnt_small_aes36_state_t plaintext,
                                    tnt_small_aes36_state_t ciphertext) {
    ciphers::tnt_small_aes36_encrypt_first_part(ctx,
                                                tweak,
                                                plaintext,
                                                ciphertext);
}

// ---------------------------------------------------------

static void clear_lists(TweakIndicesPointer &lists) {
    lists->fill(CiphertextTweakTupleVector());
}

// ---------------------------------------------------------

static void clear_tables(TablePointer &table) {
    (*table).fill(std::vector<size_t>());
}

// ---------------------------------------------------------

static bool is_difference_possible(const Ciphertext c_i,
                                   const Ciphertext c_j) {
    return ((c_i ^ c_j) & DIFFERENCE_AFTER_PI_1) == 0;
}

// ---------------------------------------------------------

static bool is_first_round_difference_possible(const Ciphertext m_i,
                                               const Ciphertext m_j) {
    const Ciphertext difference = m_i ^m_j;
    return ((difference & 0xF00000000) != 0)
           && ((difference & 0x0000F0000) != 0)
           && ((difference & 0x00000000F) != 0);
}

// ---------------------------------------------------------

static
size_t find_num_collisions_in_d(const std::vector<size_t> &entries,
                                const uint64_t msb_delta_t,
                                size_t &num_find_num_collisions_in_d_calls) {
    size_t num_collisions = 0;
    
    for (const size_t &previous_msb_delta_t : entries) {
        if (previous_msb_delta_t == msb_delta_t) {
            num_collisions++;
        }
        
        num_find_num_collisions_in_d_calls++;
    }
    
    return num_collisions;
}

// ---------------------------------------------------------

static size_t find_num_collisions(const TweakIndicesPointer &lists,
                                  const Ciphertext msb_ciphertext_l,
                                  const Ciphertext lsb_ciphertext_l,
                                  const Ciphertext t_l,
                                  TablePointer &table,
                                  const size_t num_texts_per_set_log,
                                  size_t &num_find_num_collisions_calls,
                                  size_t &num_find_num_collisions_in_d_calls) {
    size_t num_collisions = 0;
    
    const CiphertextTweakTupleVector &previous_ciphertexts =
        (*lists)[lsb_ciphertext_l];
    
    for (const CiphertextTweakTuple &item : previous_ciphertexts) {
        if (item.second != msb_ciphertext_l) { // C^1_k != C^1_l
            continue;
        }
        
        // We found a pair (C_k, C_l) that collides
        
        num_find_num_collisions_calls++;
        
        // ---------------------------------------------------------
        // Add Delta T^1_{k,l} = (T^1_k, T^1_l)
        // msb_delta_t, lsb_delta_t = msb(Delta T^1_{k,l}), lsb(Delta T^1_{k,l})
        // Append msb_delta_t to D[lsb_delta_t]
        // ---------------------------------------------------------
        
        const uint64_t t_k_index = item.first; // T^1_l
        const uint64_t t_k = generate_set1_tweak_as_int(t_k_index,
                                                        num_texts_per_set_log);
        const uint64_t delta_t = t_l ^t_k;
        
        uint64_t msb_delta_t;
        uint64_t lsb_delta_t;
        split(msb_delta_t, lsb_delta_t, delta_t);
        
        const std::vector<size_t> &entries = (*table)[lsb_delta_t];
        num_collisions += find_num_collisions_in_d(
            entries,
            msb_delta_t,
            num_find_num_collisions_in_d_calls);
    }
    
    return num_collisions;
}

// ---------------------------------------------------------

static void investigate(const TweakIndicesPointer &lists,
                        const Ciphertext msb_ciphertext_j,
                        const Ciphertext lsb_ciphertext_j,
                        const Ciphertext t_j,
                        TablePointer &table,
                        size_t &num_calls) {
    const CiphertextTweakTupleVector &previous_ciphertexts =
        (*lists)[lsb_ciphertext_j];
    
    for (const CiphertextTweakTuple &item : previous_ciphertexts) {
        if (item.second != msb_ciphertext_j) { // C^0_i != C^0_j
            continue;
        }

//        printf("C_i (msb, lsb): %09lx %09lx - item: %09lx\n",
//               msb_ciphertext_j, lsb_ciphertext_j, item.second);
        
        num_calls++;
        
        // ---------------------------------------------------------
        // Add Delta T^0_{i,j} = (T^0_i, T^0_j)
        // msb_delta_t, lsb_delta_t = msb(Delta T^0_{i,j}), lsb(Delta T^0_{i,j})
        // Append msb_delta_t to D[lsb_delta_t]
        // ---------------------------------------------------------
        
        const uint64_t t_index = item.first; // T^0_i
        const uint64_t t_i = generate_set0_tweak_as_int(t_index);
        const uint64_t delta_t = t_i ^t_j;
        
        uint64_t msb_delta_t;
        uint64_t lsb_delta_t;
        split(msb_delta_t, lsb_delta_t, delta_t);
        
        (*table)[lsb_delta_t].push_back(msb_delta_t);
    }
}

// ---------------------------------------------------------

static bool find_possible_messages(
    ExperimentContext *context,
    const EncryptFunction &first_part_encryption_function,
    CiphertextsVector &possible_messages_i,
    CiphertextsVector &possible_messages_j,
    const bool use_wrong_messages) {
    // ---------------------------------------------------------
    // Clear the good message pairs
    // ---------------------------------------------------------
    
    possible_messages_i.clear();
    possible_messages_j.clear();
    
    tnt_small_aes36_state_t tweak;
    tnt_small_aes36_state_t ciphertext;
    tnt_small_aes36_state_t base_plaintext;
    tnt_small_aes36_state_t plaintext;
    tnt_small_aes36_sse_context_t cipher_ctx = context->cipher_ctx;
    
    CiphertextsList first_part_ciphertexts;
    size_t num_possible_message_pairs = 0;
    
    // ---------------------------------------------------------
    // Random base plaintext
    // ---------------------------------------------------------
    
    utils::get_random_bytes(base_plaintext,
                            TNT_SMALL_AES36_NUM_STATE_BYTES);
    memset(tweak, 0x00, TNT_SMALL_AES36_NUM_STATE_BYTES);
    
    // ---------------------------------------------------------
    // Plaintexts M^i
    // ---------------------------------------------------------
    
    for (size_t m = 0; m < NUM_MESSAGES; ++m) {
        generate_plaintext(plaintext, base_plaintext, m);
        first_part_encryption_function(&cipher_ctx, tweak, plaintext,
                                       ciphertext);
        first_part_ciphertexts[m] = to_integer(ciphertext);
    }
    
    // ---------------------------------------------------------
    // Plaintexts M^j
    // ---------------------------------------------------------
    
    for (size_t m_i = 0; m_i < NUM_MESSAGES; ++m_i) {
        for (size_t m_j = m_i + 1; m_j < NUM_MESSAGES; ++m_j) {
            if (is_difference_possible(first_part_ciphertexts[m_i],
                                       first_part_ciphertexts[m_j])
                == use_wrong_messages) {
                continue;
            }
            
            generate_plaintext(plaintext, base_plaintext, m_i);
            const uint64_t message_i = to_integer(plaintext);
            
            generate_plaintext(plaintext, base_plaintext, m_j);
            const uint64_t message_j = to_integer(plaintext);
            
            if (is_first_round_difference_possible(message_i, message_j)
                == use_wrong_messages) {
                continue;
            }
            
            possible_messages_i.push_back(message_i);
            possible_messages_j.push_back(message_j);
            num_possible_message_pairs++;
            
            if (context->be_verbose) {
                printf("# M^i: %2zu %09lx\n", m_i, message_i);
                printf("# M^j: %2zu %09lx\n", m_j, message_j);
                printf("# S^i: %2zu %09lx\n", m_i, first_part_ciphertexts[m_i]);
                printf("# S^j: %2zu %09lx\n", m_j, first_part_ciphertexts[m_j]);
            }
            
            if (num_possible_message_pairs >=
                context->num_desired_possible_message_pairs) {
                return true;
            }
        }
    }
    
    return false;
}

// ---------------------------------------------------------

static size_t generate_lists(ExperimentContext *context,
                             const EncryptFunction &encryption_function,
                             TweakIndicesPointer &lists,
                             const Ciphertext &possible_message_i,
                             const Ciphertext &possible_message_j) {
    tnt_small_aes36_sse_context_t cipher_ctx = context->cipher_ctx;
    
    // ---------------------------------------------------------
    // Prepare the tables D_i, for i = 1..NUM_MESSAGES-1
    // ---------------------------------------------------------
    
    TablePointer num_pairs_per_difference(new Table());
    clear_tables(num_pairs_per_difference);
    
    // ---------------------------------------------------------
    // Prepare
    // ---------------------------------------------------------
    
    tnt_small_aes36_state_t tweak;
    tnt_small_aes36_state_t ciphertext;
    tnt_small_aes36_state_t plaintext;
    
    Ciphertext ciphertext_as_int;
    
    // ---------------------------------------------------------
    // Set to the possible plaintext M^i
    // ---------------------------------------------------------
    
    to_array(plaintext, possible_message_i);
    
    // ---------------------------------------------------------
    // Trackers
    // ---------------------------------------------------------
    
    size_t num_investigate_calls = 0;
    size_t num_find_num_collisions_calls = 0;
    size_t num_find_num_collisions_in_d_calls = 0;
    
    // ---------------------------------------------------------
    // Fill list with encryptions of first (T^i, M) pairs
    // ---------------------------------------------------------
    
    uint64_t msb_ciphertext_j;
    uint64_t lsb_ciphertext_j;
    
    if (context->be_verbose) {
        puts("# i C_i");
    }
    
    for (size_t i = 0; i < context->num_texts_per_set; ++i) {
        generate_set0_tweak(tweak, i);
        const size_t t = generate_set0_tweak_as_int(i);
        encryption_function(&cipher_ctx, tweak, plaintext,
                            ciphertext);
        
        // Split ciphertext C^0_i
        ciphertext_as_int = to_integer(ciphertext);
        split(msb_ciphertext_j, lsb_ciphertext_j, ciphertext_as_int);
        
        if (context->be_verbose) {
            printf("C_{%2zu} (msb, lsb): %09lx %09lx T %09lx\n",
                   i, msb_ciphertext_j, lsb_ciphertext_j, t);
        }
        
        investigate(lists,
                    msb_ciphertext_j,
                    lsb_ciphertext_j,
                    t,
                    num_pairs_per_difference,
                    num_investigate_calls);
        
        // Store (T^0_i, msb(C^0_i))
        const CiphertextTweakTuple tuple =
            std::make_pair(i, msb_ciphertext_j);
        (*lists)[lsb_ciphertext_j].push_back(tuple);
    }
    
    // ---------------------------------------------------------
    // Essential: We need to clear all ciphertext entries from L
    // for the current message M^i to reuse it for the next message.
    // ---------------------------------------------------------
    
    clear_lists(lists);
    
    // ---------------------------------------------------------
    // Encrypt second list of (T^j, M') pairs.
    // Find collisions (C^i, C^j), where C^i corresponds to some (T^i, M)
    // on-the-fly, determine for each C^j a = the number of C^i's it collides
    // with, compute delta = T^i xor T^j, add table[delta] to the number of
    // total collisions, and increase table[delta] += a.
    // ---------------------------------------------------------
    
    to_array(plaintext, possible_message_j);
    size_t num_collisions = 0;
    
    if (context->be_verbose) {
        puts("# j C_j");
    }
    
    for (size_t i = 0; i < context->num_texts_per_set; ++i) {
        generate_set1_tweak(tweak, i,
                            context->num_texts_per_set_log);
        const size_t t_prime = generate_set1_tweak_as_int(
            i, context->num_texts_per_set_log);
        
        encryption_function(&cipher_ctx, tweak, plaintext, ciphertext);
        
        // Split ciphertext C^1_j
        ciphertext_as_int = to_integer(ciphertext);
        split(msb_ciphertext_j, lsb_ciphertext_j, ciphertext_as_int);
        
        if (context->be_verbose) {
            printf("C'_{%2zu} (msb, lsb): %09lx %09lx T %09lx\n",
                   i, msb_ciphertext_j, lsb_ciphertext_j, t_prime);
        }
        
        num_collisions += find_num_collisions(lists,
                                              msb_ciphertext_j,
                                              lsb_ciphertext_j,
                                              t_prime,
                                              num_pairs_per_difference,
                                              context->num_texts_per_set_log,
                                              num_find_num_collisions_calls,
                                              num_find_num_collisions_in_d_calls);
        
        // Store (T^1_j, msb(C^1_j))
        const CiphertextTweakTuple tuple = std::make_pair(
            i, msb_ciphertext_j);
        (*lists)[lsb_ciphertext_j].push_back(tuple);
    }
    
    // ---------------------------------------------------------
    // Essential: We need to clear all ciphertext entries from L
    // for the current message M^i to reuse it for the next message.
    // ---------------------------------------------------------
    
    clear_lists(lists);
    
    printf("# investigate_calls: %zu\n", num_investigate_calls);
    printf("# find_num_collisions_calls: %zu\n",
           num_find_num_collisions_calls);
    printf("# find_num_collisions_in_d_calls: %zu\n",
           num_find_num_collisions_in_d_calls);
    
    return num_collisions;
}

// ---------------------------------------------------------

static void generate_random_key(tnt_small_aes36_key_t key) {
    utils::get_random_bytes(key, TNT_SMALL_AES36_NUM_KEY_BYTES);
    
    // Nullify nibble 9, 19, 29
    for (size_t i = 1; i <= TNT_NUM_ROUNDS; ++i) {
        key[(i * SMALL_AES36_NUM_KEY_BYTES) - 1] &= 0xF0;
    }
    
    utils::print_hex("# Key", key, TNT_SMALL_AES36_NUM_KEY_BYTES);
}

// ---------------------------------------------------------

static size_t perform_experiment(ExperimentContext *context) {
    // ---------------------------------------------------------
    // Key setup, random key
    // ---------------------------------------------------------
    
    tnt_small_aes36_sse_context_t &cipher_ctx = context->cipher_ctx;
    tnt_small_aes36_key_t key;
    
    generate_random_key(key);
    tnt_small_aes36_key_schedule(&cipher_ctx, key);
    
    // ---------------------------------------------------------
    // Find message pairs with the desired tweak difference after pi_1.
    // ---------------------------------------------------------
    
    const EncryptFunction first_part_encryption_function =
        context->use_tprp ?
        encrypt_random :
        encrypt_real_first_part;
    
    CiphertextsVector possible_messages_i;
    CiphertextsVector possible_messages_j;
    
    const bool found_enough_possible_message_pairs =
        find_possible_messages(context,
                               first_part_encryption_function,
                               possible_messages_i,
                               possible_messages_j,
                               context->use_wrong_messages);
    
    // ---------------------------------------------------------
    // Stop this experiment if we did not find enough.
    // ---------------------------------------------------------
    
    if (!found_enough_possible_message_pairs) {
        puts("# Not enough possible message pairs found");
        return 0;
    }
    
    printf("# Found %zu possible message pairs\n",
           context->num_desired_possible_message_pairs);
    
    // ---------------------------------------------------------
    // Find collisions for the individual message pairs
    // ---------------------------------------------------------
    
    size_t num_collisions = 0;
    
    TweakIndicesPointer lists(new TweakIndices());
    const EncryptFunction encryption_function =
        context->use_tprp ?
        encrypt_random :
        encrypt_real;
    
    // ---------------------------------------------------------
    // Initialize all round-key candidates to be possible
    // ---------------------------------------------------------
    
    CiphertextsVector possible_round_keys(NUM_KEY_CANDIDATES, true);
    
    // ---------------------------------------------------------
    // Find collisions for the individual message pairs
    // ---------------------------------------------------------
    
    for (size_t i = 0; i < context->num_desired_possible_message_pairs; ++i) {
        num_collisions += generate_lists(context,
                                         encryption_function,
                                         lists,
                                         possible_messages_i[i],
                                         possible_messages_j[i]);
    }
    
    // ---------------------------------------------------------
    // Initialize all round-key candidates to be possible
    // ---------------------------------------------------------
    
    return num_collisions;
}

// ---------------------------------------------------------

static void perform_experiments(ExperimentContext *context) {
    ExperimentResult all_results;
    all_results.num_collisions = 0;
    
    printf("# %8zu Experiments\n", context->num_keys);
    printf("# %8zu Sets/key\n", context->num_texts_per_set);
    printf("# Key Collisions Mean Variance \n");
    
    for (size_t i = 0; i < context->num_keys; ++i) {
        const size_t num_collisions = perform_experiment(context);
        const double mean =
            (double) num_collisions /
            (double) context->num_texts_per_set;
        
        all_results.num_collisions += num_collisions;
        all_results.num_collisions_per_set.push_back(num_collisions);
        
        printf("%4zu %8zu %8.4f\n", i + 1, num_collisions, mean);
    }
    
    const double mean = compute_mean(
        all_results.num_collisions_per_set);
    const double variance = compute_variance(
        all_results.num_collisions_per_set);
    
    printf("# Total Keys Collisions Mean Variance \n");
    printf("# %4zu %8zu %8.4f %8.8f\n",
           context->num_keys,
           all_results.num_collisions,
           mean,
           variance);
}

// ---------------------------------------------------------
// Argument parsing
// ---------------------------------------------------------

static void
parse_args(ExperimentContext *context, int argc, const char **argv) {
    ArgumentParser parser;
    parser.appName("Test for the TNT distinguisher.");
    parser.addArgument("-k", "--num_keys", 1, false);
    parser.addArgument("-p", "--num_pairs", 1, false);
    parser.addArgument("-s", "--num_texts_per_set", 1, false); // 2^12
    parser.addArgument("-r", "--use_tprp", 1, false);
    parser.addArgument("-v", "--verbose", 1, false);
    parser.addArgument("-w", "--wrong_messages", 1, false);
    
    try {
        parser.parse((size_t) argc, argv);
        
        context->num_texts_per_set_log = parser.retrieveAsLong("s");
        context->num_texts_per_set = static_cast<const size_t>(1L)
            << context->num_texts_per_set_log;
        context->num_desired_possible_message_pairs
            = parser.retrieveAsLong("p");
        context->num_keys = parser.retrieveAsLong("k");
        context->use_tprp = static_cast<bool>(parser.retrieveAsInt(
            "r"));
        context->be_verbose = static_cast<bool>(parser.retrieveAsInt(
            "v"));
        context->use_wrong_messages = static_cast<bool>(parser.retrieveAsInt(
            "w"));
    } catch (...) {
        fprintf(stderr, "%s\n", parser.usage().c_str());
        exit(EXIT_FAILURE);
    }
    
    printf("# Keys     %8zu\n", context->num_keys);
    printf("# Sets/Key %8zu\n", context->num_texts_per_set);
    printf("# Pairs    %8zu\n", context->num_desired_possible_message_pairs);
    printf("# Use TPRP %8d\n", context->use_tprp);
    printf("# Use Wrong Msg %3d\n", context->use_wrong_messages);
    printf("# Verbose  %8d\n", context->be_verbose);
}

// ---------------------------------------------------------

int main(int argc, const char **argv) {
    ExperimentContext context;
    parse_args(&context, argc, argv);
    perform_experiments(&context);
    return EXIT_SUCCESS;
}
