/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <array>
#include <functional>
#include <vector>
#include <cstdint>
#include <cstdlib>

#include "ciphers/small_present24.h"
#include "ciphers/tnt_small_present24.h"
#include "utils/argparse.h"
#include "utils/utils.h"
#include "utils/xorshift1024.h"


using ciphers::tnt_small_present24_context_t;
using ciphers::tnt_small_present24_state_t;
using ciphers::tnt_small_present24_tweak_t;
using ciphers::tnt_small_present24_key_t;
using utils::assert_equal;
using utils::compute_mean;
using utils::compute_variance;
using utils::xor_arrays;
using utils::zeroize_array;
using utils::ArgumentParser;

// ---------------------------------------------------------
// Constants
// ---------------------------------------------------------

static const size_t NUM_LISTS = 2;
static const size_t NUM_BITS = 24;
static const auto NUM_CIPHERTEXT_VALUES = static_cast<size_t>(1L) << NUM_BITS;

// ---------------------------------------------------------
// Types
// ---------------------------------------------------------

typedef struct {
    tnt_small_present24_key_t key;
    tnt_small_present24_context_t cipher_ctx;
    size_t num_keys;
    size_t num_texts_per_set;
    size_t num_texts_per_set_log;
    std::vector<size_t> num_matches;
    bool be_verbose = false;
    bool use_tprp = false;
} ExperimentContext;

typedef struct {
    std::vector<size_t> num_collisions_per_set;
    size_t num_collisions = 0;
} ExperimentResult;

typedef uint32_t Ciphertext;
typedef std::vector<Ciphertext> CiphertextsVector;
typedef std::array<CiphertextsVector, NUM_LISTS> CiphertextsLists;
typedef std::array<size_t, NUM_CIPHERTEXT_VALUES> Table;
typedef std::vector<CiphertextsVector> TweakIndices; // , NUM_CIPHERTEXT_VALUES
typedef std::vector<TweakIndices> TweakIndicesList; // , NUM_LISTS
typedef std::array<Table, NUM_LISTS> TableList;

typedef std::function<void(const tnt_small_present24_context_t *,
                           tnt_small_present24_tweak_t,
                           tnt_small_present24_state_t,
                           tnt_small_present24_state_t)> EncryptFunction;

// ---------------------------------------------------------
// Functions
// ---------------------------------------------------------

static uint32_t to_integer(const tnt_small_present24_state_t state) {
    uint32_t result = 0;
    result |= static_cast<uint32_t>(state[0]) << 16;
    result |= static_cast<uint32_t>(state[1]) << 8;
    result |= static_cast<uint32_t>(state[2]);
    return result;
}

// ---------------------------------------------------------

static void to_array(tnt_small_present24_state_t result, const uint32_t state) {
    result[0] = static_cast<uint32_t>((state >> 16) & 0xFF);
    result[1] = static_cast<uint32_t>((state >> 8) & 0xFF);
    result[2] = static_cast<uint32_t>(state & 0xFF);
}

// ---------------------------------------------------------

static void generate_set0_tweak(tnt_small_present24_state_t tweak,
                                const size_t index) {
    to_array(tweak, static_cast<uint32_t>(index));
}

// ---------------------------------------------------------

/**
 * num_bits = log(num_sets_per_key), e.g. 11, 12, 13.
 * @param tweak
 * @param index
 * @param num_bits
 */
static void generate_set1_tweak(tnt_small_present24_state_t tweak,
                                const size_t index,
                                const size_t num_bits) {
    const size_t shift = NUM_BITS - num_bits; // e.g. 5, 4, 3
    const uint32_t shifted_index = index << shift;
    to_array(tweak, shifted_index);
}

// ---------------------------------------------------------

static size_t generate_set0_tweak_as_int(const size_t index) {
    return index;
}

// ---------------------------------------------------------

/**
 * num_bits = log(num_sets_per_key), e.g. 11, 12, 13.
 * @param tweak
 * @param index
 * @param num_bits
 */
static size_t generate_set1_tweak_as_int(const size_t index,
                                         const size_t num_bits) {
    return index << (NUM_BITS - num_bits);
}

// ---------------------------------------------------------

static void encrypt_random(const tnt_small_present24_context_t *ctx,
                           const tnt_small_present24_tweak_t tweak,
                           const tnt_small_present24_state_t plaintext,
                           tnt_small_present24_state_t ciphertext) {
    // Suppress unused parameter warnings. We need those parameters only to
    // comply with the real encryption function.
    (void)ctx;
    (void)tweak;
    (void)plaintext;
    utils::get_random_bytes(ciphertext, TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
}

// ---------------------------------------------------------

static void encrypt_real(const tnt_small_present24_context_t *ctx,
                         const tnt_small_present24_tweak_t tweak,
                         const tnt_small_present24_state_t plaintext,
                         tnt_small_present24_state_t ciphertext) {
    ciphers::tnt_small_present24_encrypt(ctx, tweak, plaintext, ciphertext);
}

// ---------------------------------------------------------

static void zeroize_table(Table &table) {
    for (size_t i = 0; i < NUM_CIPHERTEXT_VALUES; ++i) {
        table[i] = 0;
    }
}

// ---------------------------------------------------------

static void generate_lists(ExperimentContext *context,
                           const EncryptFunction &encryption_function,
                           TweakIndicesList &lists) {
    tnt_small_present24_context_t cipher_ctx = context->cipher_ctx;
    tnt_small_present24_key_t key;
    
    // ---------------------------------------------------------
    // Key setup, random key
    // ---------------------------------------------------------
    
    utils::get_random_bytes(key, TNT_SMALL_PRESENT24_NUM_KEY_BYTES);
    tnt_small_present24_key_schedule(&cipher_ctx, key);
    
    // ---------------------------------------------------------
    // Random messages M^0, M^1
    // ---------------------------------------------------------
    
    tnt_small_present24_state_t plaintexts[NUM_LISTS];
    
    utils::get_random_bytes(plaintexts[0], TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    utils::get_random_bytes(plaintexts[1], TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    
    // ---------------------------------------------------------
    // Log
    // ---------------------------------------------------------
    
    if (context->be_verbose) {
        utils::print_hex("# Key", key, TNT_SMALL_PRESENT24_NUM_KEY_BYTES);
        utils::print_hex("# M^0", plaintexts[0],
                         TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
        utils::print_hex("# M^1", plaintexts[1],
                         TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
    }
    
    // ---------------------------------------------------------
    // Fill lists with encryptions
    // ---------------------------------------------------------
    
    Ciphertext tweaks_as_ints[NUM_LISTS];
    Ciphertext ciphertexts_as_ints[NUM_LISTS];
    tnt_small_present24_state_t tweaks[NUM_LISTS];
    tnt_small_present24_state_t ciphertexts[NUM_LISTS];
    
    if (context->be_verbose) {
        printf("#    i    j    t    p    c\n");
    }
    
    for (size_t i = 0; i < context->num_texts_per_set; ++i) {
        generate_set0_tweak(tweaks[0], i);
        generate_set1_tweak(tweaks[1], i, context->num_texts_per_set_log);
        
        for (size_t j = 0; j < NUM_LISTS; ++j) {
            encryption_function(&cipher_ctx,
                                tweaks[j],
                                plaintexts[j],
                                ciphertexts[j]);
    
            ciphertexts_as_ints[j] = to_integer(ciphertexts[j]);
    
            if (context->be_verbose) {
                tweaks_as_ints[j] = to_integer(tweaks[j]);
                
                printf("# %4zu %4zu %06x %02x%02x%02x %06x\n", i, j,
                       tweaks_as_ints[j],
                       plaintexts[j][0],
                       plaintexts[j][1],
                       plaintexts[j][2],
                       ciphertexts_as_ints[j]);
            }
            
            // Dirty hack, but works: Prevents that the random function generates
            // the same C = C' for different (T, M) and (T, M').
            if ((i == 0) && (j == 1) &&
                (ciphertexts_as_ints[0] == ciphertexts_as_ints[1])) {
                ciphertexts_as_ints[1]++;
            }
            
            // Store i (tweak index) into the position of the ciphertext
            // in list j.
            const size_t tweak_index = i;
            lists[j][ciphertexts_as_ints[j]].push_back(tweak_index);
        }
    }
}

// ---------------------------------------------------------

static size_t find_num_collisions_in_lists(const ExperimentContext *context,
                                           const TweakIndicesList &lists) {
    Table table;
    zeroize_table(table);
    size_t num_total_collisions = 0;
    
    for (size_t ciphertext = 0;
         ciphertext < NUM_CIPHERTEXT_VALUES; ++ciphertext) {
        const size_t left_num_values = lists[0][ciphertext].size();
        
        if (left_num_values == 0) {
            continue;
        }
        
        const size_t right_num_values = lists[1][ciphertext].size();
        
        if (right_num_values == 0) {
            continue;
        }
        
        const CiphertextsVector &left_list = lists[0][ciphertext];
        const CiphertextsVector &right_list = lists[1][ciphertext];
        
        for (size_t i = 0; i < left_num_values; ++i) {
            const size_t t_index = left_list[i];
            const size_t t = generate_set0_tweak_as_int(t_index);
            
            for (size_t j = 0; j < right_num_values; ++j) {
                const size_t t_prime_index = right_list[j];
                const size_t t_prime = generate_set1_tweak_as_int(
                    t_prime_index, context->num_texts_per_set_log);
                const size_t delta_t = t ^t_prime;
                
                num_total_collisions += table[delta_t];
                table[delta_t]++;
            }
        }
    }
    
    return num_total_collisions;
}

// ---------------------------------------------------------

static size_t perform_experiment(ExperimentContext *context) {
    // TODO
    TweakIndicesList lists(
        NUM_LISTS,
        TweakIndices(NUM_CIPHERTEXT_VALUES));
    
    const EncryptFunction encryption_function = context->use_tprp ?
                                                encrypt_random : encrypt_real;

    generate_lists(context, encryption_function, lists);
    const size_t num_collisions = find_num_collisions_in_lists(context, lists);
    return num_collisions;
}

// ---------------------------------------------------------

static void perform_experiments(ExperimentContext *context) {
    ExperimentResult all_results;
    all_results.num_collisions = 0;
    
    printf("# %8zu Experiments\n", context->num_keys);
    printf("# %8zu Sets/key\n", context->num_texts_per_set);
    printf("# Key Collisions Mean Variance \n");
    
    for (size_t i = 0; i < context->num_keys; ++i) {
        const size_t num_collisions = perform_experiment(context);
        const double mean =
            (double) num_collisions / (double) context->num_texts_per_set;
        
        all_results.num_collisions += num_collisions;
        all_results.num_collisions_per_set.push_back(num_collisions);
        
        printf("%4zu %8zu %8.4f\n", i + 1, num_collisions, mean);
    }
    
    const double mean = compute_mean(all_results.num_collisions_per_set);
    const double variance = compute_variance(
        all_results.num_collisions_per_set);
    
    printf("# Total Keys Collisions Mean Variance \n");
    printf("# %4zu %8zu %8.4f %8.8f\n",
           context->num_keys,
           all_results.num_collisions,
           mean,
           variance);
}

// ---------------------------------------------------------
// Argument parsing
// ---------------------------------------------------------

static void
parse_args(ExperimentContext *context, int argc, const char **argv) {
    ArgumentParser parser;
    parser.appName("Test for the TNT distinguisher.");
    parser.addArgument("-k", "--num_keys", 1, false);
    parser.addArgument("-s", "--num_texts_per_set", 1, false); // 2^12
    parser.addArgument("-r", "--use_tprp", 1, false);
    parser.addArgument("-v", "--verbose", 1, false);
    
    try {
        parser.parse((size_t) argc, argv);
        
        context->num_texts_per_set_log = parser.retrieveAsLong("s");
        context->num_texts_per_set = static_cast<const size_t>(1L)
            << context->num_texts_per_set_log;
        context->num_keys = parser.retrieveAsLong("k");
        context->use_tprp = static_cast<bool>(parser.retrieveAsInt("r"));
        context->be_verbose = static_cast<bool>(parser.retrieveAsInt("v"));
    } catch (...) {
        fprintf(stderr, "%s\n", parser.usage().c_str());
        exit(EXIT_FAILURE);
    }
    
    printf("# Keys     %8zu\n", context->num_keys);
    printf("# Sets/Key %8zu\n", context->num_texts_per_set);
    printf("# Use TPRP %8d\n", context->use_tprp);
    printf("# Verbose  %8d\n", context->be_verbose);
}

// ---------------------------------------------------------

int main(int argc, const char **argv) {
    ExperimentContext context;
    parse_args(&context, argc, argv);
    perform_experiments(&context);
    return EXIT_SUCCESS;
}
