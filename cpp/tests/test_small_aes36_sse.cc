/**
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdint>
#include <gtest/gtest.h>

#include "ciphers/small_aes36.h"
#include "utils/utils.h"


using ciphers::small_aes36_sse_context_t;
using ciphers::small_aes36_key_t;
using ciphers::small_aes36_state_t;
using utils::assert_equal;

// ---------------------------------------------------------

static
void run_encryption_test(const small_aes36_key_t key,
                         const small_aes36_state_t plaintext,
                         const small_aes36_state_t expected_ciphertext) {
    small_aes36_sse_context_t ctx;
    small_aes36_key_schedule(&ctx, key);
    
    small_aes36_state_t ciphertext;
    small_aes36_encrypt(&ctx, plaintext, ciphertext);
    ASSERT_TRUE(assert_equal(expected_ciphertext, ciphertext,
                             (size_t) SMALL_AES36_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

static
void run_round_encryption_test(const small_aes36_key_t key,
                               const small_aes36_state_t plaintext,
                               const small_aes36_state_t expected_ciphertext,
                               const size_t num_rounds) {
    small_aes36_sse_context_t ctx;
    small_aes36_key_schedule(&ctx, key);
    
    small_aes36_state_t ciphertext;
    small_aes36_encrypt(&ctx, plaintext, ciphertext, num_rounds);
    ASSERT_TRUE(assert_equal(expected_ciphertext, ciphertext,
                             (size_t) SMALL_AES36_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

static
void run_decryption_test(const small_aes36_key_t key,
                         const small_aes36_state_t ciphertext,
                         const small_aes36_state_t expected_plaintext) {
    small_aes36_sse_context_t ctx;
    small_aes36_key_schedule(&ctx, key);
    
    small_aes36_state_t plaintext;
    small_aes36_decrypt(&ctx, ciphertext, plaintext);
    ASSERT_TRUE(assert_equal(expected_plaintext, plaintext,
                             (size_t) SMALL_AES36_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

TEST(SMALL_AES36_SSE, test_key_schedule) {
    const small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80
    };
    small_aes36_sse_context_t ctx;
    small_aes36_key_schedule(&ctx, key);

    uint8_t round_key[16];

    const uint8_t expected_round_keys[9 * SMALL_AES36_NUM_ROUND_KEYS + 16] = {
        0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8,
        0xb, 0x8, 0x5, 0x8, 0xc, 0x0, 0xe, 0xb, 0x8,
        0x5, 0x1, 0x5, 0xd, 0xd, 0x5, 0x3, 0x6, 0xd,
        0x6, 0x0, 0x1, 0xb, 0xd, 0x4, 0x8, 0xb, 0x9,
        0x2, 0xd, 0x8, 0x9, 0x0, 0xc, 0x1, 0xb, 0x5,
        0xd, 0x3, 0x3, 0x4, 0x3, 0xf, 0x5, 0x8, 0xa,
        0x2, 0xc, 0xd, 0x6, 0xf, 0x2, 0x3, 0x7, 0x8,
        0x4, 0x5, 0x9, 0x2, 0xa, 0xb, 0x1, 0xd, 0x3,
        0xe, 0x1, 0x2, 0xc, 0xb, 0x9, 0xd, 0x6, 0xa,
        0xc, 0xe, 0x3, 0x0, 0x5, 0xa, 0xd, 0x3, 0x0,
        0x2, 0x8, 0x2, 0x2, 0xd, 0x8, 0xf, 0xe, 0x8,
        0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
        0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
    };

    for (size_t i = 0; i < SMALL_AES36_NUM_ROUND_KEYS; ++i) {
        const uint8_t *start_expected_round_key = expected_round_keys + i * 9;
        storeu(round_key, ctx.subkeys[i]);
        ASSERT_TRUE(assert_equal(start_expected_round_key, round_key, 9));
    }
}

// ---------------------------------------------------------

TEST(SMALL_AES36_SSE, test_encrypt_zeroes) {
    const small_aes36_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const small_aes36_state_t plaintext = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const small_aes36_state_t expected_ciphertext = {
        0x62, 0xed, 0xdd, 0xc2, 0xb0
    };
    run_encryption_test(key, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(SMALL_AES36_SSE, test_encrypt_1234) {
    const small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80
    };
    const small_aes36_state_t plaintext = {
        0x02, 0x46, 0x8a, 0xce, 0x30
    };
    const small_aes36_state_t expected_ciphertext = {
        0xb4, 0x32, 0x6d, 0xed, 0xf0
    };
    run_encryption_test(key, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(SMALL_AES36_SSE, test_encrypt_rounds) {
    const small_aes36_key_t key = {
        0x01, 0x23, 0x45, 0x67, 0x80
    };
    const small_aes36_state_t plaintext = {
        0x02, 0x46, 0x8a, 0xce, 0x30
    };
    const uint8_t expected_ciphertexts[
        SMALL_AES36_NUM_STATE_BYTES * (SMALL_AES36_NUM_ROUNDS + 1)] = {
            0x02, 0x46, 0x8a, 0xce, 0x30,
            0x54, 0x65, 0xc5, 0x64, 0x90,
            0x14, 0x4f, 0x0f, 0x9b, 0xf0,
            0x8f, 0xab, 0xc9, 0x4a, 0xa0,
            0x5d, 0x57, 0xe7, 0x27, 0x40,
            0x7f, 0xeb, 0x0c, 0x65, 0xb0,
            0x56, 0x04, 0xc7, 0xa0, 0xd0,
            0xbc, 0x4a, 0x2f, 0xe6, 0x30,
            0xf3, 0x80, 0x81, 0xd8, 0x10,
            0x8c, 0x2e, 0xb6, 0xd1, 0xd0,
            0x5b, 0xe3, 0xd6, 0x3e, 0xe0
        };

    for (size_t num_rounds = 0;
         num_rounds <= SMALL_AES36_NUM_ROUNDS; ++num_rounds) {
        small_aes36_state_t expected_ciphertext;
        memcpy(expected_ciphertext,
               expected_ciphertexts + num_rounds * SMALL_AES36_NUM_STATE_BYTES,
               SMALL_AES36_NUM_STATE_BYTES);
        run_round_encryption_test(key,
                                  plaintext,
                                  expected_ciphertext,
                                  num_rounds);
    }
}

// ---------------------------------------------------------

TEST(SMALL_AES36_SSE, test_encrypt_rounds_zero_key) {
    const small_aes36_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const small_aes36_state_t plaintext = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const uint8_t expected_ciphertexts[
        SMALL_AES36_NUM_STATE_BYTES * (SMALL_AES36_NUM_ROUNDS + 1)] = {
            0x00, 0x00, 0x00, 0x00, 0x00,
            0xba, 0xab, 0xaa, 0xba, 0xa0,
            0xcf, 0x7b, 0x91, 0xcf, 0x70,
            0x42, 0x8b, 0x0a, 0x4f, 0xa0,
            0xeb, 0xba, 0x9f, 0xfc, 0x40,
            0x1e, 0x70, 0xff, 0x5a, 0x00,
            0xa1, 0x6c, 0x49, 0xd2, 0x00,
            0xa8, 0x21, 0xef, 0xad, 0x80,
            0xc0, 0x2b, 0xac, 0x74, 0xd0,
            0xc2, 0xbd, 0x62, 0x1c, 0xd0,
            0x39, 0xf5, 0x5c, 0x33, 0xb0
        };
    
    for (size_t num_rounds = 0;
         num_rounds <= SMALL_AES36_NUM_ROUNDS; ++num_rounds) {
        small_aes36_state_t expected_ciphertext;
        memcpy(expected_ciphertext,
               expected_ciphertexts + num_rounds * SMALL_AES36_NUM_STATE_BYTES,
               SMALL_AES36_NUM_STATE_BYTES);
        run_round_encryption_test(key,
                                  plaintext,
                                  expected_ciphertext,
                                  num_rounds);
    }
}

// ---------------------------------------------------------

TEST(DISABLED_SMALL_AES36_SSE, test_decrypt_full) {
    const small_aes36_key_t key = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    const small_aes36_state_t ciphertext = {
        0x5f, 0x5e, 0x1c, 0x7e, 0x70
    };
    const small_aes36_state_t expected_plaintext = {
        0x00, 0x00, 0x00, 0x00, 0x00
    };
    run_decryption_test(key, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// ---------------------------------------------------------
