/**
 * __author__ = anonymized
 * __date__   = 2019-05
 * __copyright__ = Creative Commons CC0
 */

#include <stdint.h>
#include <gtest/gtest.h>

#include "ciphers/speck32.h"
#include "utils/utils.h"


using ciphers::speck32_context_t;
using ciphers::speck32_key_t;
using ciphers::speck32_state_t;
using utils::assert_equal;

// ---------------------------------------------------------

static void run_encryption_test(const speck32_key_t key,
                                const speck32_state_t plaintext,
                                const speck32_state_t expected_ciphertext) {
    speck32_context_t ctx;
    speck32_key_schedule(&ctx, key);
    
    speck32_state_t ciphertext;
    speck32_encrypt(&ctx, plaintext, ciphertext);
    ASSERT_TRUE(assert_equal(expected_ciphertext, ciphertext,
                             (size_t) SPECK_32_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

static void run_decryption_test(const speck32_key_t key,
                                const speck32_state_t ciphertext,
                                const speck32_state_t expected_plaintext) {
    speck32_context_t ctx;
    speck32_key_schedule(&ctx, key);
    
    speck32_state_t plaintext;
    speck32_decrypt(&ctx, ciphertext, plaintext);
    ASSERT_TRUE(assert_equal(expected_plaintext, plaintext,
                             (size_t) SPECK_32_NUM_STATE_BYTES));
}

// ---------------------------------------------------------

TEST(SPECK_32, test_key_schedule) {
    const speck32_key_t key = {
        0x19, 0x18, 0x11, 0x10, 0x09, 0x08, 0x01, 0x00
    };
    speck32_context_t ctx;
    speck32_key_schedule(&ctx, key);
    
    uint8_t round_key[SPECK_32_NUM_ROUND_KEY_BYTES];
    
    const uint8_t expected_round_keys[
        SPECK_32_NUM_ROUND_KEY_BYTES * SPECK_32_NUM_ROUND_KEYS] = {
            0x01, 0x00, 0x15, 0x12, 0x61, 0x7d, 0x14, 0x58,
            0x69, 0x19, 0x77, 0xe2, 0x0c, 0x89, 0xcc, 0xdb,
            0xef, 0xea, 0x4e, 0x33, 0x76, 0xf4, 0x59, 0x76,
            0xee, 0x8b, 0xdb, 0x04, 0x46, 0x17, 0xf3, 0x7e,
            0x87, 0xb4, 0x8e, 0xca, 0xed, 0x9b, 0x3a, 0x52,
            0x82, 0x29, 0xed, 0x64
        };
    
    for (size_t i = 0; i < SPECK_32_NUM_ROUND_KEYS; ++i) {
        const uint8_t *start_expected_round_key =
            expected_round_keys + i * SPECK_32_NUM_ROUND_KEY_BYTES;
        utils::to_uint8(round_key,
                        reinterpret_cast<const uint16_t *>(&(ctx.subkeys[i])),
                        SPECK_32_NUM_ROUND_KEY_BYTES);
        ASSERT_TRUE(utils::assert_equal(start_expected_round_key, round_key,
                                        SPECK_32_NUM_ROUND_KEY_BYTES));
    }
}

// ---------------------------------------------------------

TEST(SPECK_32, test_encrypt_full) {
    const speck32_key_t key = {
        0x19, 0x18, 0x11, 0x10, 0x09, 0x08, 0x01, 0x00
    };
    const speck32_state_t plaintext = {
        0x65, 0x74, 0x69, 0x4c
    };
    const speck32_state_t expected_ciphertext = {
        0xa8, 0x68, 0x42, 0xf2
    };
    run_encryption_test(key, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

TEST(SPECK_32, test_decrypt_full) {
    const speck32_key_t key = {
        0x19, 0x18, 0x11, 0x10, 0x09, 0x08, 0x01, 0x00
    };
    const speck32_state_t ciphertext = {
        0xa8, 0x68, 0x42, 0xf2
    };
    const speck32_state_t expected_plaintext = {
        0x65, 0x74, 0x69, 0x4c
    };
    run_decryption_test(key, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// ---------------------------------------------------------
