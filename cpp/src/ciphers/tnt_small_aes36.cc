/**
 * C implementation of TNT with Small-AES-36.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

// ---------------------------------------------------------------------

#include <cstdint>
#include <cstdlib>

#include "ciphers/tnt_small_aes36.h"
#include "ciphers/small_aes36.h"
#include "utils/utils.h"

namespace ciphers {
    
    void tnt_small_aes36_key_schedule(tnt_small_aes36_context_t *ctx,
                                      const tnt_small_aes36_key_t key) {
        small_aes36_key_schedule(&(ctx->permutation_ctx[0]),
                                 &(key[0]));
        small_aes36_key_schedule(&(ctx->permutation_ctx[1]),
                                 &(key[SMALL_AES36_NUM_KEY_BYTES]));
        small_aes36_key_schedule(&(ctx->permutation_ctx[2]),
                                 &(key[2 * SMALL_AES36_NUM_KEY_BYTES]));
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt(const tnt_small_aes36_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t plaintext,
                                 tnt_small_aes36_state_t ciphertext) {
        tnt_small_aes36_state_t state1;
        tnt_small_aes36_state_t state2;

//        utils::print_hex("M", plaintext, TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_encrypt(&(ctx->permutation_ctx[0]), plaintext, state1,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);

//        utils::print_hex("S", state1, TNT_SMALL_AES36_NUM_STATE_BYTES);
//        utils::print_hex("T", tweak, TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);

//        utils::print_hex("U", state1, TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_encrypt(&(ctx->permutation_ctx[1]), state1, state2,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);

//        utils::print_hex("V", state2, TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);

//        utils::print_hex("W", state2, TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_encrypt(&(ctx->permutation_ctx[2]), state2, ciphertext,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);

//        utils::print_hex("C", ciphertext, TNT_SMALL_AES36_NUM_STATE_BYTES);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt_first_part(
        const tnt_small_aes36_context_t *ctx,
        const tnt_small_aes36_tweak_t tweak,
        const tnt_small_aes36_state_t plaintext,
        tnt_small_aes36_state_t ciphertext) {
        (void) tweak;
        small_aes36_encrypt_final_round_without_mc(
            &(ctx->permutation_ctx[0]),
            plaintext,
            ciphertext,
            TNT_SMALL_AES36_NUM_PART_ROUNDS);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt_first_part(
        const tnt_small_aes36_context_t *ctx,
        const tnt_small_aes36_tweak_t tweak,
        const tnt_small_aes36_state_t plaintext,
        tnt_small_aes36_state_t ciphertext,
        const size_t num_first_rounds) {
        (void) tweak;
        small_aes36_encrypt(&(ctx->permutation_ctx[0]),
                            plaintext,
                            ciphertext,
                            num_first_rounds);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt(const tnt_small_aes36_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t plaintext,
                                 tnt_small_aes36_state_t ciphertext,
                                 const size_t num_first_rounds) {
        tnt_small_aes36_state_t state1;
        tnt_small_aes36_state_t state2;
        
        small_aes36_encrypt(&(ctx->permutation_ctx[0]),
                            plaintext,
                            state1,
                            num_first_rounds);
        
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_encrypt(&(ctx->permutation_ctx[1]), state1, state2,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
        
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_encrypt(&(ctx->permutation_ctx[2]), state2, ciphertext,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_decrypt(const tnt_small_aes36_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t ciphertext,
                                 tnt_small_aes36_state_t plaintext) {
        tnt_small_aes36_state_t state1;
        tnt_small_aes36_state_t state2;
        
        small_aes36_decrypt(&(ctx->permutation_ctx[2]), ciphertext, state2,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
        
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_decrypt(&(ctx->permutation_ctx[1]), state2, state1,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
        
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_decrypt(&(ctx->permutation_ctx[0]), state1, plaintext,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_decrypt(const tnt_small_aes36_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t ciphertext,
                                 tnt_small_aes36_state_t plaintext,
                                 const size_t num_first_rounds) {
        tnt_small_aes36_state_t state1;
        tnt_small_aes36_state_t state2;
        
        small_aes36_decrypt(&(ctx->permutation_ctx[2]), ciphertext, state2,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
        
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_decrypt(&(ctx->permutation_ctx[1]), state2, state1,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
        
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_decrypt(&(ctx->permutation_ctx[0]),
                            state1,
                            plaintext,
                            num_first_rounds);
    }
    
    // ---------------------------------------------------------
    // SSE Functions
    // ---------------------------------------------------------
    
    void tnt_small_aes36_key_schedule(tnt_small_aes36_sse_context_t *ctx,
                                      const tnt_small_aes36_key_t key) {
        small_aes36_key_schedule(&(ctx->permutation_ctx[0]),
                                 &(key[0]));
        small_aes36_key_schedule(&(ctx->permutation_ctx[1]),
                                 &(key[SMALL_AES36_NUM_KEY_BYTES]));
        small_aes36_key_schedule(&(ctx->permutation_ctx[2]),
                                 &(key[2 * SMALL_AES36_NUM_KEY_BYTES]));
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt(const tnt_small_aes36_sse_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t plaintext,
                                 tnt_small_aes36_state_t ciphertext) {
        tnt_small_aes36_state_t state1;
        tnt_small_aes36_state_t state2;

//        utils::print_hex("M", plaintext, TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_encrypt(&(ctx->permutation_ctx[0]), plaintext, state1,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);

//        utils::print_hex("S", state1, TNT_SMALL_AES36_NUM_STATE_BYTES);
//        utils::print_hex("T", tweak, TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);

//        utils::print_hex("U", state1, TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_encrypt(&(ctx->permutation_ctx[1]), state1, state2,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);

//        utils::print_hex("V", state2, TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);

//        utils::print_hex("W", state2, TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_encrypt(&(ctx->permutation_ctx[2]), state2, ciphertext,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);

//        utils::print_hex("C", ciphertext, TNT_SMALL_AES36_NUM_STATE_BYTES);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt_first_part(
        const tnt_small_aes36_sse_context_t *ctx,
        const tnt_small_aes36_tweak_t tweak,
        const tnt_small_aes36_state_t plaintext,
        tnt_small_aes36_state_t ciphertext) {
        (void) tweak;
        small_aes36_encrypt_final_round_without_mc(
            &(ctx->permutation_ctx[0]),
            plaintext,
            ciphertext,
            TNT_SMALL_AES36_NUM_PART_ROUNDS);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt_first_part(
        const tnt_small_aes36_sse_context_t *ctx,
        const tnt_small_aes36_tweak_t tweak,
        const tnt_small_aes36_state_t plaintext,
        tnt_small_aes36_state_t ciphertext,
        const size_t num_first_rounds) {
        (void) tweak;
        small_aes36_encrypt(&(ctx->permutation_ctx[0]),
                            plaintext,
                            ciphertext,
                            num_first_rounds);
    }
    
    // ---------------------------------------------------------
    
    uint64_t tnt_small_aes36_encrypt_first_part(
        const tnt_small_aes36_sse_context_t *ctx,
        const uint64_t tweak,
        const uint64_t plaintext) {
        return tweak ^ small_aes36_encrypt(&(ctx->permutation_ctx[0]),
                                           plaintext);
    }
    
    // ---------------------------------------------------------
    
    uint64_t tnt_small_aes36_encrypt_first_two_parts(
        const tnt_small_aes36_sse_context_t *ctx,
        const uint64_t tweak,
        const uint64_t plaintext) {
        uint64_t state;
        state = small_aes36_encrypt(&(ctx->permutation_ctx[0]),
                                    plaintext);
        state ^= tweak;
        
        state = small_aes36_encrypt(&(ctx->permutation_ctx[1]),
                                    state);
        state ^= tweak;
        return state;
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_encrypt(const tnt_small_aes36_sse_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t plaintext,
                                 tnt_small_aes36_state_t ciphertext,
                                 const size_t num_first_rounds) {
        tnt_small_aes36_state_t state1;
        tnt_small_aes36_state_t state2;
        
        small_aes36_encrypt(&(ctx->permutation_ctx[0]),
                            plaintext,
                            state1,
                            num_first_rounds);
        
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_encrypt(&(ctx->permutation_ctx[1]), state1, state2,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
        
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_encrypt(&(ctx->permutation_ctx[2]), state2, ciphertext,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_decrypt(const tnt_small_aes36_sse_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t ciphertext,
                                 tnt_small_aes36_state_t plaintext) {
        tnt_small_aes36_state_t state1;
        tnt_small_aes36_state_t state2;
        
        small_aes36_decrypt(&(ctx->permutation_ctx[2]), ciphertext, state2,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
        
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_decrypt(&(ctx->permutation_ctx[1]), state2, state1,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
        
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_decrypt(&(ctx->permutation_ctx[0]), state1, plaintext,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_aes36_decrypt(const tnt_small_aes36_sse_context_t *ctx,
                                 const tnt_small_aes36_tweak_t tweak,
                                 const tnt_small_aes36_state_t ciphertext,
                                 tnt_small_aes36_state_t plaintext,
                                 const size_t num_first_rounds) {
        tnt_small_aes36_state_t state1;
        tnt_small_aes36_state_t state2;
        
        small_aes36_decrypt(&(ctx->permutation_ctx[2]), ciphertext, state2,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
        
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_decrypt(&(ctx->permutation_ctx[1]), state2, state1,
                            TNT_SMALL_AES36_NUM_PART_ROUNDS);
        
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_AES36_NUM_STATE_BYTES);
        
        small_aes36_decrypt(&(ctx->permutation_ctx[0]),
                            state1,
                            plaintext,
                            num_first_rounds);
    }
    
}
