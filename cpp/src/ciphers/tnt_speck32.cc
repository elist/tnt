/**
 * C implementation of TNT with Speck-32.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

// ---------------------------------------------------------------------

#include <stdint.h>
#include <stdlib.h>

#include "ciphers/tnt_speck32.h"
#include "ciphers/speck32.h"
#include "utils/utils.h"

using utils::xor_arrays;

namespace ciphers {
    
    void tnt_speck32_key_schedule(tnt_speck32_context_t *ctx,
                                  const tnt_speck32_key_t key) {
        speck32_key_schedule(&(ctx->permutation_ctx[0]), &(key[0]));
        speck32_key_schedule(&(ctx->permutation_ctx[1]), &(key[SPECK_32_NUM_KEY_BYTES]));
        speck32_key_schedule(&(ctx->permutation_ctx[2]), &(key[2 * SPECK_32_NUM_KEY_BYTES]));
    }
    
    // ---------------------------------------------------------
    
    void tnt_speck32_encrypt(const tnt_speck32_context_t *ctx,
                             const tnt_speck32_tweak_t tweak,
                             const tnt_speck32_state_t plaintext,
                             tnt_speck32_state_t ciphertext) {
        tnt_speck32_state_t state1;
        tnt_speck32_state_t state2;
        speck32_encrypt(&(ctx->permutation_ctx[0]), plaintext, state1);
        xor_arrays(state1, state1, tweak, TNT_SPECK_32_NUM_STATE_BYTES);
        
        speck32_encrypt(&(ctx->permutation_ctx[1]), state1, state2);
        xor_arrays(state2, state2, tweak, TNT_SPECK_32_NUM_STATE_BYTES);
        
        speck32_encrypt(&(ctx->permutation_ctx[2]), state2, ciphertext);
    }
    
    // ---------------------------------------------------------
    
    void tnt_speck32_decrypt(const tnt_speck32_context_t *ctx,
                             const tnt_speck32_tweak_t tweak,
                             const tnt_speck32_state_t ciphertext,
                             tnt_speck32_state_t plaintext) {
        tnt_speck32_state_t state1;
        tnt_speck32_state_t state2;
        speck32_decrypt(&(ctx->permutation_ctx[2]), ciphertext, state2);
        xor_arrays(state2, state2, tweak, TNT_SPECK_32_NUM_STATE_BYTES);
    
        speck32_decrypt(&(ctx->permutation_ctx[1]), state2, state1);
        xor_arrays(state1, state1, tweak, TNT_SPECK_32_NUM_STATE_BYTES);
    
        speck32_decrypt(&(ctx->permutation_ctx[0]), state1, plaintext);
    }
    
}
