/**
 * C implementation of Small-AES36 with tweak addition after rounds 2 and 3.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdio>

#include "ciphers/experimental_small_kiasubc36.h"

// ---------------------------------------------------------

namespace ciphers {
    
    void experimental_small_kiasubc36_key_schedule(small_aes36_context_t *ctx,
                                                   const small_aes36_key_t key) {
        small_aes36_key_schedule(ctx, key);
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t
    experimental_small_kiasubc36_encrypt(const small_aes36_context_t *ctx,
                                         const uint64_t tweak,
                                         const uint64_t plaintext) {
        uint64_t state = plaintext;
        state ^= ctx->subkeys[0];
//        printf("# AK %09lx\n", state);
        
        state = small_aes36_encrypt_round(ctx->subkeys[1], state);
        state = small_aes36_encrypt_round(ctx->subkeys[2], state);
        state ^= tweak;
        state = small_aes36_encrypt_round(ctx->subkeys[3], state);
        state ^= tweak;
        state = small_aes36_encrypt_round(ctx->subkeys[4], state);
        state = small_aes36_encrypt_final_round(ctx->subkeys[5], state);

//        printf("# C  %09lx\n", state);
        return state;
    }
    
}
