/**
 * C implementation of Small-PRESENT20.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdio>
#include "ciphers/small_present20.h"

// ---------------------------------------------------------

namespace ciphers {
    
    static uint32_t to_integer(const small_present20_state_t state) {
        uint32_t result = 0;
        result |= static_cast<uint32_t>(state[0]) << 12;
        result |= static_cast<uint32_t>(state[1]) << 4;
        result |= static_cast<uint32_t>(state[2] & 0xF0) >> 4;
        return result;
    }
    
    // ---------------------------------------------------------
    
    static void to_array(small_present20_state_t result, uint32_t state) {
        result[0] = static_cast<uint32_t>((state >> 12) & 0xFF);
        result[1] = static_cast<uint32_t>((state >> 4) & 0xFF);
        result[2] = static_cast<uint32_t>((state << 4) & 0xF0);
    }
    
    // ---------------------------------------------------------
    
    static uint32_t small_present20_sbox_layer(uint32_t state) {
        return (SMALL_PRESENT_SBOX[(state >> 16) & 0xF] << 16)
               | (SMALL_PRESENT_SBOX[(state >> 12) & 0xF] << 12)
               | (SMALL_PRESENT_SBOX[(state >> 8) & 0xF] << 8)
               | (SMALL_PRESENT_SBOX[(state >> 4) & 0xF] << 4)
               | SMALL_PRESENT_SBOX[state & 0xF];
    }
    
    // ---------------------------------------------------------
    
    static uint32_t small_present20_inverse_sbox_layer(uint32_t state) {
        return (SMALL_PRESENT_INVERSE_SBOX[(state >> 16) & 0xF] << 16)
               | (SMALL_PRESENT_INVERSE_SBOX[(state >> 12) & 0xF] << 12)
               | (SMALL_PRESENT_INVERSE_SBOX[(state >> 8) & 0xF] << 8)
               | (SMALL_PRESENT_INVERSE_SBOX[(state >> 4) & 0xF] << 4)
               | SMALL_PRESENT_INVERSE_SBOX[state & 0xF];
    }
    
    // ---------------------------------------------------------
    
    static uint32_t small_present20_apply_permutation(uint32_t state,
                                                      const size_t *permutation) {
        uint32_t result = 0;
        size_t shift;
        
        for (size_t i = 0; i < 20; ++i) {
            shift = permutation[i];
            result |= ((state >> shift) & 0x1) << (19 - i);
        }
        
        return result;
    }
    
    // ---------------------------------------------------------
    
    static uint32_t small_present20_permutation_layer(uint32_t state) {
        return small_present20_apply_permutation(
            state,
            SMALL_PRESENT20_PERMUTATION);
    }
    
    // ---------------------------------------------------------
    
    static uint32_t small_present20_inverse_permutation_layer(uint32_t state) {
        return small_present20_apply_permutation(
            state,
            SMALL_PRESENT20_INVERSE_PERMUTATION);
    }
    
    // ---------------------------------------------------------
    
    void small_present20_key_schedule(small_present20_context_t *ctx,
                                      const small_present20_key_t key) {
        uint64_t msb_key_bits = ((uint64_t) (key[0]) << 8)
                                | ((uint64_t) (key[1]));
        uint64_t lsb_key_bits = ((uint64_t) (key[2]) << 56)
                                | ((uint64_t) (key[3]) << 48)
                                | ((uint64_t) (key[4]) << 40)
                                | ((uint64_t) (key[5]) << 32)
                                | ((uint64_t) (key[6]) << 24)
                                | ((uint64_t) (key[7]) << 16)
                                | ((uint64_t) (key[8]) << 8)
                                | (uint64_t) (key[9]);
        uint64_t temp;
        
        ctx->subkeys[0] = (lsb_key_bits >> 16) & 0xFFFFF;
        // printf("Key after round %d: %05x\n", 0, ctx->subkeys[0]);
        
        for (size_t i = 1; i <= SMALL_PRESENT20_NUM_ROUNDS; ++i) {
            temp = msb_key_bits;
            msb_key_bits = (lsb_key_bits >> 3) & 0xFFFF;
            lsb_key_bits = (temp << 45)
                           | (lsb_key_bits << 61)
                           | (lsb_key_bits >> 19);
            temp = static_cast<uint64_t>(
                SMALL_PRESENT_SBOX[(msb_key_bits >> 12) & 0x0F]
            );
            
            msb_key_bits = (msb_key_bits & 0x0FFF) | (temp << 12);
            lsb_key_bits ^= i << 15;
            ctx->subkeys[i] = (lsb_key_bits >> 16) & 0xFFFFF;
            // printf("Key after round %zu: %05x\n", i, ctx->subkeys[i]);
        }
    }
    
    // ---------------------------------------------------------
    
    void small_present20_encrypt(const small_present20_context_t *ctx,
                                 const small_present20_state_t plaintext,
                                 small_present20_state_t ciphertext) {
        uint32_t state = to_integer(plaintext);
        // printf("ENC: State after round %d: %05x\n", 0, state);
        state ^= ctx->subkeys[0];
        
        for (size_t i = 1; i <= SMALL_PRESENT20_NUM_ROUNDS; ++i) {
            // printf("ENC: S xor k     round %zu: %05x\n", i, state);
            state = small_present20_sbox_layer(state);
            // printf("ENC: S(s xor k)  round %zu: %05x\n", i, state);
            state = small_present20_permutation_layer(state);
            state ^= ctx->subkeys[i];
            // printf("ENC: State after round %zu: %05x\n", i, state);
        }
        
        to_array(ciphertext, state);
    }
    
    // ---------------------------------------------------------
    
    void small_present20_decrypt(const small_present20_context_t *ctx,
                                 const small_present20_state_t ciphertext,
                                 small_present20_state_t plaintext) {
        uint32_t state = to_integer(ciphertext);
        
        for (size_t i = SMALL_PRESENT20_NUM_ROUNDS; i > 0; --i) {
            // printf("DEC: State after round %zu: %05x\n", i, state);
            state ^= ctx->subkeys[i];
            state = small_present20_inverse_permutation_layer(state);
            // printf("DEC: S(s xor k)  round %zu: %05x\n", i, state);
            state = small_present20_inverse_sbox_layer(state);
            // printf("DEC: S xor k     round %zu: %05x\n", i, state);
        }
        
        state ^= ctx->subkeys[0];
        // printf("DEC: State after round %d: %05x\n", 0, state);
        to_array(plaintext, state);
    }
    
}
