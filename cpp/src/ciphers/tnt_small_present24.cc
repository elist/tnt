/**
 * C implementation of TNT with Small-PRESENT-24.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

// ---------------------------------------------------------------------

#include <cstdint>
#include <cstdlib>

#include "ciphers/tnt_small_present24.h"
#include "ciphers/small_present24.h"
#include "utils/utils.h"

namespace ciphers {
    
    void tnt_small_present24_key_schedule(tnt_small_present24_context_t *ctx,
                                          const tnt_small_present24_key_t key) {
        small_present24_key_schedule(&(ctx->permutation_ctx[0]), &(key[0]));
        small_present24_key_schedule(&(ctx->permutation_ctx[1]),
                                     &(key[SMALL_PRESENT24_NUM_KEY_BYTES]));
        small_present24_key_schedule(&(ctx->permutation_ctx[2]),
                                     &(key[2 * SMALL_PRESENT24_NUM_KEY_BYTES]));
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_present24_encrypt(const tnt_small_present24_context_t *ctx,
                                     const tnt_small_present24_tweak_t tweak,
                                     const tnt_small_present24_state_t plaintext,
                                     tnt_small_present24_state_t ciphertext) {
        tnt_small_present24_state_t state1;
        tnt_small_present24_state_t state2;
        small_present24_encrypt(&(ctx->permutation_ctx[0]), plaintext, state1);
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
        
        small_present24_encrypt(&(ctx->permutation_ctx[1]), state1, state2);
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
        
        small_present24_encrypt(&(ctx->permutation_ctx[2]), state2, ciphertext);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_present24_encrypt_first_part(
        const tnt_small_present24_context_t *ctx,
        const tnt_small_present24_tweak_t tweak,
        const tnt_small_present24_state_t plaintext,
        tnt_small_present24_state_t ciphertext) {
        (void)tweak;
        small_present24_encrypt(&(ctx->permutation_ctx[0]),
                                plaintext,
                                ciphertext);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_present24_encrypt_first_part(
        const tnt_small_present24_context_t *ctx,
        const tnt_small_present24_tweak_t tweak,
        const tnt_small_present24_state_t plaintext,
        tnt_small_present24_state_t ciphertext,
        const size_t num_first_rounds) {
        (void)tweak;
        small_present24_encrypt(&(ctx->permutation_ctx[0]),
                                plaintext,
                                ciphertext,
                                num_first_rounds);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_present24_encrypt(const tnt_small_present24_context_t *ctx,
                                     const tnt_small_present24_tweak_t tweak,
                                     const tnt_small_present24_state_t plaintext,
                                     tnt_small_present24_state_t ciphertext,
                                     const size_t num_first_rounds) {
        tnt_small_present24_state_t state1;
        tnt_small_present24_state_t state2;
        small_present24_encrypt(&(ctx->permutation_ctx[0]),
                                plaintext,
                                state1,
                                num_first_rounds);
        
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
        
        small_present24_encrypt(&(ctx->permutation_ctx[1]), state1, state2);
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
        
        small_present24_encrypt(&(ctx->permutation_ctx[2]), state2, ciphertext);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_present24_decrypt(const tnt_small_present24_context_t *ctx,
                                     const tnt_small_present24_tweak_t tweak,
                                     const tnt_small_present24_state_t ciphertext,
                                     tnt_small_present24_state_t plaintext) {
        tnt_small_present24_state_t state1;
        tnt_small_present24_state_t state2;
        small_present24_decrypt(&(ctx->permutation_ctx[2]), ciphertext, state2);
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
        
        small_present24_decrypt(&(ctx->permutation_ctx[1]), state2, state1);
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
        
        small_present24_decrypt(&(ctx->permutation_ctx[0]), state1, plaintext);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_present24_decrypt(const tnt_small_present24_context_t *ctx,
                                     const tnt_small_present24_tweak_t tweak,
                                     const tnt_small_present24_state_t ciphertext,
                                     tnt_small_present24_state_t plaintext,
                                     const size_t num_first_rounds) {
        tnt_small_present24_state_t state1;
        tnt_small_present24_state_t state2;
        small_present24_decrypt(&(ctx->permutation_ctx[2]), ciphertext, state2);
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
        
        small_present24_decrypt(&(ctx->permutation_ctx[1]), state2, state1);
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_PRESENT24_NUM_STATE_BYTES);
        
        small_present24_decrypt(&(ctx->permutation_ctx[0]),
                                state1,
                                plaintext,
                                num_first_rounds);
    }
    
}
