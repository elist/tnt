/**
 * C implementation of Speck-64.
 *
 * Do NOT use for any production purpose. No guarantees are given for anything.
 *
 * __author__ = anonymized
 * __date__   = 2019-05
 * __copyright__ = Creative Commons CC0
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ciphers/speck32.h"
#include "utils/utils.h"

namespace ciphers {
    
    // ---------------------------------------------------------
    // Basic functions and their inverses
    // ---------------------------------------------------------
    
    void speck32_round(uint16_t *left,
                       uint16_t *right,
                       const uint16_t *round_key) {
        (*left) = ROTR16((*left), SPECK_32_ROT_ALPHA);
        (*left) += (*right);
        (*left) ^= (*round_key);
        (*right) = ROTL16((*right), SPECK_32_ROT_BETA);
        (*right) ^= (*left);
    }
    
    // ---------------------------------------------------------
    
    void speck32_inverse_round(uint16_t *left,
                               uint16_t *right,
                               const uint16_t *round_key) {
        (*right) ^= (*left);
        (*right) = ROTR16((*right), SPECK_32_ROT_BETA);
        (*left) ^= (*round_key);
        (*left) -= (*right);
        (*left) = ROTL16((*left), SPECK_32_ROT_ALPHA);
    }
    
    // ---------------------------------------------------------
    // Key Schedule
    // ---------------------------------------------------------
    
    void speck32_key_schedule(speck32_context_t *ctx,
                              const uint8_t *master_key) {
        uint16_t key[SPECK_32_NUM_KEY_BYTES];
        utils::to_uint16(key, master_key, SPECK_32_NUM_KEY_BYTES);
        uint16_t lp0 = 0;
        uint16_t lp1 = 0;
        uint16_t lp2 = 0;
        uint16_t lp3 = 0;
        
        ctx->subkeys[0] = key[3];
        
        for (uint16_t i = 0; i < SPECK_32_NUM_ROUNDS - 1; ++i) {
            if (i == 0) {
                lp0 = key[2]; // L[2] = left
                lp1 = key[1]; // L[1] = middle
                lp2 = key[0]; // L[0] = right
            } else {
                lp0 = lp1;    // L[0] = new left
                lp1 = lp2;    // L[1] = next in pipeline
                lp2 = lp3;    // L[2] = next in pipeline
            }
            
            // left side
            lp3 = static_cast<uint16_t>(
                (ROTR16(lp0, SPECK_32_ROT_ALPHA) + ctx->subkeys[i]) ^ i
                );
            // new right
            ctx->subkeys[i + 1] = static_cast<uint16_t>(
                ROTL16(ctx->subkeys[i], SPECK_32_ROT_BETA) ^ lp3);
        }
    }
    
    // ---------------------------------------------------------
    // API
    // ---------------------------------------------------------
    
    void speck32_encrypt_rounds(const speck32_context_t *ctx,
                                const uint8_t *plaintext,
                                uint8_t *ciphertext,
                                size_t num_rounds) {
        uint16_t state[2];
        utils::to_uint16(state, plaintext, SPECK_32_NUM_STATE_BYTES);

#ifdef DEBUG
        printf("Round %2d\n", 0);
        utils::print_hex("State (bytes)", (uint8_t*)state, 8);
        printf("Left  (uint)   %08x\n", state[0]);
        printf("Right (uint)   %08x\n", state[1]);
#endif
        
        for (size_t i = 0; i < num_rounds; ++i) {
            speck32_round(reinterpret_cast<uint16_t *>(&(state[0])),
                          reinterpret_cast<uint16_t *>(&(state[1])),
                          &(ctx->subkeys[i]));

#ifdef DEBUG
            printf("Round %2zu\n", i+1);
            utils::print_hex("State (bytes)", (uint8_t*)state, 8);
            printf("Left  (uint)   %08x\n", state[0]);
            printf("Right (uint)   %08x\n", state[1]);
            printf("Key   (uint)   %08x\n", ctx->subkeys[i]);
#endif
        }
        
        utils::to_uint8(ciphertext, state, SPECK_32_NUM_STATE_BYTES);
    }
    
    // ---------------------------------------------------------
    
    void speck32_decrypt_rounds(const speck32_context_t *ctx,
                                const uint8_t *ciphertext,
                                uint8_t *plaintext,
                                size_t num_rounds) {
        uint16_t state[2];
        utils::to_uint16(state, ciphertext, SPECK_32_NUM_STATE_BYTES);

#ifdef DEBUG
        printf("Round %2d\n", SPECK_32_NUM_ROUNDS);
        utils::print_hex("State (bytes)", (uint8_t*)state, 8);
        printf("Left  (uint)   %08x\n", state[0]);
        printf("Right (uint)   %08x\n", state[1]);
#endif
        
        for (int i = (int) num_rounds - 1; i >= 0; --i) {
            speck32_inverse_round(reinterpret_cast<uint16_t *>(&(state[0])),
                                  reinterpret_cast<uint16_t *>(&(state[1])),
                                  &(ctx->subkeys[i]));

#ifdef DEBUG
            printf("Round %2d\n", i);
            utils::print_hex("State (bytes)", (uint8_t*)state, 8);
            printf("Left  (uint)   %08x\n", state[0]);
            printf("Right (uint)   %08x\n", state[1]);
            printf("Key   (uint)   %08x\n", ctx->subkeys[i]);
#endif
        }
        
        utils::to_uint8(plaintext, state, SPECK_32_NUM_STATE_BYTES);
    }
    
    // ---------------------------------------------------------
    
    void speck32_encrypt(const speck32_context_t *ctx,
                         const uint8_t plaintext[SPECK_32_NUM_STATE_BYTES],
                         uint8_t ciphertext[SPECK_32_NUM_STATE_BYTES]) {
        speck32_encrypt_rounds(ctx, plaintext, ciphertext, SPECK_32_NUM_ROUNDS);
    }
    
    // ---------------------------------------------------------
    
    void speck32_decrypt(const speck32_context_t *ctx,
                         const uint8_t ciphertext[SPECK_32_NUM_STATE_BYTES],
                         uint8_t plaintext[SPECK_32_NUM_STATE_BYTES]) {
        speck32_decrypt_rounds(ctx, ciphertext, plaintext, SPECK_32_NUM_ROUNDS);
    }
    
}
