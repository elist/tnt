/**
 * C implementation of TNT with Small-PRESENT-16.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

// ---------------------------------------------------------------------

#include <cstdint>
#include <cstdlib>

#include "ciphers/tnt_small_present16.h"
#include "ciphers/small_present16.h"
#include "utils/utils.h"

namespace ciphers {
    
    void tnt_small_present16_key_schedule(tnt_small_present16_context_t *ctx,
                                          const tnt_small_present16_key_t key) {
        small_present16_key_schedule(&(ctx->permutation_ctx[0]), &(key[0]));
        small_present16_key_schedule(&(ctx->permutation_ctx[1]),
                                     &(key[SMALL_PRESENT16_NUM_KEY_BYTES]));
        small_present16_key_schedule(&(ctx->permutation_ctx[2]),
                                     &(key[2 * SMALL_PRESENT16_NUM_KEY_BYTES]));
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_present16_encrypt(const tnt_small_present16_context_t *ctx,
                                     const tnt_small_present16_tweak_t tweak,
                                     const tnt_small_present16_state_t plaintext,
                                     tnt_small_present16_state_t ciphertext) {
        tnt_small_present16_state_t state1;
        tnt_small_present16_state_t state2;
        small_present16_encrypt(&(ctx->permutation_ctx[0]), plaintext, state1);
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
        
        small_present16_encrypt(&(ctx->permutation_ctx[1]), state1, state2);
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
        
        small_present16_encrypt(&(ctx->permutation_ctx[2]), state2, ciphertext);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_present16_encrypt(const tnt_small_present16_context_t *ctx,
                                     const tnt_small_present16_tweak_t tweak,
                                     const tnt_small_present16_state_t plaintext,
                                     tnt_small_present16_state_t ciphertext,
                                     size_t num_first_rounds) {
        tnt_small_present16_state_t state1;
        tnt_small_present16_state_t state2;
        small_present16_encrypt(&(ctx->permutation_ctx[0]),
                                plaintext,
                                state1,
                                num_first_rounds);
        
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
        
        small_present16_encrypt(&(ctx->permutation_ctx[1]), state1, state2);
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
        
        small_present16_encrypt(&(ctx->permutation_ctx[2]), state2, ciphertext);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_present16_decrypt(const tnt_small_present16_context_t *ctx,
                                     const tnt_small_present16_tweak_t tweak,
                                     const tnt_small_present16_state_t ciphertext,
                                     tnt_small_present16_state_t plaintext) {
        tnt_small_present16_state_t state1;
        tnt_small_present16_state_t state2;
        small_present16_decrypt(&(ctx->permutation_ctx[2]), ciphertext, state2);
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
        
        small_present16_decrypt(&(ctx->permutation_ctx[1]), state2, state1);
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
        
        small_present16_decrypt(&(ctx->permutation_ctx[0]), state1, plaintext);
    }
    
    // ---------------------------------------------------------
    
    void tnt_small_present16_decrypt(const tnt_small_present16_context_t *ctx,
                                     const tnt_small_present16_tweak_t tweak,
                                     const tnt_small_present16_state_t ciphertext,
                                     tnt_small_present16_state_t plaintext,
                                     size_t num_first_rounds) {
        tnt_small_present16_state_t state1;
        tnt_small_present16_state_t state2;
        small_present16_decrypt(&(ctx->permutation_ctx[2]), ciphertext, state2);
        utils::xor_arrays(state2, state2, tweak,
                          TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
        
        small_present16_decrypt(&(ctx->permutation_ctx[1]), state2, state1);
        utils::xor_arrays(state1, state1, tweak,
                          TNT_SMALL_PRESENT16_NUM_STATE_BYTES);
        
        small_present16_decrypt(&(ctx->permutation_ctx[0]),
                                state1,
                                plaintext,
                                num_first_rounds);
        
    }
    
}
