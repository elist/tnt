/**
 * C implementation of a 3x3-nibble version of Small-AES.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdio>

#include "ciphers/small_aes36.h"

// ---------------------------------------------------------

namespace ciphers {
    
    // ---------------------------------------------------------
    // Constants
    // ---------------------------------------------------------

//    static const size_t TIMES2[16] = {
//        //0    1    2    3    4    5    6    7
//        0x0, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe,
//        //8    9   10   11   12   13   14   15
//        //8    9    a    b    c    d    e    f
//        0x3, 0x1, 0x7, 0x5, 0xb, 0x9, 0xf, 0xd
//    };
    
    static const size_t TIMES3[16] = {
        //0    1    2    3    4    5    6    7
        0x0, 0x3, 0x6, 0x5, 0xc, 0xf, 0xa, 0x9,
        //8    9   10   11   12   13   14   15
        //8    9    a    b    c    d    e    f
        0xb, 0x8, 0xd, 0xe, 0x7, 0x4, 0x1, 0x2
    };
    
    // ---------------------------------------------------------------------
    // Functions
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_to_integer(const small_aes36_state_t input) {
        return (((uint64_t) (input[0])) << 28)
               | (((uint64_t) (input[1])) << 20)
               | (((uint64_t) (input[2])) << 12)
               | (((uint64_t) (input[3])) << 4)
               | (((uint64_t) (input[4])) >> 4);
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_to_byte_array(small_aes36_state_t output,
                                   const uint64_t input) {
        output[0] = (uint8_t) ((input >> 28) & 0xFF);
        output[1] = (uint8_t) ((input >> 20) & 0xFF);
        output[2] = (uint8_t) ((input >> 12) & 0xFF);
        output[3] = (uint8_t) ((input >> 4) & 0xFF);
        output[4] = (uint8_t) ((input << 4) & 0xF0);
    }
    
    // ---------------------------------------------------------------------
    
    static uint64_t
    small_aes36_sbox_layer(const size_t sbox[16], const uint64_t state) {
        uint64_t result = 0;
        size_t shift;
        
        for (size_t i = 0; i < SMALL_AES36_NUM_CELLS; ++i) {
            shift = i * 4;
            result |= sbox[(state >> shift) & 0xF] << shift;
        }
        
        return result;
    }
    
    // ---------------------------------------------------------------------
    
    static uint64_t small_aes36_sub_bytes(const uint64_t state) {
        return small_aes36_sbox_layer(SMALL_AES36_SBOX_ARRAY, state);
    }
    
    // ---------------------------------------------------------------------
    
    static uint64_t small_aes36_inverse_sub_bytes(const uint64_t state) {
        return small_aes36_sbox_layer(SMALL_AES36_INVERSE_SBOX_ARRAY, state);
    }
    
    // ---------------------------------------------------------------------
    
    /**
     * In nibbles
     * 036    036
     * 147 -> 471
     * 258    825
     *
     * 012 345 678 => 048 372 615
     * @param state
     * @return
     */
    static uint64_t small_aes36_shift_rows(const uint64_t state) {
        return (state & 0xF00F00F00)
               | ((state << 12) & 0x0F00F0000)
               | ((state << 24) & 0x00F000000)
               | ((state >> 12) & 0x00000F00F)
               | ((state >> 24) & 0x0000000F0);
    }
    
    // ---------------------------------------------------------------------
    
    /**
     * In nibbles
     * 036    036
     * 147 -> 714
     * 258    582
     *
     * 012 345 678 => 075 318 642
     * @param state
     * @return
     */
    static uint64_t small_aes36_inverse_shift_rows(const uint64_t state) {
        return (state & 0xF00F00F00)
               | ((state << 12) & 0x00F00F000)
               | ((state << 24) & 0x0F0000000)
               | ((state >> 12) & 0x0000F00F0)
               | ((state >> 24) & 0x00000000F);
    }
    
    // ---------------------------------------------------------------------
    
    static uint64_t small_aes36_mix_column(const uint64_t column) {
        const uint64_t first = (column >> 8) & 0xF;
        const uint64_t second = (column >> 4) & 0xF;
        const uint64_t third = column & 0xF;
        const uint64_t sum = first ^second ^third;
        return ((sum << 8) | (sum << 4) | sum)
               ^ (((TIMES3[third]) << 8) | (TIMES3[second] << 4) |
                  (TIMES3[first]));
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_mix_columns(const uint64_t state) {
        const uint64_t column_0 = (state >> 24) & 0xFFF;
        const uint64_t column_1 = (state >> 12) & 0xFFF;
        const uint64_t column_2 = state & 0xFFF;
        return (small_aes36_mix_column(column_0) << 24)
               | (small_aes36_mix_column(column_1) << 12)
               | small_aes36_mix_column(column_2);
    }
    
    // ---------------------------------------------------------------------
    
    static uint64_t small_aes36_inverse_mix_columns(const uint64_t state) {
        return state;
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_encrypt_round(const uint64_t round_key,
                                       const uint64_t input) {
        uint64_t state = small_aes36_sub_bytes(input);
//        printf("# SB %09lx\n", state);
        state = small_aes36_shift_rows(state);
//        printf("# SR %09lx\n", state);
        state = small_aes36_mix_columns(state);
//        printf("# MC %09lx\n", state);
        state = state ^ round_key;
//        printf("# AK %09lx\n", state);
        return state;
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_encrypt_final_round(const uint64_t round_key,
                                             const uint64_t input) {
        uint64_t state = small_aes36_sub_bytes(input);
//        printf("# SB %09lx\n", state);
        state = small_aes36_shift_rows(state);
//        printf("# SR %09lx\n", state);
        state = state ^ round_key;
//        printf("# AK %09lx\n", state);
        return state;
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_decrypt_round(const uint64_t round_key,
                                       const uint64_t input) {
        // printf("# AK %09lx\n", input);
        uint64_t state = input ^round_key;
        // printf("# MC %09lx\n", state);
        state = small_aes36_inverse_mix_columns(state);
        // printf("# SR %09lx\n", state);
        state = small_aes36_inverse_shift_rows(state);
        // printf("# SB %09lx\n", state);
        return small_aes36_inverse_sub_bytes(state);
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_decrypt_final_round(const uint64_t round_key,
                                             const uint64_t input) {
        uint64_t state = input ^round_key;
        state = small_aes36_inverse_shift_rows(state);
        return small_aes36_inverse_sub_bytes(state);
    }
    
    // ---------------------------------------------------------------------
    
    void
    small_aes36_key_schedule(small_aes36_context_t *ctx,
                             const small_aes36_key_t key) {
        uint64_t round_key = small_aes36_to_integer(key);
        ctx->subkeys[0] = round_key;
        uint64_t column_0;
        uint64_t column_1;
        uint64_t column_2;
        
        for (size_t round = 1; round <= SMALL_AES36_NUM_ROUNDS; ++round) {
            // Rotate final column
            column_2 = ((round_key << 4) & 0xFF0) | ((round_key >> 8) & 0x00F);
            // Apply S-box
            column_2 = (SMALL_AES36_SBOX_ARRAY[(column_2 >> 8) & 0xF] << 8)
                       | (SMALL_AES36_SBOX_ARRAY[(column_2 >> 4) & 0xF] << 4)
                       | SMALL_AES36_SBOX_ARRAY[column_2 & 0xF];
            column_2 ^= (SMALL_AES_ROUND_CONSTANTS[round] << 8);
            
            column_0 = ((round_key >> 24) ^ column_2) & 0xFFF;
            column_1 = ((round_key >> 12) ^ column_0) & 0xFFF;
            column_2 = (round_key ^ column_1) & 0xFFF;
            round_key = (column_0 << 24) | (column_1 << 12) | column_2;
            ctx->subkeys[round] = round_key;
        }
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt(const small_aes36_context_t *ctx,
                             const small_aes36_state_t plaintext,
                             small_aes36_state_t ciphertext) {
        uint64_t state = small_aes36_to_integer(plaintext);
//        printf("# M  %09lx\n", state);
        
        state ^= ctx->subkeys[0];
//        printf("# AK %09lx\n", state);
        
        for (size_t round = 1; round < SMALL_AES36_NUM_ROUNDS; ++round) {
            state = small_aes36_encrypt_round(ctx->subkeys[round], state);
//            printf("# Round %zu %09lx\n", round, state);
        }
        
        state = small_aes36_encrypt_final_round(
            ctx->subkeys[SMALL_AES36_NUM_ROUNDS],
            state);
//        printf("# C  %09lx\n", state);
        small_aes36_to_byte_array(ciphertext, state);
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt(const small_aes36_context_t *ctx,
                             const small_aes36_state_t plaintext,
                             small_aes36_state_t ciphertext,
                             const size_t num_rounds) {
        if (num_rounds > SMALL_AES36_NUM_ROUNDS) {
            return;
        }
        
        uint64_t state = small_aes36_to_integer(plaintext);
//        printf("# M  %09lx\n", state);
        
        if (num_rounds == 0) {
            small_aes36_to_byte_array(ciphertext, state);
            return;
        }
        
        state ^= ctx->subkeys[0];
//        printf("# AK %09lx\n", state);
        
        for (size_t round = 1; round <= num_rounds; ++round) {
            state = small_aes36_encrypt_round(ctx->subkeys[round], state);
//            printf("# Round %zu %09lx\n", round, state);
        }

//        printf("# C  %09lx\n", state);
        small_aes36_to_byte_array(ciphertext, state);
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt_final_round_without_mc(
        const small_aes36_context_t *ctx,
        const small_aes36_state_t plaintext,
        small_aes36_state_t ciphertext,
        const size_t num_rounds) {
        if (num_rounds > SMALL_AES36_NUM_ROUNDS) {
            return;
        }
        
        uint64_t state = small_aes36_to_integer(plaintext);
//        printf("# M  %09lx\n", state);
        
        if (num_rounds == 0) {
            small_aes36_to_byte_array(ciphertext, state);
            return;
        }
        
        state ^= ctx->subkeys[0];
//        printf("# AK %09lx\n", state);
        
        for (size_t round = 1; round < num_rounds; ++round) {
//            printf("# Round %zu\n", round);
            state = small_aes36_encrypt_round(ctx->subkeys[round], state);
        }
        
        state = small_aes36_encrypt_final_round(ctx->subkeys[num_rounds],
                                                state);

//        printf("# C  %09lx\n", state);
        small_aes36_to_byte_array(ciphertext, state);
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_decrypt(const small_aes36_context_t *ctx,
                             const small_aes36_state_t ciphertext,
                             small_aes36_state_t plaintext) {
        uint64_t state = small_aes36_to_integer(ciphertext);
        state = small_aes36_decrypt_final_round(
            ctx->subkeys[SMALL_AES36_NUM_ROUNDS],
            state);
        
        for (size_t round = SMALL_AES36_NUM_ROUNDS - 1; round > 0; --round) {
            state = small_aes36_decrypt_round(ctx->subkeys[round], state);
        }
        
        state ^= ctx->subkeys[0];
        small_aes36_to_byte_array(plaintext, state);
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_decrypt(const small_aes36_context_t *ctx,
                             const small_aes36_state_t ciphertext,
                             small_aes36_state_t plaintext,
                             const size_t num_rounds) {
        if (num_rounds > SMALL_AES36_NUM_ROUNDS) {
            return;
        }
        
        uint64_t state = small_aes36_to_integer(ciphertext);
        
        if (num_rounds == 0) {
            small_aes36_to_byte_array(plaintext, state);
            return;
        }
        
        for (size_t round = SMALL_AES36_NUM_ROUNDS; round > 0; --round) {
            state = small_aes36_decrypt_round(ctx->subkeys[round], state);
        }
        
        state ^= ctx->subkeys[0];
        small_aes36_to_byte_array(plaintext, state);
    }
    
    // ---------------------------------------------------------------------
    // SSE Functions
    // ---------------------------------------------------------------------
    
    static void small_aes36_to_array(uint8_t temp[16],
                                     const __m128i input) {
        // x0, x2, ..., x14; 0, ... 0
        __m128i evens = vshuffle(
            input,
            vsetr8(0, 2, 4, 6, 8, 10, 12, 14,
                   (uint8_t) 0xFF, (uint8_t) 0xFF, (uint8_t) 0xFF,
                   (uint8_t) 0xFF, (uint8_t) 0xFF, (uint8_t) 0xFF,
                   (uint8_t) 0xFF, (uint8_t) 0xFF
            )
        );
        // x1, x3, ..., x15; 0, ... 0
        __m128i odds = vshuffle(
            input,
            vsetr8(1, 3, 5, 7,
                   9, 11, 13, 15,
                   (uint8_t) 0xFF, (uint8_t) 0xFF, (uint8_t) 0xFF,
                   (uint8_t) 0xFF,
                   (uint8_t) 0xFF, (uint8_t) 0xFF, (uint8_t) 0xFF,
                   (uint8_t) 0xFF
            )
        );
        // Shift evens to higher nibbles
        evens = vshiftleft16(evens, 4);
        storeu(temp, vxor(evens, odds));
    }
    
    // ---------------------------------------------------------------------
    
    static void small_aes36_to_byte_array(small_aes36_state_t output,
                                          const __m128i input) {
        uint8_t temp[16];
        small_aes36_to_array(temp, input);
        memcpy(output, temp, SMALL_AES36_NUM_STATE_BYTES);
    }
    
    // ---------------------------------------------------------------------
    
    static uint64_t small_aes36_to_uint64(const __m128i input) {
        uint8_t temp[16];
        small_aes36_to_array(temp, input);
        return small_aes36_to_integer(temp);
    }
    
    // ---------------------------------------------------------------------
    
    /**
     * Given an 8-byte input, spreads the nibbles to 16 bytes.
     * @param input
     * @return
     */
    static
    __m128i small_aes36_to_lower_nibbles(const small_aes36_state_t input) {
        // K01, K23, K34, ...
        const __m128i x = vset8(
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, input[4],
            input[3], input[2], input[1], input[0]
        );
        const __m128i mask = LO_NIBBLES_MASK;
        
        // K1, K3, K5, ...
        __m128i x_hi = vand(x, mask);
        const __m128i x_lo = vand(vshiftright16(x, 4), mask);
        // K0, K2, K4, ...
        x_hi = vand(x_hi, mask);
        return vunpacklo8(x_lo, x_hi);
    }
    
    // ---------------------------------------------------------------------
    
    /**
     * Given an 8-byte input, spreads the nibbles to 16 bytes.
     * @param input
     * @return
     */
    static
    __m128i small_aes36_to_lower_nibbles(const uint64_t input) {
        // K01, K23, K34, ...
        const __m128i x = vset8(
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, (input & 0x0F) << 4,
            (input >> 4) & 0xFF,
            (input >> 12) & 0xFF,
            (input >> 20) & 0xFF,
            (input >> 28) & 0xFF
        );
        const __m128i mask = LO_NIBBLES_MASK;
        
        // K1, K3, K5, ...
        __m128i x_hi = vand(x, mask);
        const __m128i x_lo = vand(vshiftright16(x, 4), mask);
        // K0, K2, K4, ...
        x_hi = vand(x_hi, mask);
        return vunpacklo8(x_lo, x_hi);
    }
    
    // ---------------------------------------------------------------------
    
    static __m128i generate_round_key(__m128i k, const size_t round) {
        __m128i col0 = small_aes36_sub_bytes(k);
        
        // rotate the last column and put it to the front column
        __m128i mask = vsetr8(
            7, 8, 6, (uint8_t) 0xff,
            (uint8_t) 0xff, (uint8_t) 0xff, (uint8_t) 0xff, (uint8_t) 0xff,
            (uint8_t) 0xff, (uint8_t) 0xff, (uint8_t) 0xff, (uint8_t) 0xff,
            (uint8_t) 0xff, (uint8_t) 0xff, (uint8_t) 0xff, (uint8_t) 0xff
        );
        col0 = vshuffle(col0, mask); // [S(12), S(15), S(14), S(13), 0, ..., 0]
        col0 = vxor(SMALL_AES36_RCON(round), col0);
        col0 = vand(
            vxor(k, col0),
            vsetr8(0xFF, 0xFF, 0xFF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        ); // col0 contains the first correct column
        
        __m128i col1 = vand(vxor(vshiftleft_bytes(col0, 3), k),
                            vsetr8(0, 0, 0, 0xFF, 0xFF, 0xFF, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0));
        __m128i col2 = vand(vxor(vshiftleft_bytes(col1, 3), k),
                            vsetr8(0, 0, 0, 0, 0, 0, 0xFF, 0xFF, 0xFF, 0, 0, 0,
                                   0, 0, 0, 0));
        return vxor(vxor(col0, col1), col2);
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_key_schedule(small_aes36_sse_context_t *ctx,
                                  const small_aes36_key_t key) {
        ctx->subkeys[0] = small_aes36_to_lower_nibbles(key);
        
        for (size_t i = 1; i < SMALL_AES36_NUM_ROUND_KEYS; ++i) {
            ctx->subkeys[i] = generate_round_key(ctx->subkeys[i - 1], i);
        }
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt(const small_aes36_sse_context_t *ctx,
                             const small_aes36_state_t plaintext,
                             small_aes36_state_t ciphertext) {
        __m128i state = small_aes36_to_lower_nibbles(plaintext);
        const __m128i *keys = ctx->subkeys;
//        utils::print_128("M ", state);
        
        state = vxor(state, keys[0]);
//        utils::print_128("# AK ", state);
        
        for (size_t i = 1; i < SMALL_AES36_NUM_ROUNDS; ++i) {
            state = small_aes36_encrypt_round(state, keys[i]);
//            printf("# Round %zu", i);
//            utils::print_128("", state);
        }
        
        state = small_aes36_encrypt_final_round(state,
                                                keys[SMALL_AES36_NUM_ROUNDS]);

//        utils::print_128("M ", state);
        small_aes36_to_byte_array(ciphertext, state);
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt(const small_aes36_sse_context_t *ctx,
                             const small_aes36_state_t plaintext,
                             small_aes36_state_t ciphertext,
                             const size_t num_rounds) {
        if (num_rounds > SMALL_AES36_NUM_ROUNDS) {
            return;
        }
        
        if (num_rounds == 0) {
            memcpy(ciphertext, plaintext, SMALL_AES36_NUM_STATE_BYTES);
            return;
        }
        
        __m128i state = small_aes36_to_lower_nibbles(plaintext);
        const __m128i *keys = ctx->subkeys;
//        utils::print_128("M ", state);
        
        state = vxor(state, keys[0]);
        
        for (size_t i = 1; i <= num_rounds; ++i) {
            state = small_aes36_encrypt_round(state, keys[i]);
//            printf("# Round %zu", i);
//            utils::print_128("", state);
        }

//        utils::print_128("M ", state);
        small_aes36_to_byte_array(ciphertext, state);
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_encrypt(const small_aes36_sse_context_t *ctx,
                                 const uint64_t plaintext) {
        __m128i state = small_aes36_to_lower_nibbles(plaintext);
        const __m128i *keys = ctx->subkeys;
        
        state = vxor(state, keys[0]);
        
        for (size_t i = 1; i <= SMALL_AES36_NUM_ROUNDS; ++i) {
            state = small_aes36_encrypt_round(state, keys[i]);
        }
        
        return small_aes36_to_uint64(state);
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_encrypt(const small_aes36_sse_context_t *ctx,
                                 const uint64_t plaintext,
                                 const size_t num_rounds) {
        if (num_rounds > SMALL_AES36_NUM_ROUNDS) {
            return 0;
        }
        
        if (num_rounds == 0) {
            return plaintext;
        }
        
        __m128i state = small_aes36_to_lower_nibbles(plaintext);
        const __m128i *keys = ctx->subkeys;
        
        state = vxor(state, keys[0]);
        
        for (size_t i = 1; i <= num_rounds; ++i) {
            state = small_aes36_encrypt_round(state, keys[i]);
        }

        return small_aes36_to_uint64(state);
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_encrypt_final_round_without_mc(
        const small_aes36_sse_context_t *ctx,
        const small_aes36_state_t plaintext,
        small_aes36_state_t ciphertext,
        const size_t num_rounds) {
        if (num_rounds > SMALL_AES36_NUM_ROUNDS) {
            return;
        }
        
        if (num_rounds == 0) {
            memcpy(ciphertext, plaintext, SMALL_AES36_NUM_STATE_BYTES);
            return;
        }
        
        __m128i state = small_aes36_to_lower_nibbles(plaintext);
        const __m128i *keys = ctx->subkeys;
        
        state = vxor(state, keys[0]);
        
        for (size_t i = 1; i < num_rounds; ++i) {
            state = small_aes36_encrypt_round(state, keys[i]);
        }
        
        state = small_aes36_encrypt_final_round(state, keys[num_rounds]);
        small_aes36_to_byte_array(ciphertext, state);
    }
    
    // ---------------------------------------------------------------------
    
    __m128i small_aes36_encrypt_round(__m128i state, const __m128i round_key) {
        state = small_aes36_sub_bytes(state);
//        utils::print_128("# SB", state);
        state = small_aes36_shift_rows(state);
//        utils::print_128("# SR", state);
        state = small_aes36_mix_columns(state);
//        utils::print_128("# MC", state);
        return vxor(state, round_key);
    }
    
    // ---------------------------------------------------------------------
    
    __m128i
    small_aes36_encrypt_final_round(__m128i state, const __m128i round_key) {
        state = small_aes36_sub_bytes(state);
        state = small_aes36_shift_rows(state);
        return vxor(state, round_key);
    }
    
    // ---------------------------------------------------------------------
    
    __m128i small_aes36_shift_rows(__m128i state) {
        return vshuffle(state, SMALL_AES36_SHIFT_ROWS);
    }
    
    // ---------------------------------------------------------------------
    
    __m128i small_aes36_sub_bytes(__m128i state) {
        return vshuffle(SMALL_AES36_SBOX, state);
    }
    
    // ---------------------------------------------------------------------
    
    __m128i small_aes36_mix_columns(__m128i state) {
        // x = (x_0, x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8) = state
        // a = (x_1, x_2, x_0, x_4, x_5, x_3, x_7, x_8, x_6)
        // b = (x_2, x_0, x_1, x_5, x_3, x_4, x_8, x_6, x_7)
        // c = (x_2, x_1, x_0, x_5, x_4, x_3, x_8, x_7, x_6)
        // d = 3 * c
        // y = 3 * c + a + b + c = a + b + 2 * c
        const __m128i a = vshuffle(state,
                                   vsetr8(1, 2, 0, 4, 5, 3, 7, 8, 6, 0xff, 0xff,
                                          0xff, 0xff, 0xff, 0xff, 0xff));
        __m128i b = vshuffle(state,
                             vsetr8(2, 0, 1, 5, 3, 4, 8, 6, 7, 0xff, 0xff, 0xff,
                                    0xff, 0xff, 0xff, 0xff));
        __m128i c = vshuffle(state,
                             vsetr8(2, 1, 0, 5, 4, 3, 8, 7, 6, 0xff, 0xff, 0xff,
                                    0xff, 0xff, 0xff, 0xff));
        const __m128i d = vshuffle(SMALL_AES36_TIMES_THREE, c);
        return vxor(vxor(state, a), vxor(b, d));
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_decrypt(const small_aes36_sse_context_t *ctx,
                             const small_aes36_state_t ciphertext,
                             small_aes36_state_t plaintext) {
        (void) ctx;
        (void) ciphertext;
        (void) plaintext;
    }
    
    // ---------------------------------------------------------------------
    
    void small_aes36_decrypt(const small_aes36_sse_context_t *ctx,
                             const small_aes36_state_t ciphertext,
                             small_aes36_state_t plaintext,
                             const size_t num_rounds) {
        (void) ctx;
        (void) ciphertext;
        (void) plaintext;
        (void) num_rounds;
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_decrypt(const small_aes36_sse_context_t *ctx,
                                 uint64_t ciphertext) {
        (void) ctx;
        return ciphertext;
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t small_aes36_decrypt(const small_aes36_sse_context_t *ctx,
                                 uint64_t ciphertext,
                                 size_t num_rounds) {
        (void) ctx;
        (void) num_rounds;
        return ciphertext;
    }
    
}
