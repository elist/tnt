/**
 * C implementation of Small-AES36 with tweak addition after rounds 2 and 3.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdio>

#include "ciphers/experimental_small_twaes36.h"

// ---------------------------------------------------------

namespace ciphers {
    
    void experimental_small_twaes36_key_schedule(small_aes36_context_t *ctx,
                                                 const small_aes36_key_t key) {
        small_aes36_key_schedule(ctx, key);
    }
    
    // ---------------------------------------------------------------------
    
    /**
     * Given a 12-bit tweak input 0* || t_0, t_1, t_2 (in the lsb),
     * expands it to a 36-bit tweak in the 0^28 || x_0 ... x_8 (in the lsb)
     * by computing
     *
     * s = t_0 ^ t_1 ^ t_2
     *
     * and returning 0^28 ||
     *
     * x_0     x_3     x_6
     * x_1     x_4     x_7
     * x_2     x_5     x_8
     *
     * t_0     t_1     t_2
     * t_0 ^ s t_1 ^ s t_2 ^ s
     * 0       0       0
     *
     * @param tweak_input
     * @return
     */
    uint64_t
    experimental_small_twaes36_expand_tweak(const uint64_t tweak_input) {
        const uint64_t sum = ((tweak_input)
                              ^ (tweak_input >> 4)
                              ^ (tweak_input >> 8)) & 0xF;
        const uint64_t tweak_base = ((tweak_input & 0xF00) << 24)
                                    | ((tweak_input & 0xF00) << 20)
                                    | ((tweak_input & 0x0F0) << 16)
                                    | ((tweak_input & 0x0F0) << 12)
                                    | ((tweak_input & 0x00F) << 8)
                                    | ((tweak_input & 0x00F) << 4);
        return ((sum << 28) | (sum << 16) | (sum << 4)) ^ tweak_base;
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t
    experimental_small_twaes36_encrypt(const small_aes36_context_t *ctx,
                                       const uint64_t tweak,
                                       const uint64_t plaintext) {
        uint64_t state = plaintext;
        const uint64_t expanded_tweak = experimental_small_twaes36_expand_tweak(
            tweak);
//        printf("Round %2d: %09zx\n", 0, state);
//        printf("Tweak:    %09zx\n", expanded_tweak);
        state ^= ctx->subkeys[0];
        
        for (size_t round = 1; round < SMALL_AES36_NUM_ROUNDS; ++round) {
            state = small_aes36_encrypt_round(ctx->subkeys[round], state);
            
            if ((round & 1) == 0) {
                state ^= expanded_tweak;
            }
            
//            printf("Round %2zu: %09zx\n", round, state);
        }
        
        return small_aes36_encrypt_final_round(
            ctx->subkeys[SMALL_AES36_NUM_ROUNDS],
            state);
//        printf("Round %2d: %09zx\n", SMALL_AES36_NUM_ROUNDS, state);
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t
    experimental_small_twaes36_encrypt_rounds(const small_aes36_context_t *ctx,
                                              const uint64_t tweak,
                                              const uint64_t plaintext,
                                              const size_t num_rounds) {
        if ((num_rounds == 0) || (num_rounds > SMALL_AES36_NUM_ROUNDS)) {
            return plaintext;
        }
        
        uint64_t state = plaintext;
        const uint64_t expanded_tweak =
            experimental_small_twaes36_expand_tweak(tweak);
//        printf("Tweak:    %09zx\n", expanded_tweak);
        state ^= ctx->subkeys[0];
        
        for (size_t round = 1; round < num_rounds; ++round) {
            state = small_aes36_encrypt_round(ctx->subkeys[round], state);
            
            if ((round & 1) == 0) {
                state ^= expanded_tweak;
            }
        }
        
        if (num_rounds == SMALL_AES36_NUM_ROUNDS) {
            return small_aes36_encrypt_final_round(
                ctx->subkeys[SMALL_AES36_NUM_ROUNDS],
                state);
        }
        
        state = small_aes36_encrypt_round(ctx->subkeys[num_rounds], state);
        
        if ((num_rounds & 1) == 0) {
            state ^= expanded_tweak;
        }
        
        return state;
    }
    
    // ---------------------------------------------------------------------
    
    uint64_t
    experimental_small_twaes36_decrypt(const small_aes36_context_t *ctx,
                                       const uint64_t tweak,
                                       const uint64_t ciphertext) {
        uint64_t state = ciphertext;
        const uint64_t expanded_tweak = experimental_small_twaes36_expand_tweak(
            tweak);
        state = small_aes36_decrypt_final_round(
            ctx->subkeys[SMALL_AES36_NUM_ROUNDS],
            state);
//        printf("Tweak:    %09zx\n", expanded_tweak);
        
        for (size_t round = SMALL_AES36_NUM_ROUNDS - 1; round >= 1; --round) {
//            printf("Round %2zu: %09zx\n", round, state);
            
            if ((round & 1) == 0) {
                state ^= expanded_tweak;
            }
            
            state = small_aes36_decrypt_round(ctx->subkeys[round], state);
        }
        
        state ^= ctx->subkeys[0];
//        printf("Round %2d: %09zx\n", 0, state);
        return state;
    }
    
}
