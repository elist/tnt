/**
 * C implementation of Small-PRESENT24.
 *
 * __author__ = anonymized
 * __date__   = 2020-02
 * __copyright__ = Creative Commons CC0
 */

#include <cstdio>
#include "ciphers/tweakable_small_present24.h"

// ---------------------------------------------------------

namespace ciphers {
    
    static uint32_t to_integer(const small_present24_state_t state) {
        uint32_t result = 0;
        result |= static_cast<uint32_t>(state[0]) << 16;
        result |= static_cast<uint32_t>(state[1]) << 8;
        result |= static_cast<uint32_t>(state[2]);
        return result;
    }
    
    // ---------------------------------------------------------
    
    static void to_array(small_present24_state_t result, uint32_t state) {
        result[0] = static_cast<uint32_t>((state >> 16) & 0xFF);
        result[1] = static_cast<uint32_t>((state >> 8) & 0xFF);
        result[2] = static_cast<uint32_t>(state & 0xFF);
    }
    
    // ---------------------------------------------------------
    
    void
    tweakable_small_present24_encrypt(const small_present24_context_t *ctx,
                                      const small_present24_state_t tweak,
                                      const small_present24_state_t plaintext,
                                      small_present24_state_t ciphertext) {
        uint32_t state = to_integer(plaintext);
        const uint32_t tweak_state = to_integer(tweak);
        // printf("ENC: State after round %d: %06x\n", 0, state);
        state ^= ctx->subkeys[0];
        state ^= tweak_state;
        
        for (size_t i = 1; i <= SMALL_PRESENT24_NUM_ROUNDS; ++i) {
            // printf("ENC: S xor k     round %zu: %06x\n", i, state);
            state = small_present24_sbox_layer(state);
            // printf("ENC: S(s xor k)  round %zu: %06x\n", i, state);
            state = small_present24_permutation_layer(state);
            state ^= ctx->subkeys[i];
            state ^= tweak_state;
            // printf("ENC: State after round %zu: %06x\n", i, state);
        }
        
        to_array(ciphertext, state);
    }
    
    // ---------------------------------------------------------
    
    void
    tweakable_small_present24_encrypt(const small_present24_context_t *ctx,
                                      const small_present24_state_t tweak,
                                      const small_present24_state_t plaintext,
                                      small_present24_state_t ciphertext,
                                      size_t num_rounds) {
        if (num_rounds > SMALL_PRESENT24_NUM_ROUNDS) {
            return;
        }
        
        uint32_t state = to_integer(plaintext);
        const uint32_t tweak_state = to_integer(tweak);
        // printf("ENC: State after round %d: %06x\n", 0, state);
        state ^= ctx->subkeys[0];
        state ^= tweak_state;
        
        for (size_t i = 1; i <= num_rounds; ++i) {
            // printf("ENC: S xor k     round %zu: %06x\n", i, state);
            state = small_present24_sbox_layer(state);
            // printf("ENC: S(s xor k)  round %zu: %06x\n", i, state);
            state = small_present24_permutation_layer(state);
            state ^= ctx->subkeys[i];
            state ^= tweak_state;
            // printf("ENC: State after round %zu: %06x\n", i, state);
        }
        
        to_array(ciphertext, state);
    }
    
    // ---------------------------------------------------------
    
    void
    tweakable_small_present24_decrypt(const small_present24_context_t *ctx,
                                      const small_present24_state_t tweak,
                                      const small_present24_state_t ciphertext,
                                      small_present24_state_t plaintext) {
        uint32_t state = to_integer(ciphertext);
        const uint32_t tweak_state = to_integer(tweak);
        
        for (size_t i = SMALL_PRESENT24_NUM_ROUNDS; i > 0; --i) {
            // printf("DEC: State after round %zu: %06x\n", i, state);
            state ^= tweak_state;
            state ^= ctx->subkeys[i];
            state = small_present24_inverse_permutation_layer(state);
            // printf("DEC: S(s xor k)  round %zu: %06x\n", i, state);
            state = small_present24_inverse_sbox_layer(state);
            // printf("DEC: S xor k     round %zu: %06x\n", i, state);
        }
    
        state ^= tweak_state;
        state ^= ctx->subkeys[0];
        // printf("DEC: State after round %d: %06x\n", 0, state);
        to_array(plaintext, state);
    }
    
    // ---------------------------------------------------------
    
    void
    tweakable_small_present24_decrypt(const small_present24_context_t *ctx,
                                      const small_present24_state_t tweak,
                                      const small_present24_state_t ciphertext,
                                      small_present24_state_t plaintext,
                                      size_t num_rounds) {
        if (num_rounds > SMALL_PRESENT24_NUM_ROUNDS) {
            return;
        }
        
        uint32_t state = to_integer(ciphertext);
        const uint32_t tweak_state = to_integer(tweak);
        
        for (size_t i = num_rounds; i > 0; --i) {
            // printf("DEC: State after round %zu: %06x\n", i, state);
            state ^= tweak_state;
            state ^= ctx->subkeys[i];
            state = small_present24_inverse_permutation_layer(state);
            // printf("DEC: S(s xor k)  round %zu: %06x\n", i, state);
            state = small_present24_inverse_sbox_layer(state);
            // printf("DEC: S xor k     round %zu: %06x\n", i, state);
        }
    
        state ^= tweak_state;
        state ^= ctx->subkeys[0];
        // printf("DEC: State after round %d: %06x\n", 0, state);
        to_array(plaintext, state);
    }
    
}
