# Experiments for our distinguishers on Tweak-aNd-Tweak (TNT) with small versions of PRESENT and AES

## Contents

This repository contains the experiments for our distinguishers on TNT [BGGS20] with minified version of the AES and PRESENT.

Block ciphers are important constructions of encryption, which encrypt an n-bit message M to an n-bit ciphertext C under a key K.
Tweakable block ciphers (TBCs) do the same, but allow an additional public input T, called the tweak, that can be used as additional message input, for separating domains, and in general, can be used in particular encryption or authentication schemes with higher security.
Constructions that transform block ciphers into tweakable block ciphers are therefore desirable, and TNT is one of them.

In its most generic form, Tweak-aNd-Tweak transforms three classical block ciphers E_{K_1}, E_{K_2}, E_{K_3} into a tweakable block cipher by encrypting a message M under a tweak T to a ciphertext C as

C = E_{K_3}(E_{K_2}(E_{K_1}(M) xor T) xor T).

The authors proved that this construction is secure for up to O(2^{2n/3}) queries, where n is the state size in bits of M (and C and T, respectively).
In practice, taking three full block ciphers would be too inefficient.
For this purpose, the authors proposed an instance, TNT-AES, that employs six-round AES in each call of E, and therefore 18 rounds in total.
The key schedule is simply that of the AES, extended to produce 18 + 1 (the initial) round keys.

In [Men18], Bart Mennink proposed a distinguisher on cascaded LRW2, one of the first existing TBCs. Here, we provide distinguishers similar to Mennink's on TNT and TNT-AES with lower complexity.
Still, the computational, data, and memory effort would be infeasible when instantiating them with the real AES.
Thus, our experiments employ smaller ciphers.

This repository contains them in several C++ implementations. Some represent distinguishers, others are functional tests to verify our implementations.


### Ciphers

We basically employ the following block ciphers:

- `src/ciphers/small_aes36`
  We employ an even further reduced version of Small-AES [CMR05] that operates on 36-bit states with a 3x3 matrix of 4-bit cells. Warning: at the moment, we needed and implemented only the encryption direction.

- `src/ciphers/small_present{16,20,24}`
  These are instances of Small-PRESENT from [Lea10] with 16-, 20-, or 24-bit state size, respectively.

They are used in the following instances of TNT:

- `src/ciphers/tnt_small_aes36`
  TNT with 5-round Small-AES36 each.

- `src/ciphers/tnt_small_present{16,20,24}`
  TNT with full-round Small-PRESENT each, that is, full 31 rounds.


### Distinguishers/Key Recovery

We conduct two distinguishers and a key recovery.
The distinguishers can be classified as parallel-road and cross-road distinguishers, both of which form quartets of message-tweak tuples.
For (configurable) numbers m and t, let M^i, 1 <= i <= m, be plaintexts and T^i be tweak sets, for 1 <= i <= t.
For each message, we employ the same tweaks so that we obtain message-tweak tuples (M^i, T^j).
The goal is to find quartets (M^i, T^0), (M^i, T^1), (M^j, T^0), (M^j, T^1)
such that for their correspond ciphertexts C^i, C^j, C^k, C^l, we have two pairs.
The parallel-road distinguisher searches for quartets with C^i = C^j and C^k = C^l.
The cross-road distinguisher searches for quartets with C^i = C^l and C^j = C^k.
Those quartets occur also for a random permutation;
but for TNT, the number of quartets is expected to be about 2x higher as for a random permutation in the case of the cross-road distinguisher, and 3x higher in the case of the parallel-road distinguisher.

A performant approach is implemented in:

- `tests/test_tnt_small_aes36_parallel_roads_distinguisher.cc`
- `tests/test_tnt_small_present{16,20,24}_cross_roads_distinguisher.cc`
- `tests/test_tnt_small_present{16,20,24}_parallel_roads_efficient_distinguisher.cc`

A prior, naive (and slower) approach was implemented in

- `tests/test_tnt_small_present{16,20,24}_parallel_roads_distinguisher.cc`
- `tests/test_tnt_small_present{16,20,24}_cross_roads_efficient_distinguisher.cc`

Moreover, we conducted experiments for the key recovery for TNT-AES with Small-AES36. 

- `tests/test_tnt_small_aes36_parallel_roads_key_recovery.cc`

At the current stage, this is a test with three-step TNT-AES with five-round Small-AES36 in each step.
It uses 2^{12} messages, a random key and conducts the impossible-differential attack to filter incorrect key candidates of the 12-bit of the first diagonal of the initial round key.
This is a test in the sense that it does not conduct the full attack (that involves collecting all message-tweak tuples for all 2^{12} messages, but identifies the right messages with internal knowledge, simply to avoid having to wait too long for the results.


### Functional Tests

We compiled tests to verify the implementations of our primitives and the instances of TNT.

- `tests/test_small_aes36`
  Functional tests of the encryption with Small-AES36.

- `tests/test_small_aes36_sse`
  Functional tests of the encryption with Small-AES36 for an SSE-optimized variant.

- `tests/test_small_present{16,20,24}`
  Functional tests of the encryption with Small-PRESENT{16,20,24}.

- `tests/test_tnt_small_aes36`
  Functional tests of the encryption of TNT with three times five-round
  Small-AES36.

- `tests/test_tnt_small_present{16,20,24}`
  Functional tests of the encryption of TNT with full Small-PRESENT{16,20,24}.


## Build

A `CMakeLists.txt` file is shipped that should allow you to build the tests above, if `CMake`, a C++ compiler, and `GTest` are installed.

In the root directory, type `cmake .` in a commandline/shell to build a make file. Thereupon, you can build the individual targets with `make <target>` where `<target>` is e.g., `test_five_round_distinguisher_small`, or build all by typing `make all`.

Executables will be placed in the directory `bin`.

## Dependencies

- `cmake` for building.
- `g++` or `clang++` as standard compiler. This can be customized in the `CMakeLists.txt` file.
- `gtest`: Google's unit test framework for the functional tests.
- `pthreads`: Needed by `gtest`.


## Third-party code
This repository uses the following open-source components:

- `include/utils/argparse.h`
  https://github.com/hbristow/argparse

- `include/utils/uint128_t.h` and `src/utils/uint128_t.cc`
  copyright Jason Lee, calccrypto at gmail.com

- `include/utils/xorshift1024.h`
  Sebastiano Vigna (vigna (at) acm.org)

This repository comes without any guarantee or endorsement from their corresponding authors or contributors.
The usage licenses and copyright notices are explicitly provided in those files as the corresponding authors and contributors stated.
They may have been slightly modified to add convenience functionality.


## References

- [BGGS20] Zhenzhen Bao, Chun Guo, Jian Guo, and Ling Song. TNT: How to Tweak a Block Cipher. In Anne Canteaut and Yuval Ishai, editors, EUROCRYPT, volume 12106 of LNCS, pages 1–31. Springer, 2020.
- [CMR05] Carlos Cid, Sean Murphy, and Matthew J. B. Robshaw. Small Scale Variants of the AES. In Henri Gilbert and Helena Handschuh, editors, FSE, volume 3557 of Lecture Notes in Computer Science, pages 145–162. Springer, 2005.
- [Lea10] Gregor Leander. Small Scale Variants Of The Block Cipher PRESENT. IACR Cryptology ePrint Archive, 2010:143, 2010.
- [Men18] Bart Mennink. Towards Tight Security of Cascaded LRW2. In Amos Beimel and Stefan Dziembowski, editors, TCC II, volume 11240 of Lecture Notes in Computer Science, pages 192–222. Springer, 2018.
